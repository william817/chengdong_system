<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//登录接口

//获取openid
Route::any('get/code','WechatLoginController@getCode');
Route::any('get/openid','WechatLoginController@getOpenid');

//验证token
Route::any('check/openid','WechatUserController@checkOpenid');

//七牛上传
Route::any('rollback','QiniuController@pic_submit');
Route::any('test','QiniuController@test');

//后台
Route::group(['prefix'=>'admin'],function(){

    //测试
    Route::any('login','AdminLoginController@login');
    Route::any('examList/add','AdminExamListController@examListAdd');
    Route::any('examList/index','AdminExamListController@examListIndex');
    Route::any('examList/update','AdminExamListController@examListUpdate');
    Route::any('examList/delete','AdminExamListController@examListDelete');
    Route::any('examList/history','AdminExamListController@examListHistory'); //进入测试页面加载的列表
    Route::any('exam/add','AdminExamController@examAdd');
    Route::any('exam/list','AdminExamController@examList');
    Route::any('exam/update','AdminExamController@examUpdate');
    Route::any('exam/delete','AdminExamController@examDelete');
//    Route::any('exam/info','AdminExamController@examInfo');
    Route::any('start/exam','AdminExamListController@startExam');
    Route::any('end/exam','AdminExamListController@endExam');
//    Route::any('end/examList','AdminEndController@endExamList');  //微信端每个人答完题后记录接口



    //党建必修课
    Route::any('partyReq/save','AdminPartyActController@partyReqSave');
    Route::any('partyReq/list','AdminPartyActController@partyReqList');
    Route::any('partyReq/edit','AdminPartyActController@partyReqEdit');
    Route::any('partyReq/info','AdminPartyActController@partyReqInfo');
    Route::any('partyReq/delete','AdminPartyActController@partyReqDelete');
    Route::any('partyReq/feedList','AdminPartyActController@partyReqFeedList'); //必修课反馈通过列表
    Route::any('partyReq/feedNoList','AdminPartyActController@partyReqFeedNoList');//必修课反馈不通过列表
    Route::any('partyReq/feed','AdminPartyActController@partyReqFeed');//必修课反馈通过/不通过接口
    Route::any('partyReq/feedDelete','AdminPartyActController@partyReqFeedDelete');//必修课反馈删除接口
    Route::any('partyReq/signIn','AdminPartyActController@partyReqSignIn'); //必修课签到详情接口
    Route::any('party/release','AdminPartyActController@partyRelease'); //发布/撤销详情接口

    //党建选修课
    Route::any('partyCho/save','AdminPartyActController@partyChoSave');
    Route::any('partyCho/list','AdminPartyActController@partyChoList');
    Route::any('partyCho/edit','AdminPartyActController@partyChoEdit');
    Route::any('partyCho/info','AdminPartyActController@partyChoInfo');
    Route::any('partyCho/delete','AdminPartyActController@partyChoDelete');
    Route::any('partyCho/feedList','AdminPartyActController@partyChoFeedList'); //选修课反馈通过列表
    Route::any('partyCho/feedNoList','AdminPartyActController@partyChoFeedNoList');//选修课反馈不通过列表
    Route::any('partyCho/feed','AdminPartyActController@partyChoFeed');//选修课反馈通过/不通过接口
    Route::any('partyCho/feedDelete','AdminPartyActController@partyChoFeedDelete');//选修课反馈删除接口
    Route::any('partyCho/signIn','AdminPartyActController@partyChoSignIn'); //选修课签到详情接口
    Route::any('partyCho/signUp','AdminPartyActController@partyChoSignUp'); //选修课报名详情接口
    Route::any('Cho/start','AdminPartyActController@ChoStart');
    Route::any('Cho/end','AdminPartyActController@ChoEnd');

    //综合必修课
    Route::any('multiReq/save','AdminMultiAcrController@multiReqSave');
    Route::any('multiReq/list','AdminMultiAcrController@multiReqList');
    Route::any('multiReq/edit','AdminMultiAcrController@multiReqEdit');
    Route::any('multiReq/info','AdminMultiAcrController@multiReqInfo');
    Route::any('multiReq/delete','AdminMultiAcrController@multiReqDelete');
    Route::any('multiReq/feedList','AdminMultiAcrController@multiReqFeedList'); //必修课反馈通过列表
    Route::any('multiReq/feedNoList','AdminMultiAcrController@multiReqFeedNoList');//必修课反馈不通过列表
    Route::any('multiReq/feed','AdminMultiAcrController@multiReqFeed');//必修课反馈通过/不通过接口
    Route::any('multiReq/feedDelete','AdminMultiAcrController@multiReqFeedDelete');//必修课反馈删除接口
    Route::any('multiReq/signIn','AdminMultiAcrController@multiReqSignIn'); //必修课签到详情接口
    Route::any('multi/release','AdminMultiAcrController@multiRelease'); //发布/撤销详情接口

    //综合选修课
    Route::any('multiCho/save','AdminMultiAcrController@multiChoSave');
    Route::any('multiCho/list','AdminMultiAcrController@multiChoList');
    Route::any('multiCho/edit','AdminMultiAcrController@multiChoEdit');
    Route::any('multiCho/info','AdminMultiAcrController@multiChoInfo');
    Route::any('multiCho/delete','AdminMultiAcrController@multiChoDelete');
    Route::any('multiCho/feedList','AdminMultiAcrController@multiChoFeedList'); //选修课反馈通过列表
    Route::any('multiCho/feedNoList','AdminMultiAcrController@multiChoFeedNoList');//选修课反馈不通过列表
    Route::any('multiCho/feed','AdminMultiAcrController@multiChoFeed');//选修课反馈通过接口
    Route::any('multiCho/feedDelete','AdminMultiAcrController@multiChoFeedDelete');//选修课反馈删除接口
    Route::any('multiCho/signIn','AdminMultiAcrController@multiChoSignIn'); //选修课签到详情接口
    Route::any('multiCho/signUp','AdminMultiAcrController@multiChoSignUp'); //选修课报名详情接口

    //意见征集
    Route::any('option/list','AdminOptionController@optionList');
    Route::any('option/info','AdminOptionController@optionInfo');
    Route::any('option/delete','AdminOptionController@optionDelete');
    Route::any('option/pass','AdminOptionController@optionPass');
    Route::any('option/noPass','AdminOptionController@optionNoPass');

    //商城
    Route::any('shop/list','AdminShopController@shopList');
    Route::any('shop/add','AdminShopController@shopAdd');
    Route::any('shop/update','AdminShopController@shopUpdate');
    Route::any('shop/delete','AdminShopController@shopDelete');
    Route::any('shop/info','AdminShopController@shopInfo');
    Route::any('shop/group','AdminShopController@shopGroup');  //商品上下架
    Route::any('shop/exchange','AdminShopController@shopExchange');  //商品兑换记录

    //用户
    Route::any('user/passList','AdminUserController@userPassList');//员工审核通过列表
    Route::any('user/passNoList','AdminUserController@userNoPassList');//员工不审核通过列表
    Route::any('user/pass','AdminUserController@examineAndVerify');//员工审核通过接口
    Route::any('user/delete','AdminUserController@userDelete');//员工删除
    Route::any('user/edit','AdminUserController@userEdit');//员工编辑
    Route::any('sendNotice','AdminUserController@sendNotice');//发送模板消息

    //积分列表
    Route::any('score/list','AdminScoreController@scoreList');//
    Route::any('score/edit','AdminScoreController@scoreEdit');//

});

//微信端
Route::group(['prefix'=>'wechat'],function(){

    Route::any('submit/info','WechatUserController@submitUserInfo'); //首次提交个人信息
    Route::any('user/pass','WechatUserController@userPass');
    Route::any('user/edit','WechatUserController@userEdit');
    //报名
    Route::any('sign/up','WechatSignUpController@signUp');
    Route::any('signUp/list','WechatSignUpController@signUpList');
    Route::any('sign/in','WechatSignInController@signIn');


    Route::any('multiCho/list','WechatMultiAcrController@multiChoList');
    Route::any('multiReq/list','WechatMultiAcrController@multiReqList');
    Route::any('multi/info','WechatMultiAcrController@multiInfo');

    Route::any('partyReq/list','WechatPartyActController@partyReqList');
    Route::any('partyCho/list','WechatPartyActController@partyChoList');
    Route::any('party/info','WechatPartyActController@partyInfo');

    Route::any('feedback/add','WechatFeedbackController@feedbackSave');
    Route::any('zan/add','WechatZanRecordController@zanAdd');
    Route::any('feedback/list','WechatFeedbackController@feedbackList');


    Route::any('score/total','WechatScoreController@scoreTotal');
    Route::any('score/record','WechatScoreRecordController@scoreRecord');
    Route::any('score/rank','WechatScoreController@scoreRank');

    Route::any('exam/list','WechatExamListController@examList');
    Route::any('examList/info','WechatExamListController@examListInfo');
    Route::any('exam/info','WechatExamController@examInfo');
    Route::any('exam/sub','WechatExamListController@examListSub');
    Route::any('examList/check','WechatExamListController@examListCheck');

    Route::any('option/list','WechatOptionController@optionList');
    Route::any('option/info','WechatOptionController@optionInfo');
    Route::any('option/save','WechatOptionController@optionSave');

    Route::any('shop/list','WechatShopController@shopList');
    Route::any('shop/exchange','WechatShopController@shopExchange');

});

Route::any('active/addpeople','ActiveController@addPeople');
Route::any('active/peoplelist','ActiveController@peopleList');
Route::any('active/face','ActiveController@face');
Route::any('active/faceimg','ActiveController@faceimg');
Route::any('active/share','ActiveController@share');
Route::any('active/gitconfig','ActiveController@getConfig');

Route::any('submit','RouteController@submit');



/*微信页面路由*/
Route::any('index','RouteController@index');
Route::any('holdOn','RouteController@holdOn');

Route::any('integral/conversion','RouteController@integralConversion');
Route::any('integral/integral','RouteController@integralIntegral');
Route::any('integral/rank','RouteController@integralRank');
Route::any('integral/record','RouteController@integralRecord');

Route::any('Learning/index','RouteController@LearningIndex');
Route::any('Learning/text','RouteController@LearningText');
Route::any('Learning/must','RouteController@LearningMust');
Route::any('Learning/activity','RouteController@LearningActivity');
Route::any('Learning/feed','RouteController@LearningFeed');
Route::any('Learning/take','RouteController@LearningTake');
Route::any('Learning/room','RouteController@LearningRoom');
Route::any('feed/list','RouteController@FeedFeedList');

Route::any('my/index','RouteController@myIndex');
Route::any('my/sub_info','RouteController@mySub_Info');
Route::any('my/info','RouteController@myInfo');
Route::any('my/issue','RouteController@myIssue');

Route::any('opinion','RouteController@opinion');
Route::any('opinion_list','RouteController@opinionList');
Route::any('opinion_info','RouteController@opinionInfo');

Route::any('synth/index','RouteController@synthIndex');
Route::any('synth/must','RouteController@synthMust');
Route::any('synth/activity','RouteController@synthActivity');
Route::any('synth/feed','RouteController@synthFeed');
Route::any('synth/take','RouteController@synthTake');
Route::any('synth/take_activity','RouteController@synthTakeActivity');

Route::any('text/finish','RouteController@textFnish');
Route::any('text/index','RouteController@textIndex');
Route::any('text/wraning','RouteController@textWraning');
Route::any('text/charter','RouteController@textChapter');

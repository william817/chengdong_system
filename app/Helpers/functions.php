<?php

/**
 * @created by neek li
 * @param $inputArr
 * @return array 文件大小、文件路径
 */
function uploadXls($inputArr){
//    $input = array(
//        'fileArr'=>$_FILES,
//        'comment'=>'wo',
//        'inputName'=>'uploadinput',
//        'filename' => '小业主费用信息',
//    );
//    $res = uploadXls($input);
//    dd($res);

    require_once(app_path() . '/' . 'Libs/attachment/XLS.php');

    $inputName = $inputArr['inputName'];//必填参数,表单中input的名字
    $fileArr = $inputArr['fileArr'][$inputName];//必填参数,$_FILES
    $filename = iconv('utf-8','gbk',$inputArr['filename']);

    $savePath = (isset($inputArr['savePath']) && !empty($inputArr['savePath'])) ? $inputArr['savePath'] : "file";//选填参数 文件路径 默认public下file
    $type = (isset($inputArr['type']) && !empty($inputArr['type'])) ? $inputArr['type'] : "file";//选填参数 文件类型 默认 file
    $comment = (isset($inputArr['comment']) && !empty($inputArr['comment'])) ? $inputArr['comment'] : "";//选填参数 文件拼接解释 默认空

    return  XLS::upload($savePath,$fileArr,$type,$comment,$filename);
}


/**
 * created by neek li
 * @param $inputArr
 */
function uploadImg($inputArr){
//savePath填写规则：

//班级相册：班级相册的目录
//  class/album/smallone[具体的班级]/春游

//专业讲堂：  chengdong/teach
//党建活动：  chengdong/member
//自主活动：  chengdong/activity

//    $input = array(
//        'fileArr'=>$_FILES,
//        'imgName'=>'uploadinput',
//        'maxSize'=>0,
//        'overWrite'=>0,
//        'savePath'=>'teach',
//        'thumb'=>1,
//        'thumbWidth'=>10,
//        'thumbHeight'=>10
//    );
//    uploadImg($input);
    $fileArr = $inputArr['fileArr'];//必填参数,$_FILES
    $imgName = $inputArr['imgName'];//必填参数,表单中input的名字
    $savePath = (isset($inputArr['savePath']) && !empty($inputArr['savePath'])) ? $inputArr['savePath'] : "upload";
    $savePath = 'chengdong/image/'.$savePath;//拼接上幼儿园的id
    $maxSize = (isset($inputArr['maxSize']) && !empty($inputArr['maxSize'])) ? $inputArr['maxSize'] : 0;//文件大小限制kb
    $overWrite = (isset($inputArr['overWrite']) && !empty($inputArr['overWrite'])) ? $inputArr['overWrite'] : 0;//0 不允许  1 允许
    $thumb = (isset($inputArr['thumb']) && !empty($inputArr['thumb'])) ? $inputArr['thumb'] : 0;//是否生成缩率图 0否 1是
    $thumbWidth = (isset($inputArr['thumbWidth']) && !empty($inputArr['thumbWidth'])) ? $inputArr['thumbWidth'] : 130;//缩率图宽,默认130
    $thumbHeight = (isset($inputArr['thumbHeight']) && !empty($inputArr['thumbHeight'])) ? $inputArr['thumbHeight'] : 130;//缩率图高 默认130
    if($fileArr[$imgName]['name'] <> ""){
        //包含上传文件类
        require_once(app_path() . '/' . 'Libs/image/upload.php');
        //创建目录
        makeDirectory($savePath,0,1);
        //允许的文件类型
        $fileFormat = array('gif','jpg','jpge','png');
        //初始化上传类
        $f = new Upload( $savePath, $fileFormat, $maxSize, $overWrite);

        if($thumb){
           $f->setThumb(1,$thumbWidth,$thumbHeight);
        }else{
            $f->setThumb(0);
        }
        //后面的0表示不更改文件名，若为1，则由系统生成随机文件名
        if (!$f->run($imgName,1)){
            $returnArr['status'] = 10001;
            $returnArr['msg'] = '图片上传失败';
            return $returnArr;//上传失败
            //通过$f->errmsg()只能得到最后一个出错的信息，
            //详细的信息在$f->getInfo()中可以得到。
            //echo   $f->errmsg()."<br>\n";
        }else{

            $uploadName = array();
            foreach($f->getInfo() as $v){
                $uploadName[] = $v['saveName'];
            }
            $returnArr['status'] = 200;
            $returnArr['msg'] = '图片上传成功';
            $returnArr['uploadNme'] = $uploadName;
            return $returnArr;
        }
    }
}

function makeDirectory($directory,$type=0) {
    //savePath 规则：班级相册 album/具体班级/相册名称    album/samllone/春游
    if(!$type){
        $directoryName = 'chengdong'.'/image/'.$directory;
    }else{
        $directoryName = $directory;
    }


    $directoryName = str_replace("\\","/",$directoryName);
    $dirNames = explode('/', $directoryName);
    $total = count($dirNames) ;
    $temp = '';
    for($i=0; $i<$total; $i++) {
        $temp .= $dirNames[$i].'/';
        if (!is_dir($temp)) {
            $oldmask = umask(0);
            if (!mkdir($temp, 0777)) exit("不能建立目录 $temp");
            umask($oldmask);
        }
    }
    return true;
}

/**
 * edit by neek li
 * @param $str //要取的变量名
 * @param string $convert  //是否是整形强转
 * @param string $default  //当为空时的默认值
 * @return int|string
 */
    function I($str,$convert='',$default=''){

        if(isset($_POST[$str]) && !empty($_POST[$str])){
            $res = trim($_POST[$str]);
            if($convert=='int'){
                $res = (int)$res;
            }
            return $res;
        }

        $res = $default ? $default : '';
        return $res;
    }

/**
 * created by neek li
 * @param $inputArr
 * @param int $checkType
 * @return array
 */
function checkAll($inputArr,$checkType=0){

    if(count($inputArr)==1 && array_key_exists('access_token',$inputArr) && !$checkType){
        $return['status'] = 0;
        $return['msg'] = '请提交合法数据';
        extjson($return);
    }

    $resArr = array();
    $ra=Array('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/','/script/','/javascript/','/vbscript/','/expression/','/applet/','/meta/','/xml/','/blink/','/link/','/style/','/embed/','/object/','/frame/','/layer/','/title/','/bgsound/','/base/','/onload/','/onunload/','/onchange/','/onsubmit/','/onreset/','/onselect/','/onblur/','/onfocus/','/onabort/','/onkeydown/','/onkeypress/','/onkeyup/','/onclick/','/ondblclick/','/onmousedown/','/onmousemove/','/onmouseout/','/onmouseover/','/onmouseup/','/onunload/');
    foreach($inputArr as $k=>$value){
        if(is_array($value)){
            foreach($value as $k1 =>$v){
                $v  = preg_replace($ra,'',$v);     //删除非打印字符，粗暴式过滤xss可疑字符串
                $resArr[$k][$k1]  = htmlentities(strip_tags($v)); //去除 HTML 和 PHP 标记并转换为 HTML 实体
            }
        }else{
            $value  = preg_replace($ra,'',$value);     //删除非打印字符，粗暴式过滤xss可疑字符串
            $resArr[$k]  = htmlentities(strip_tags($value)); //去除 HTML 和 PHP 标记并转换为 HTML 实体
        }

    }
    return $resArr;
}

/**
 * created by neek li
 * json输出
 * @param $return
 */
function extjson($return){
    header("Content-type: application/json");
    $string = json_encode($return);
    echo $string;

//    logOutput($string);
    exit;
}

/**
 * created by neek li
 * 入参日志
 */
function logInput(){
    $uri = $_SERVER['REQUEST_URI'];
    $postStr = serialize($_POST);
    $str = "\r\n\r\n".date("Y-m-d G:i:s")."\r\n";
    $str .= "\r\n".$uri."\r\n";
    $str .= "\r\n".$postStr."\r\n";
    $str .= "\r\n==================================================================================================\r\n";

    file_put_contents('./input.log',var_export($str,true),FILE_APPEND);
}

/**
 * created by neek li
 * 出参日志
 * @param $strJson
 */
function logOutput($strJson){
    $str = "\r\n\r\n".date("Y-m-d G:i:s")."\r\n";
    $str .= "\r\n".$strJson."\r\n";
    $str .= "\r\n==================================================================================================\r\n";

    file_put_contents('./output.log',var_export($str,true),FILE_APPEND);
}

/**
 * created by neek li
 * @param $string 明文 或 密文
 * @param string $operation  DECODE表示解密,其它表示加密
 * @param int $expiry
 * @param string $key 密匙
 * @return string 密文有效期
 */
function authcode($string, $operation = 'DECODE', $expiry = 0,$key = '%_dm%$s&e2yeeyifo') {
    // 动态密匙长度，相同的明文会生成不同密文就是依靠动态密匙
    // 加入随机密钥，可以令密文无任何规律，即便是原文和密钥完全相同，加密结果也会每次不同，增大破解难度。
    // 取值越大，密文变动规律越大，密文变化 = 16 的 $ckey_length 次方
    // 当此值为 0 时，则不产生随机密钥
    if($expiry<1){
        $expiry = 86400*30;
    }
    $ckey_length = 16;
    // 密匙
    $key = md5($key);
    // 密匙a会参与加解密
    $keya = md5(substr($key, 0, 16));
    // 密匙b会用来做数据完整性验证
    $keyb = md5(substr($key, 16, 16));
    // 密匙c用于变化生成的密文
    $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : '';
    // 参与运算的密匙
    $cryptkey = $keya.md5($keya.$keyc);
    $key_length = strlen($cryptkey);
    // 明文，前10位用来保存时间戳，解密时验证数据有效性，10到26位用来保存$keyb(密匙b)，解密时会通过这个密匙验证数据完整性
    // 如果是解码的话，会从第$ckey_length位开始，因为密文前$ckey_length位保存 动态密匙，以保证解密正确
    $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string;
    $string_length = strlen($string);
    $result = '';
    $box = range(0, 255);
    $rndkey = array();
    // 产生密匙簿
    for($i = 0; $i <= 255; $i++) {
        $rndkey[$i] = ord($cryptkey[$i % $key_length]);
    }
    // 用固定的算法，打乱密匙簿，增加随机性，好像很复杂，实际上并不会增加密文的强度
    for($j = $i = 0; $i < 256; $i++) {
        $j = ($j + $box[$i] + $rndkey[$i]) % 256;
        $tmp = $box[$i];
        $box[$i] = $box[$j];
        $box[$j] = $tmp;
    }
    // 核心加解密部分
    for($a = $j = $i = 0; $i < $string_length; $i++) {
        $a = ($a + 1) % 256;
        $j = ($j + $box[$a]) % 256;
        $tmp = $box[$a];
        $box[$a] = $box[$j];
        $box[$j] = $tmp;
        // 从密匙簿得出密匙进行异或，再转成字符
        $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
    }
    if($operation == 'DECODE') {
        // substr($result, 0, 10) == 0 验证数据有效性
        // substr($result, 0, 10) - time() > 0 验证数据有效性
        // substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16) 验证数据完整性
        // 验证数据有效性，请看未加密明文的格式
        if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) {
            return substr($result, 26);
        } else {
            return '';
        }
    } else {
        // 把动态密匙保存在密文里，这也是为什么同样的明文，生产不同密文后能解密的原因
        // 因为加密后的密文可能是一些特殊字符，复制过程可能会丢失，所以用base64编码
        return $keyc.str_replace('=', '', base64_encode($result));
    }

}

/**
 * @param $key
 * @param $value
 * @return session值
 */
function _session($key,$value=false){
    session_save_path(storage_path()."/framework/sessions");
    //判断session是否打开
    if(!isset($_SESSION))
    {
        session_start();
    }
    //获取值
    if($value==false){
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
        }else{
            return NULL;
        }
    }
    //删除值
    if($value==null){
        unset($_SESSION[$key]);
        return null;
    }
    //设置值
    if($value!=""){
        return $_SESSION[$key] = $value;
    }
}

/**
 * @param Array $file $_FILES['Filedata']
 * @param Array $array //传入的excel对应的表name值
 * @param  $table //是导入的表
 * @param  Array $insert 导入记录时 额外加的值
 * @param  Array $info 导入记录时 判断是否有相同的存在
 * @param table调用这个函数需把对应模型中的table改成public
 * @return
 */
function xlsImport($file,$array,$table,$insert=null,$info=array()){
    $prefix = \Illuminate\Support\Facades\Config::get('database.connections.mysql.prefix');
    $table = '\App\Http\Model\\'.$table;
    $table = new $table;
    $table_s = $prefix.$table->table;
    include_once app_path().'/Libs/attachment/XLS.php';
    $file = \XLS::upload(base_path().'/storage/app/public',$file,'file','importXLS');
    require_once(app_path().'/Libs/PHPExcel/PHPExcel.php');
    $objPHPExcel = \PHPExcel_IOFactory::load($file['file_dir']);
    $j=0;
    $k=0;
    $return = array();
    foreach($objPHPExcel->getWorksheetIterator() as $sheet){
        foreach($sheet->getRowIterator() as $row){
            if($row->getRowIndex()<2){
                continue;
            }
            $i=0;
            //判断导入文件列数和设定列是否一致
            $currentSheet = $objPHPExcel->getSheet(0);
            $allColumn = $currentSheet->getHighestColumn();
            $allColumn = PHPExcel_Cell::columnIndexFromString($allColumn);
            $j++;
            if(count($array)!=$allColumn){
                $return['status']= 10009;
                $return['msg'] = '上传文件列数和设定列数不一致';
                extjson($return);
            }
            foreach($row->getCellIterator() as $cell){
                $data = $cell->getValue();
                $result[$array[$i]]=$data;
                $i++;
            }
            $sql = "select * from " .$table_s;
            if (array_has($info, ['where'])) {
                for ($i1 = 0; $i1 < count($info['where']); $i1++) {
                    if ($i1 == 0) {
                        $sql .= " where " . $info['where'][$i1]['key'] . ' ' . $info['where'][$i1]['char'] . ' ' . (strlen($info['where'][$i1]['value']) ? $info['where'][$i1]['value'] : $result[$info['where'][$i1]['key']]);
                    } else {
                        $sql .= " and " . $info['where'][$i1]['key'] . ' ' . $info['where'][$i1]['char'] . ' ' . (strlen($info['where'][$i1]['value']) ? $info['where'][$i1]['value'] : $result[$info['where'][$i1]['key']]);
                    }
                }
                $employee = DB::select($sql);
                if($employee){
                    $return['status']=10010;
                    $return['msg']="该数据已经存在";
                }else{
                    $k++;
                    //dd($insert);
                    if($insert!=null){
                        foreach($insert as $key=>$v){
                            $result[$key]= $v;
                        }
                    }
                    $table::create($result);
                }
            }else{
                if($insert!=null){
                    foreach($insert as $key=>$v){
                        $result[$key]= $v;
                    }
                }
                $table::create($result);
                $k++;
            }

        }
    }
    if(isset($return['status'])&&$return['status']==10010){
        $return['msg']=  "总共导入".$j."条数据其中".$k."条数据成功插入数据库,部分数据已存在！";
    }else{
        $return['status'] = 200;
        $return['msg'] = '数据导入成功！';
    }
    extjson($return);
}

/**
 * @param object $datas 数据对象
 * @param Array $array //导出的excel $array['name']  = 姓名 name对应数据表字段 姓名对应excel表的第一行值
 * @return
 */
function xlsExport($datas,$array,$filename){
    require_once(app_path().'/Libs/PHPExcel/PHPExcel.php');
    $objPHPExcel = new \PHPExcel();
    $objSheet = $objPHPExcel->getActiveSheet();
    //extjson($allColumn);
    $i=0;
    foreach($array as $key=>$v){
        $objSheet->setCellValue(PHPExcel_Cell::stringFromColumnIndex($i)."1",$v);
        $i++;
    }
    //$objSheet->setCellValue("A1","姓名")->setCellValue("B1","职位")->setCellValue("C1","工号")->setCellValue("D1","手机号")->setCellValue("E1","邮箱");
    $j=2;
    foreach ($datas as $data) {
        $i=0;
        foreach($array as $key=>$v){
            $objSheet->setCellValue(PHPExcel_Cell::stringFromColumnIndex($i).$j,$data->{$key});
            $i++;
        }
        $j++;
    }
//    $j=2;
//    foreach ($datas as $data) {
//        $objSheet->setCellValue("A".$j,$data->name)->setCellValue("B".$j,$data->position)->setCellValue("C".$j,$data->job_number)->setCellValue("D".$j,$data->phone)->setCellValue("E".$j,$data->email);
//        $j++;
//    }
    $type='Excel5';
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel,$type);//生成excel文件
    //$objWriter->save();//保存文件
//    $filename = time().'.xls';
    if($type == 'Excel5'){
        header('Content:Type: application/vmd.ms-excel');
    }else{
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }
    header('Content-Disposition: attachment;filename='.$filename);
    header('Cache-Control: max-age=0');
    $objWriter->save("php://output");
}
/**
 * @param array $datas 数组数据
 * @param int $page 当前页数
 * @param int $to   当前条数
 * @return
 */

function cusPage($data,$page = 1, $to = 10){
    if(!is_numeric($page)){
        $page = 1;
    }
    $last_page = ((int)ceil(count($data)/$to));
    $collection = collect($data);
    $data1 = $collection->forPage($page, $to);
    $return['manager_list'] =  $data1;
    $return['current_page'] = $page;
    $return['last_page'] = $last_page;
    $return['totall'] = count($data);
    $return['to'] = $to;
    return $return;
}

/**
 * 生成订单编号(两位随机 + 从2000-01-01 00:00:00 到现在的秒数+微秒+会员ID%1000)
 * 长度 =2位 + 10位 + 3位 + 3位  = 19位
 * 1000个会员同一微秒提订单，重复机率为1/1000
 * @return string
 */

function makeOrderSn($order_type,$use_no){
    return $order_type.mt_rand(10,99)
    . sprintf('%010d',time() - 946656000)
    . sprintf('%03d', (float) microtime() * 1000)
    . sprintf('%03d', (int) $use_no % 1000);
}


function err_download(){
    $err = session()->pull('error');//得到session中error对应的数据
    if($err){
        Excel::create('导入失败数据',function($excel) use($err){
            $excel->sheet('列表', function($sheet) use($err) {

                $sheet->rows($err);

            })->download('xls');
        });
    }
}

function millisecond()
{
    return ceil(microtime(true) * 1000);
    //ceil() 函数向上舍入为最接近的整数   microtime()返回当前 Unix 时间戳的微秒数：当设置为 TRUE 时，规定函数应该返回浮点数，否则返回字符串。默认为 FALSE。
}

//对象转化成数组的方法
function object2array($object) {
    if (is_object($object)) {
        foreach ($object as $key => $value) {
            $array[$key] = $value;
        }
    }
    else {
        $array = $object;
    }
    return $array;
}

//无限极分类
function getTree($arr, $pid = 0 ,$level = 0 ) {
    static $result = array();
    foreach ($arr as $v) {
        if ($v['pid'] == $pid) {
            $v['level'] = $level;
            $result[] = $v;
            getTree($arr,$v['id'],1);
        }
    }
    return $result;
}
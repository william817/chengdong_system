<?php
class XLS{
    /**
     * upload
     *
     * 文件上传
     *
     * @param String $path e.g. Zend_Registry::get('upload')
     * @param Array $files e.g. $_FILES['Filedata']
     * @param String $dir e.g. $_POST['dir']
     *
     * return Array $msg e.g. if($msg['error'])
     */
    static function upload($savePath,$files,$dir='file',$comment='',$filename)
    {
        $msg=array();
        //文件保存目录路径
        $save_path = $savePath;
        //文件保存目录URL
        $save_url = $savePath;
        //定义允许上传的文件扩展名
        $ext_arr = array(
            'image' => array('gif', 'jpg', 'jpeg', 'png', 'bmp'),
            'flash' => array('swf', 'flv'),
            'media' => array('swf', 'flv', 'mp3', 'wav', 'wma', 'wmv', 'mid', 'avi', 'mpg', 'asf', 'rm', 'rmvb'),
            'file' => array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'htm', 'html', 'txt', 'zip', 'rar', 'gz', 'bz2'),
        );
        //最大文件大小
        $max_size = 1000000;
        $save_path = $save_path . '/';
        //有上传文件时
        if (empty($_FILES) === false) {
            //原文件名
            $file_name = $files['name'];
            //服务器上临时文件名
            $tmp_name = $files['tmp_name'];
            //文件大小
            $file_size = $files['size'];
            //目录名
            $dir_name = empty($dir) ? 'image' : trim($dir);
            //检查文件名
            if (!$file_name) {
                $msg['error'] = "请选择文件。";
            }
           //检查是否已上传
            else if (@is_uploaded_file($tmp_name) === false) {
                $msg['error'] = "临时文件可能不是上传文件。请重新上传";
            }
            //检查文件大小
            else if ($file_size > $max_size) {
                $msg['error'] = "上传文件大小超过限制。";
            }
            //检查目录名
            else if (empty($ext_arr[$dir_name])) {
                $msg['error'] = "文件类型不正确。";
            }
            else
            {
                //获得文件扩展名
                $temp_arr = explode(".", $file_name);
                $file_ext = array_pop($temp_arr);
                $file_ext = trim($file_ext);
                $file_ext = strtolower($file_ext);
                //检查扩展名
                if (in_array($file_ext, $ext_arr[$dir_name]) === false) {
                    $msg['error'] = "上传文件扩展名是不允许的扩展名。n只允许" . implode(",", $ext_arr[$dir_name]) . "格式。";
                }
                else
                {
                    //创建文件夹
                    $save_path = substr( str_replace("\\","/", $save_path) , -1) == "/" ? $save_path : $save_path."/";
                    if (!file_exists($save_path)) {
                        mkdir($save_path);
                    }

                    //新文件名,加上对文件的备注
                    $new_file_name = date("YmdHis")."$filename" . '_' . rand(10000, 99999) . '.' . $file_ext;
                    //移动文件
                    $file_path = $save_path . $new_file_name;
                    if (move_uploaded_file($tmp_name, $file_path) === false) {
                        $msg['error'] = "上传文件失败。";
                    }

                    @chmod($file_path, 0644);
                    $file_url = $save_url .'/'. $new_file_name;
                    $msg['file_dir'] = $file_url;
                    $msg['file_name'] = $new_file_name;
                    $msg['file_size'] = $file_size;
                }//检查扩展名
            }//目录正确性

            return $msg;
        }
    }
//文件上传
}
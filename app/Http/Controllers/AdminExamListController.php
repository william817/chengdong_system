<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Model\AdminExamList;
use App\Http\Controllers\CommonController;
class AdminExamListController extends CommonController
{
    public function examListAdd()
    {
        AdminExamList::examListAdd(Input::all());
    }

    public function examListIndex()
    {
        AdminExamList::examListIndex();
    }

    public function examListUpdate()
    {
        $input = Input::all();
        if (Input::except('id')) {
            AdminExamList::examListUpdate(Input::all());
        } else {
            AdminExamList::examListInfo(Input::all());
        }
    }

    public function examListDelete()
    {
        AdminExamList::examListDelete(Input::all());
    }

    public function examListHistory()
    {
        AdminExamList::examListHistory(Input::all());
    }

    public function startExam()
    {
        AdminExamList::startExam(Input::all());
    }

    public function endExam()
    {
        AdminExamList::endExam(Input::all());
    }
}

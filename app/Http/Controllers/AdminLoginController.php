<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Model\AdminLogin;
use Illuminate\Support\Facades\Input;
class AdminLoginController extends Controller
{
    //登陆
    public function login()
    {
        $return = AdminLogin::login(Input::all());
        return $return;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Model\AdminExam;
use App\Http\Controllers\CommonController;
class AdminExamController extends CommonController
{
    public function examAdd()
    {
        $input = Input::all();
//        $input['exam'] = array (
//            array (
//                'id' => 1,
//                'question' => '中国的现任主席是谁？',
//                'answer' => array(
//                    '0' =>'A:邓小平',
//                    '1' =>'B:江泽民',
//                    '2' =>'C:胡锦涛',
//                    '3' =>'D:习近平'
//                ),
//                'result' => 'D',
//                'grade' => 5,
//            ),
//            array (
//                'id' => 1,
//                'question' => '中国的现任主席是谁？',
//                'answer' => array(
//                    "0" =>"A:邓小平",
//                    "1" =>"B:江泽民",
//                    "2" =>"C:胡锦涛",
//                    "3" =>"D:习近平"
//                ),
//                'result' => 'D',
//                'grade' => 5,
//            ),
//        );
        AdminExam::examAdd($input);
    }

    public function examList()
    {
        AdminExam::examList(Input::all());
    }

    public function examUpdate()
    {
        $input = Input::all();
//        $input['exam'] = array (
//            array (
//                'times_id' => 1,
//                'id'=>18,
//                'question' => '中国的现任主席是谁？',
//                'answer' => array(
//                    '0' =>'A:邓小平',
//                    '1' =>'B:江泽民',
//                    '2' =>'C:胡锦涛',
//                    '3' =>'D:习近平'
//                ),
//                'result' => 'D',
//                'grade' => 15,
//            ),
//            array (
//                'times_id' => 1,
//                'question' => '中国的现任主席是谁？',
//                'answer' => array(
//                    "0" =>"A:邓小平",
//                    "1" =>"B:江泽民",
//                    "2" =>"C:胡锦涛",
//                    "3" =>"D:习近平"
//                ),
//                'result' => 'D',
//                'grade' => 10,
//            ),
//        );
//        $input['exam'] = json_encode($input['exam']);
        if (Input::except('times_id')) {
            AdminExam::examUpdate($input);
        } else {
            AdminExam::examInfo($input);
        }
    }

    public function examInfo()
    {
        AdminExam::examInfo(Input::all());
    }
    public function examDelete()
    {
        AdminExam::examDelete(Input::all());
    }

    public function examScoreAdd()
    {
        AdminExam::examScoreAdd(Input::all());
    }



}

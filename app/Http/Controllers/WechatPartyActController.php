<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Model\WechatPartyAct;
class WechatPartyActController extends Controller
{
    //综合选修列表
    public function partyChoList()
    {
        $return = WechatPartyAct::partyChoList(Input::all());
        extjson($return);
    }

    //综合必修列表
    public function partyReqList()
    {
        $return = WechatPartyAct::partyReqList(Input::all());
        extjson($return);
    }

    /**
     * partyReqInfo
     * 活动详情
     */
    public function partyInfo()
    {
        $return = WechatPartyAct::partyInfo(Input::all());
        extjson($return);
    }
}

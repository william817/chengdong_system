<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Model\WechatExamList;
class WechatExamListController extends Controller
{
    public function examList()
    {
        $return = WechatExamList::examList();
        extjson($return);
    }

    public function examListInfo()
    {
        $return = WechatExamList::examListInfo(Input::all());
        extjson($return);
    }

    public function examListSub()
    {
        $return = WechatExamList::examListSub(Input::all());
        extjson($return);
    }

    public function examListCheck()
    {
        $return = WechatExamList::examListCheck(Input::all());
        extjson($return);
    }

}

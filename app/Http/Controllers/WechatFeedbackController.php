<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Model\WechatFeedback;

class WechatFeedbackController extends Controller
{
    /**
     * feedbackSave
     * 增加反馈
     */
    public function feedbackSave()
    {
        $return = WechatFeedback::feedbackSave(Input::all());
        extjson($return);
    }

    /**
     * feedbackSave
     * 增加反馈
     */
    public function feedbackList()
    {
        $return = WechatFeedback::feedbackList(Input::all());
        extjson($return);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Model\WechatZanRecord;
use Illuminate\Support\Facades\Input;

class WechatZanRecordController extends Controller
{
    /**
     * zanAdd
     * 点赞
     * @return array
     */
    public function zanAdd()
    {
        $return = WechatZanRecord::zanAdd(Input::all());
        extjson($return);
    }
}

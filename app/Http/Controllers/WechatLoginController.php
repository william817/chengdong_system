<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Model\WechatLogin;
use Illuminate\Support\Facades\Input;
use App\Http\Model\WechatUser;
class WechatLoginController
{
    public function getCode(){

        $REDIRECT_URI=config('services.weixin.redirect').'get/openid';
        $appid = config('services.weixin.app_id');
        $scope='snsapi_base';
        //$scope='snsapi_userinfo';//需要授权
        $redirect_uri=urlencode($REDIRECT_URI);
        $url='https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$appid.'&redirect_uri='.$redirect_uri.'&response_type=code&scope='.$scope.'&state=123#wechat_redirect';
        header("Location:".$url);
//        exit;
//        return redirect($url);
    }
    public function getOpenid(){
        //Login::getOpenid(Input::all());
        $code = Input::get('code');
        $appid = config('services.weixin.app_id');
        $secret = config('services.weixin.app_secret');
        $weixin =  file_get_contents("https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$appid."&secret=".$secret."&code=".$code."&grant_type=authorization_code");//通过code换取网页授权access_token
        $jsondecode = json_decode($weixin); //对JSON格式的字符串进行编码
        $array = get_object_vars($jsondecode);//转换成数组
 
        //$access_token = $array['access_token'];
        $openid = $array['openid'];
        $userInfo = WechatUser::where('openid',$openid)->first();
        if (!$userInfo){

            $user['openid'] = $openid;
            $user['status'] = 3;
            $user['create_time'] = time();
            $user_info = WechatUser::create($user);

            session()->put('openId',$openid);
            session()->put('userid',$user_info->id);
            session()->save();
//            dd(session()->get('userid'));
            header("Location:".config('services.weixin.redirect')."my/sub_info");

            die;
        } else {
            
            session()->put('openId',$openid);
            session()->put('userid',$userInfo->id);
//            dd(session()->get('userid'));
            session()->save();

//            dd(session()->get('openId'));

            if ($userInfo->status == 1) {
                header("Location:".config('services.weixin.redirect')."index");
//                dd(config('services.weixin.redirect')."index");
//               return redirect(config('services.weixin.redirect')."index");

                die;
            } else {
                header("Location:".config('services.weixin.redirect')."my/sub_info");
//                header("Location:http://www.bainiuhz.com/chengdong/manage/public/my/sub_info");
                die;
            }

        }

        //dd(session()->get('openid'));
    }

}

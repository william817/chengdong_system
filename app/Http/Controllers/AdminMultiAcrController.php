<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Model\AdminMultiAcr;
use App\Http\Controllers\CommonController;
use App\Http\Model\AdminFeedback;
use App\Http\Model\AdminSignIn;
use App\Http\Model\AdminSignUp;
class AdminMultiAcrController extends CommonController
{
    //综合必修增加
    public function multiReqSave()
    {
        $return = AdminMultiAcr::multiReqSave(Input::all());
        extjson($return);
    }

    //综合必修列表
    public function multiReqList()
    {
        $return = AdminMultiAcr::multiReqList(Input::all());
        extjson($return);
    }

    //综合必修编辑
    public function multiReqEdit()
    {
        if (Input::except('id')){
            $return = AdminMultiAcr::multiReqEdit(Input::all());
        } else {
            $return = AdminMultiAcr::multiReqInfo(Input::all());
        }
        extjson($return);
    }

    //综合必修列表
    public function multiReqInfo()
    {
        $return = AdminMultiAcr::multiReqInfo(Input::all());
        extjson($return);
    }

    //综合必修列表
    public function multiReqDelete()
    {
        $return = AdminMultiAcr::multiReqDelete(Input::all());
        extjson($return);
    }



    //必修课反馈通过列表
    public function multiReqFeedList()
    {
        $return = AdminFeedback::multiReqFeedList(Input::all());
        extjson($return);
    }

    //必修课反馈不通过列表
    public function multiReqFeedNoList()
    {
        $return = AdminFeedback::multiReqFeedNoList(Input::all());
        extjson($return);
    }

    //必修课反馈通过/不通过接口
    public function multiReqFeed()
    {
        $return = AdminFeedback::multiReqFeed(Input::all());
        return $return;
    }

    //必修课反馈删除接口
    public function multiReqFeedDelete()
    {
        $return = AdminFeedback::multiReqFeedDelete(Input::all());
        return $return;
    }

    //综合选修增加
    public function multiChoSave()
    {
        $return = AdminMultiAcr::multiChoSave(Input::all());
        extjson($return);
    }

    //综合选修列表
    public function multiChoList()
    {
        $return = AdminMultiAcr::multiChoList(Input::all());
        extjson($return);
    }

    //综合选修编辑
    public function multiChoEdit()
    {
        if (Input::except('id')){
            $return = AdminMultiAcr::multiChoEdit(Input::all());
        } else {
            $return = AdminMultiAcr::multiChoInfo(Input::all());
        }
        extjson($return);
    }

    //综合选修列表
    public function multiChoInfo()
    {
        $return = AdminMultiAcr::multiChoInfo(Input::all());
        extjson($return);
    }

    //综合选修列表
    public function multiChoDelete()
    {
        $return = AdminMultiAcr::multiChoDelete(Input::all());
        extjson($return);
    }



    //选修课反馈通过列表
    public function multiChoFeedList()
    {
        $return = AdminFeedback::multiChoFeedList(Input::all());
        extjson($return);
    }

    //选修课反馈不通过列表
    public function multiChoFeedNoList()
    {
        $return = AdminFeedback::multiChoFeedNoList(Input::all());
        extjson($return);
    }

    //选修课反馈通过/不通过接口
    public function multiChoFeed()
    {
        $return = AdminFeedback::multiChoFeed(Input::all());
        extjson($return);
    }

    //选修课反馈删除接口
    public function multiChoFeedDelete()
    {
        $return = AdminFeedback::multiChoFeedDelete(Input::all());
        extjson($return);
    }

    //必修课签到详情接口
    public function multiReqSignIn()
    {
        $return = AdminSignIn::multiReqSignIn(Input::all());
        extjson($return);
    }

    //选修课签到详情接口
    public function multiChoSignIn()
    {
        $return = AdminSignIn::multiChoSignIn(Input::all());
        extjson($return);
    }

    //选修课报名详情接口
    public function multiChoSignUp()
    {
        $return = AdminSignUp::multiChoSignUp(Input::all());
        extjson($return);
    }

    //发布/撤销详情接口
    public function multiRelease()
    {
        $return = AdminMultiAcr::multiChoRelease(Input::all());
        extjson($return);
    }
}

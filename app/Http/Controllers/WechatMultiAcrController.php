<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Model\WechatMultiAcr;
use Illuminate\Support\Facades\Input;
class WechatMultiAcrController extends Controller
{
    //综合选修列表
    public function multiChoList()
    {
        $return = WechatMultiAcr::multiChoList(Input::all());
        extjson($return);
    }

    //综合必修列表
    public function multiReqList()
    {
        $return = WechatMultiAcr::multiReqList(Input::all());
        extjson($return);
    }

    /**
     * multiReqInfo
     * 活动详情
     */
    public function multiInfo()
    {
        $return = WechatMultiAcr::multiInfo(Input::all());
        extjson($return);
    }
}

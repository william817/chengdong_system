<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Model\WechatExam;
class WechatExamController extends Controller
{
    public function examInfo()
    {
        $return = WechatExam::examInfo(Input::all());
        extjson($return);
    }
}

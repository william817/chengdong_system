<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Model\WechatOption;
use Illuminate\Support\Facades\Input;
class WechatOptionController extends Controller
{
    public function optionList()
    {
        $return = WechatOption::optionList();
        extjson($return);
    }

    public function optionInfo()
    {
        $return = WechatOption::optionInfo(Input::all());
        extjson($return);
    }

    public function optionSave()
    {
        $return = WechatOption::optionSave(Input::all());
        extjson($return);
    }
}

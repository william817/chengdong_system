<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\WechatLoginController;
use App\Http\Model\WechatUser;
use Illuminate\Support\Facades\Input;

class WechatUserController extends Controller
{
    //查询是否存在该用户
    public function checkOpenid($url = 'index'){
        if(session()->get('openId')){
            $openid =session()->get('openId');
            $userInfo = WechatUser::where('openid',$openid)->first();
            if($userInfo){
                session()->put('userid',$userInfo->id);
                session()->save();
                if ($userInfo->status != 1) {
                    header("Location:".config('services.weixin.redirect')."my/sub_info"); //这里应该跳转到个人信息页面
                    die;
                }
                $jump_url = config('services.weixin.redirect').$url;
//                header("Location:http://www.bainiuhz.com/chengdong/manage/public/index"); //这里应该跳转到个人信息页面
                header("Location:".$jump_url); //这里应该跳转到个人信息页面
                die;
            }else{
                $user['openid'] = $openid;
                $user['status'] = 3;
                $user['create_time'] = time();
                $user_info = WechatUser::create($user);
                $user_id = $user_info->id;
                session()->put('userid',$user_id);
                session()->save();
                header("Location:".config('services.weixin.redirect')."my/sub_info");  //这里应该跳转到个人信息页面
                die;
            }
        }else{
            $getOpenid = new WechatLoginController();
            return $getOpenid->getCode($url);
        }
    }
    //首次提交个人信息保存
    public function submitUserInfo()
    {
        $return = WechatUser::submitUserInfo(Input::all());
        extjson($return);
    }

    //首次提交个人信息保存
    public function UserEdit()
    {
        $return = WechatUser::UserEdit(Input::all());
        extjson($return);
    }

    public function userPass()
    {
        $user_id = session()->get('userid');
        $user_info = WechatUser::where('id',$user_id)->first();
        $return['status'] = 200;
        $return['data'] = $user_info;
        extjson($return);
    }

}

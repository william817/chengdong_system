<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Model\WechatScoreRecord;

class WechatScoreRecordController extends Controller
{
    public function scoreRecord()
    {
        $return = WechatScoreRecord::scoreRecord();
        extjson($return);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Model\WechatShop;
use Illuminate\Support\Facades\Input;
class WechatShopController extends Controller
{
    public function shopList()
    {
        $return = WechatShop::shopList();
        extjson($return);
    }

    public function shopExchange()
    {
        $return = WechatShop::shopExchange(Input::all());
        extjson($return);
    }
}

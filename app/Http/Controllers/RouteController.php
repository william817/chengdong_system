<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\WechatLoginController;
use App\Http\Controllers\WechatUserController;
use Illuminate\Support\Facades\Input;
use App\Http\Model\WechatUser;
use App\Http\Controllers\WechatBaseController;
class RouteController extends WechatBaseController
{
    public function index()
    {
		return view('index');
    }

    public function holdOn()
    {
        return view('holdOn');
    }

    public function integralConversion()
    {
        return view('chengdong.integral.conversion');
    }


    public function integralIntegral()
    {
        return view('chengdong.integral.integral');
    }

    public function integralRank()
    {
        return view('chengdong.integral.rank');
    }

    public function integralRecord()
    {
        return view('chengdong.integral.record');
    }

    public function LearningIndex()
    {
        return view('chengdong.Learning.index');
    }

    public function LearningText()
    {
        return view('chengdong.Learning.text');
    }

    public function LearningMust()
    {
        return view('chengdong.Learning.the_party_leaning_must');
    }

    public function LearningActivity()
    {
        if(!(session()->get('openId'))){
            $url = 'Learning/activity';
            $obj = new WechatUserController();
            $obj->checkOpenid($url);
        } else {
            $status = WechatUser::where('openid',session()->get('openId'))->value('status');
            if ($status!=1){
                $url = 'Learning/activity';
                $obj = new WechatUserController();
                $obj->checkOpenid($url);
            }
        }
        return view('chengdong.Learning.the_party_leaning_must_activity');
    }

    public function LearningFeed()
    {
        return view('chengdong.Learning.the_party_leaning_must_the_feed');
    }

    public function FeedFeedList()
    {
        return view('chengdong.feed.feed_list');
    }

    public function LearningTake()
    {
        return view('chengdong.Learning.the_party_leaning_take');
    }

    public function LearningRoom()
    {
		$id = Input::get('id');	
        if(empty(session()->get('openId'))){
            $url = 'Learning/room?id='.$id;
            $obj = new WechatUserController();
            $obj->checkOpenid($url);
        } else {
            $status = WechatUser::where('openid',session()->get('openId'))->value('status');
            if ($status!=1){
                $url = 'Learning/room?id='.$id;
                $obj = new WechatUserController();
                $obj->checkOpenid($url);
            }
        }
        return view('chengdong.Learning.the_party_leaning_take_room');
    }

    public function myIndex()
    {
        return view('chengdong.my.index');
    }

    public function myInfo()
    {
        return view('chengdong.my.info');
    }

    public function mySub_Info()
    {
        return view('chengdong.my.sub_info');
    }

    public function myIssue()
    {
        return view('chengdong.my.issue');
    }

    public function opinion()
    {
        return view('chengdong.opinion.opinion');
    }

    public function opinionList()
    {
        return view('chengdong.opinion.opinion_list');
    }
    public function opinionInfo()
    {
        return view('chengdong.opinion.opinion_info');
    }

    public function synthIndex()
    {
        return view('chengdong.synth.index');
    }

    public function synthMust()
    {
        return view('chengdong.synth.the_synth_must');
    }

    public function synthActivity()
    {
        if(!(session()->get('openId'))){
            $url = 'synth/activity';
            $obj = new WechatUserController();
            $obj->checkOpenid($url);
        } else {
            $status = WechatUser::where('openid',session()->get('openId'))->value('status');
            if ($status!=1){
                $url = 'synth/activity';
                $obj = new WechatUserController();
                $obj->checkOpenid($url);
            }
        }
        return view('chengdong.synth.the_synth_must_activity');
    }
    
    public function synthFeed()
    {
        return view('chengdong.synth.the_synth_must_activity_feed');
    }

    public function synthTake()
    {
        return view('chengdong.synth.the_synth_take');
    }

    public function synthTakeActivity()
    {

        if(!(session()->get('openId'))){
            $url = 'synth/take_activity';
            $obj = new WechatUserController();
            $obj->checkOpenid($url);
        } else {
            $status = WechatUser::where('openid',session()->get('openId'))->value('status');
            if ($status!=1){
                $url = 'synth/take_activity';
                $obj = new WechatUserController();
                $obj->checkOpenid($url);
            }
        }
        return view('chengdong.synth.the_synth_take_activity');
    }

    public function textFnish()
    {
        return view('chengdong.text.fnish');
    }

    public function textIndex()
    {
        return view('chengdong.text.index');
    }

    public function textWraning()
    {
        return view('chengdong.text.wraning');
    }

    public function textChapter()
    {
        return view('chengdong.text.test.chapter');
    }


}

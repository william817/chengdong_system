<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CommonController;
use App\Http\Model\AdminScore;
use Illuminate\Support\Facades\Input;
class AdminScoreController extends CommonController
{
    public function scoreList()
    {
        $return = AdminScore::scoreList();
        extjson($return);
    }

    public function scoreEdit()
    {
        if (Input::except('id')) {
            $return = AdminScore::scoreEdit(Input::all());
            extjson($return);
        } else {
            $return = AdminScore::scoreInfo(Input::all());
            extjson($return);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CommonController;
use Illuminate\Support\Facades\Input;
use App\Http\Model\AdminOption;
class AdminOptionController extends CommonController
{
    public function optionList()
    {
        $return = AdminOption::optionList();
        extjson($return);
    }

    public function optionInfo()
    {
        $return = AdminOption::optionInfo(Input::all());
        extjson($return);
    }

    public function optionDelete()
    {
        $return = AdminOption::optionDelete(Input::all());
        extjson($return);
    }
}

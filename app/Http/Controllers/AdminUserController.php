<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Model\AdminUser;
use App\Http\Controllers\CommonController;

class AdminUserController extends CommonController
{

    //后台审核通过
    public function examineAndVerify()
    {
        $return = AdminUser::examineAndVerify(Input::all());
        extjson($return);
    }


    //后台用户删除
    public function userDelete()
    {
        $return = AdminUser::userDelete(Input::all());
        extjson($return);
    }

    //审核通过得员工列表
    public function userPassList()
    {
        $return = AdminUser::userPassList();
        return $return;
    }

    //审核不通过得员工列表
    public function userNoPassList()
    {
        $return = AdminUser::userNoPassList();
        return $return;
    }

    //审核通过得员工编辑
    public function userEdit()
    {
        $return = AdminUser::userEdit(Input::all());
        return $return;
    }
    //发送模板消息
    public function sendNotice()
    {
        $return = AdminUser::sendNotice(Input::all());
        return $return;
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Model\People;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class ActiveController extends Controller
{
    public function addPeople(Request $request)
    {
        $people=[
            "name" => $request->input('name'),
            "phone" => $request->input('phone'),
            "age" => $request->input('age'),
            "sex" => $request->input('sex'),
            "job" => $request->input('job'),
            "city" => $request->input('city')
        ];
        People::addPeople($people);
        $return['status'] = 200;
        $return['msg'] = '插入成功';
        extjson($return);

    }

    public function peopleList()
    {
        $result = People::peopleList();
        extjson($result);
    }

    public function face(Request $request)
    {
        $imgbase = $request->input('imgbase');
        $url    = 'https://dtplus-cn-shanghai.data.aliyuncs.com/face/detect';
        $data   = [
            'type'      => 0, //0: 通过url识别，参数image_url不为空；1: 通过图片content识别，参数content不为空
            'image_url' => $imgbase
            //'content' => $imgbase
        ];
        $header = $this->commonHeader($url, $data);
        $head   = [];
        foreach ($header as $k => $v) {
            $head[] = $k . ':' . $v;
        }
        $res = $this->curl($url, json_encode($data), $head);
        echo $res;
    
    }

    public function faceimg(Request $request)
    {
        $imgbase = $request->input('imgbase');
        $url    = 'https://dtplus-cn-shanghai.data.aliyuncs.com/face/detect';
        $data   = [
            'type'      => 1, //0: 通过url识别，参数image_url不为空；1: 通过图片content识别，参数content不为空
            'content' => $imgbase
        ];
        $header = $this->commonHeader($url, $data);
        $head   = [];
        foreach ($header as $k => $v) {
            $head[] = $k . ':' . $v;
        }
        $res = $this->curl($url, json_encode($data), $head);
        echo $res;
    
    }
    
    public function http_header_curl($url, $header, $post = false, $post_data = [])
    {
        $ch = curl_init();//开启会话机制
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //获取数据返回
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true); //启用
        if ($post == true) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 对认证证书来源的检查
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($ch); //执行
        if ($result === false) {
            var_dump(curl_error($ch));
        }
        curl_close($ch); //关闭会话机制

        return $result; //
    }

    public function curl($url, $post_data, $header)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($ch, CURLOPT_POST, 1);   // post数据
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);   // post的变量
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        if (($res = curl_exec($ch)) === false) {
            echo 'Curl error: ' . curl_error($ch);
        } else {
//            echo '操作完成没有任何错误';
        }
        curl_close($ch);

        return $res;
    }

    public function common($gmdate)
    {
        $options = [
            'http' => [
                'header' => [
                    'accept'       => "application/json",
                    'content-type' => "application/json",
                    'date'         => $gmdate,
                ],
                'method' => "POST", //可以是 GET, POST, DELETE, PUT
            ],
        ];

        return $options;
    }

    public function commonHeader($url, $data)
    {
        $gmdate  = gmdate("D, d M Y H:i:s \G\M\T");
        $options = [
            'http' => [
                'header' => [
                    'accept'       => "application/json",
                    'content-type' => "application/json",
                    'date'         => $gmdate,
                ],
                'method' => "POST", //可以是 GET, POST, DELETE, PUT
            ],
        ];
        $header  = [
            'accept'        => "application/json",
            'content-type'  => "application/json",
            'date'          => $gmdate,
            'authorization' => $this->authorization($url, $data, $options),
        ];

        return $header;
    }

    /**
     * 计算authorization
     * @param string $url
     * @param array $data
     * @param array $options
     * @return string
     */
    public function authorization($url, $data, $options)
    {
        $AccessKeySecret = 'JUgSlh1JBCXj2iMMnw7P00HbReZ6gH';
        $AccessKeyId = 'LTAI7Hba035btRoo';
        $http          = $options['http'];
        $header        = $http['header'];
        $bodymd5       = base64_encode(md5(json_encode($data), true));
        $parse         = parse_url($url);
        $path          = $parse['path'];
        $stringToSign  = $http['method'] . "\n" . $header['accept'] . "\n" . $bodymd5 . "\n" . $header['content-type'] . "\n" . $header['date'] . "\n" . $path;
        $Signature     = base64_encode(hash_hmac("sha1", $stringToSign, $AccessKeySecret, true));
        $authorization = "Dataplus {$AccessKeyId}:{$Signature}";

        return $authorization;
    }

    public function share(Request $request)
    {
        $appId = 'wx7610b4be34fd157b';
        $appsecret = '5b8d802d822568b9bb45d57c4d154f26';

        $timestamp = time();
        $jsapi_ticket =$this->make_ticket($appId,$appsecret);
        $nonceStr = $this->make_nonceStr();
        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $signature = $this->make_signature($nonceStr,$timestamp,$jsapi_ticket,$url);
        $return['timestamp']=$timestamp;
        $return['nonceStr']=$nonceStr;
        $return['signature']=$signature;
        extjson($return);
    }

    function make_nonceStr()
    {
        $codeSet = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        for ($i = 0; $i<16; $i++) {
            $codes[$i] = $codeSet[mt_rand(0, strlen($codeSet)-1)];
        }
        $nonceStr = implode($codes);
        return $nonceStr;
    }

    function make_signature($nonceStr,$timestamp,$jsapi_ticket,$url)
    {
        $tmpArr = array(
        'noncestr' => $nonceStr,
        'timestamp' => $timestamp,
        'jsapi_ticket' => $jsapi_ticket,
        'url' => $url
        );
        ksort($tmpArr, SORT_STRING);
        $string1 = http_build_query( $tmpArr );
        $string1 = urldecode( $string1 );
        $signature = sha1( $string1 );
        return $signature;
    }

    function make_ticket($appId,$appsecret)
    {
        // access_token 应该全局存储与更新，以下代码以写入到文件中做示例
        $data = json_decode(file_get_contents("access_token.json"));
        if ($data->expire_time < time()) {
            $TOKEN_URL="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appId."&secret=".$appsecret;
            $json = file_get_contents($TOKEN_URL);
            $result = json_decode($json,true);
            $access_token = $result['access_token'];
            echo "$access_token";
            
            if ($access_token) {
                $data->expire_time = time() + 7000;
                $data->access_token = $access_token;
                $fp = fopen("access_token.json", "w");
                fwrite($fp, json_encode($data));
                fclose($fp);
            }
        }else{
            $access_token = $data->access_token;
        }

        // jsapi_ticket 应该全局存储与更新，以下代码以写入到文件中做示例
        $data = json_decode(file_get_contents("jsapi_ticket.json"));
        if ($data->expire_time < time()) {
            $ticket_URL="https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=".$access_token."&type=jsapi";
            $json = file_get_contents($ticket_URL);
            $result = json_decode($json,true);
            $ticket = $result['ticket'];
            if ($ticket) {
                $data->expire_time = time() + 7000;
                $data->jsapi_ticket = $ticket;
                $fp = fopen("jsapi_ticket.json", "w");
                fwrite($fp, json_encode($data));
                fclose($fp);
            }
        }else{
            $ticket = $data->jsapi_ticket;
        }

        return $ticket;
    }
    public function getConfig()
    {
        //获取参数
        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        //公众号的appid、secret
        $appid = "wx964a35198da58215";
        $secret = "11c154ca519604e7e8d1d3b1d24a93a0";

        //获取access_token的请求地址
        $accessTokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appid."&secret=".$secret;
        //请求地址获取access_token
        $accessTokenJson = file_get_contents($accessTokenUrl);
        $accessTokenObj = json_decode($accessTokenJson);
        $accessToken = $accessTokenObj->access_token;

 
        //获取jsapi_ticket的请求地址
        $ticketUrl = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=".$accessToken."&type=jsapi";
        $jsapiTicketJson = file_get_contents($ticketUrl);
        $jsapiTicketObj = json_decode($jsapiTicketJson);
        $jsapiTicket = $jsapiTicketObj->ticket;
 
        //随机生成16位字符
        $noncestr = str_random(16);
        //时间戳
        $time = time();
        //拼接string1
        $jsapiTicketNew = "jsapi_ticket=".$jsapiTicket."&noncestr=".$noncestr."&timestamp=".$time."&url=".$url;
        //对string1作sha1加密
        $signature = sha1($jsapiTicketNew);
        //存入数据
        $data = [
            'appid' => $appid,
            'timestamp' => $time,
            'nonceStr' => $noncestr,
            'signature' => $signature,
            'jsapiTicket' => $jsapiTicket,
            'url' => $url,
            'jsApiList' => [
                'api' => '#'
            ]
        ];
        //返回
        return json_encode($data);
    }

}

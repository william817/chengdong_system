<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Model\AdminShop;
use App\Http\Controllers\CommonController;

class AdminShopController extends CommonController
{
    public function shopAdd()
    {
        AdminShop::shopAdd(Input::all());
    }

    public function shopList()
    {
        AdminShop::shopList(Input::all());
    }

    public function shopUpdate()
    {
        if (Input::except('id')) {
            AdminShop::shopUpdate(Input::all());
        } else {
            AdminShop::shopInfo(Input::all());
        }
    }

    public function shopDelete()
    {
        AdminShop::shopDelete(Input::all());
    }

    public function shopInfo()
    {
        AdminShop::shopInfo(Input::all());
    }

    public function shopGrounding()
    {
        AdminShop::shopGrounding(Input::all());
    }

    public function shopExchange()
    {
        AdminShop::shopExchange(Input::all());
    }
}

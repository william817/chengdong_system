<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Model\AdminSignUp;
use App\Http\Controllers\CommonController;

class AdminSignUpController extends CommonController
{
    //报名记录
    public function signUp()
    {
        $return = AdminSignUp::signUp(Input::all());
    }
}

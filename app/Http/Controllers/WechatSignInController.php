<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;
use App\Http\Model\WechatSignIn;
use Illuminate\Support\Facades\Input;

class WechatSignInController extends Controller
{
    /**
     * signIn
     * 签到接口
     */
    public function signIn()
    {
        $return = WechatSignIn::signIn(Input::all());
        extjson($return);
    }
}

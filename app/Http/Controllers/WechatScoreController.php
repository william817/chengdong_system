<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Model\WechatScore;

class WechatScoreController extends Controller
{
    /**
     * author  song
     */
    public function scoreTotal()
    {
        $return = WechatScore::scoreTotal();
        extjson($return);
    }

    public function scoreRank()
    {
        $return = WechatScore::scoreRank();
        extjson($return);
    }

}

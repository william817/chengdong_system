<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Model\AdminPartyAct;
use App\Http\Controllers\CommonController;
use App\Http\Model\AdminFeedback;
use App\Http\Model\AdminSignIn;
use App\Http\Model\AdminSignUp;
class AdminPartyActController extends CommonController
{
    //党建必修增加
    public function partyReqSave()
    {
        $return = AdminPartyAct::partyReqSave(Input::all());
        extjson($return);
    }

    //党建必修列表
    public function partyReqList()
    {
        $return = AdminPartyAct::partyReqList(Input::all());
        extjson($return);
    }

    //党建必修编辑
    public function partyReqEdit()
    {
        if (Input::except('id')){
            $return = AdminPartyAct::partyReqEdit(Input::all());
        } else {
            $return = AdminPartyAct::partyReqInfo(Input::all());
        }
        extjson($return);
    }

    //党建必修列表
    public function partyReqInfo()
    {
        $return = AdminPartyAct::partyReqInfo(Input::all());
        extjson($return);
    }

    //党建必修列表
    public function partyReqDelete()
    {
        $return = AdminPartyAct::partyReqDelete(Input::all());
        extjson($return);
    }



    //必修课反馈通过列表
    public function partyReqFeedList()
    {
        $return = AdminFeedback::partyReqFeedList(Input::all());
        extjson($return);
    }

    //必修课反馈不通过列表
    public function partyReqFeedNoList()
    {
        $return = AdminFeedback::partyReqFeedNoList(Input::all());
        extjson($return);
    }

    //必修课反馈通过/不通过接口
    public function partyReqFeed()
    {
        $return = AdminFeedback::partyReqFeed(Input::all());
        return $return;
    }

    //必修课反馈删除接口
    public function partyReqFeedDelete()
    {
        $return = AdminFeedback::partyReqFeedDelete(Input::all());
        return $return;
    }

    //党建选修增加
    public function partyChoSave()
    {
        $return = AdminPartyAct::partyChoSave(Input::all());
        extjson($return);
    }

    //党建选修列表
    public function partyChoList()
    {
        $return = AdminPartyAct::partyChoList(Input::all());
        extjson($return);
    }

    //党建选修编辑
    public function partyChoEdit()
    {
        if (Input::except('id')){
            $return = AdminPartyAct::partyChoEdit(Input::all());
        } else {
            $return = AdminPartyAct::partyChoInfo(Input::all());
        }
        extjson($return);
    }

    //党建选修列表
    public function partyChoInfo()
    {
        $return = AdminPartyAct::partyChoInfo(Input::all());
        extjson($return);
    }

    //党建选修列表
    public function partyChoDelete()
    {
        $return = AdminPartyAct::partyChoDelete(Input::all());
        extjson($return);
    }



    //选修课反馈通过列表
    public function partyChoFeedList()
    {
        $return = AdminFeedback::partyChoFeedList(Input::all());
        extjson($return);
    }

    //选修课反馈不通过列表
    public function partyChoFeedNoList()
    {
        $return = AdminFeedback::partyChoFeedNoList(Input::all());
        extjson($return);
    }

    //选修课反馈通过/不通过接口
    public function partyChoFeed()
    {
        $return = AdminFeedback::partyChoFeed(Input::all());
        return $return;
    }

    //选修课反馈删除接口
    public function partyChoFeedDelete()
    {
        $return = AdminFeedback::partyChoFeedDelete(Input::all());
        return $return;
    }

    //必修课签到详情接口
    public function partyReqSignIn()
    {
        $return = AdminSignIn::partyReqSignIn(Input::all());
        return $return;
    }

    //选修课签到详情接口
    public function partyChoSignIn()
    {
        $return = AdminSignIn::partyChoSignIn(Input::all());
        return $return;
    }

    //选修课报名详情接口
    public function partyChoSignUp()
    {
        $return = AdminSignUp::partyChoSignUp(Input::all());
        return $return;
    }

    //发布/撤销详情接口
    public function partyRelease()
    {
        $return = AdminPartyAct::partyRelease(Input::all());
        return $return;
    }

    public function ChoStart()
    {
        $return = AdminPartyAct::ChoStart(Input::all());
        return $return;
    }

    public function ChoEnd()
    {
        $return = AdminPartyAct::ChoEnd(Input::all());
        return $return;
    }


}

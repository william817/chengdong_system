<?php

namespace App\Http\Model;

use EasyWeChat\Core\Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminSignUp extends Model
{
    public $table = 'sign_up';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    //党建选修课报名详情接口
    public static function partyChoSignUp($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $SignInList = AdminSignUp::where('type',2)->where('act_id',$id)->paginate(10);
        foreach ($SignInList as $k=>$v)
        {
            $v->username = AdminUser::where('id',$v->user_id)->value('username');
            $v->team = AdminUser::where('id',$v->user_id)->value('team');
            $v->title = AdminPartyAct::where('id',$v->act_id)->value('title');
        }
        return $return = array(
            'status' => 200,
            'data' => $SignInList
        );
    }

    //选修课报名详情接口
    public static function multiChoSignUp($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $SignInList = AdminSignUp::where('type',2)->where('act_id',$id)->paginate(10);
        foreach ($SignInList as $k=>$v)
        {
            $v->username = AdminUser::where('id',$v->user_id)->value('username');
            $v->team = AdminUser::where('id',$v->user_id)->value('team');
            $v->title = AdminMultiAcr::where('id',$v->act_id)->value('title');
        }
        return $return = array(
            'status' => 200,
            'data' => $SignInList
        );
    }
}

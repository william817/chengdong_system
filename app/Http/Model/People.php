<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    public $table = 'people';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    public static function addPeople($input) {
        $result = People::insert($input);
        return $result;
    }

    public static function peopleList() {
        $result = People::orderBy('id','desc')->get();;
        return $result;
    }
}

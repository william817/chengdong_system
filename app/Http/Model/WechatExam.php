<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class WechatExam extends Model
{
    public $table = 'exam';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    public static function examInfo($input) {
        $times_id = isset($input['times_id']) && !empty($input['times_id']) ? $input['times_id'] : 0;
        if ($times_id == 0) {
            return $return = array(
                'status' => 400,
                'msg' => '请选择参加得题目列表'
            );
        }
        $examList = WechatExam::where('times_id',$times_id)->get();
        foreach ($examList as $k=>$v) {
            $v->answer = json_decode($v->answer);
        }
        $return['status'] = 200;
        $return['data'] = $examList;
        return $return;
    }
}

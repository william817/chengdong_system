<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class WechatSignIn extends Model
{
    public $table = 'sign_in';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    /**
     * signIn
     * 签到接口
     * @param $input
     * @return array
     */
    public static function signIn($input)
    {
        $user_id = session()->get('userid');
//        dd($user_id);
//        $user_id = 76;
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        $type = isset($input['type']) && !empty($input['type']) ? $input['type'] : 0;
        $List = array();
        if ( $type == 1) {
            $List = WechatPartyAct::find($id);
            $member = WechatUser::where('id',$user_id)->where('status',1)->value('member');
//            // 党建活动判断参与人时候为党员
//            if ($member != 1) {
//                return $return = array(
//                    'status' => 400,
//                    'msg' => '只有党员可以参加'
//                );
//            }
        } elseif ($type == 2) {
            $List = WechatMultiAcr::find($id);
        }
        // 建立查询条件
        $where = array(
            'act_id' => $id,
            'user_id' => $user_id,
            'type' => $type,
            'style' => $List->type
        );
        // 判断该id是否有效
        if (!$List) {
            return $return = array(
                'status' => 400,
                'msg' => '未找到任何有关活动'
            );
        }
        // 判断是否发布
        if ($List->status != 2) {
            return $return = array(
                'status' => 400,
                'msg' => '未开启，不能签到'
            );
        }

        if (($List->start_time - 1800) >time()) {
            return $return = array(
                'status' => 400,
                'msg' => '只能在活动开始前半小时才能签到呦'
            );
        }
        // 判断选修得活动时候报名
        if ($List->type == 2) {
            $signInfo = WechatSignUp::where('act_id' , $id)->where('user_id', $user_id) ->where('type',$type)->first();
            if (!$signInfo) {
                return $return = array(
                    'status' => 400,
                    'msg' => '未检测到您得报名信息'
                );
            }
        }
        // 判断是否已经签到

        if (WechatSignIn::where($where)->first()) {
            return $return = array(
                'status' => 400,
                'msg' => '您已经签过到了'
            );
        }
        DB::beginTransaction();
            $data['user_id'] = $user_id;
            $data['act_id'] = $id;
            $data['type'] = $type;
            $data['style'] = $List->type;
            $data['sign_time'] = time();
            WechatSignIn::create($data);

            DB::commit();
            $return['status'] = 200;
            $return['msg'] = '签到成功';
            extjson($return);
            DB::rollback();
            $return['status'] = 400;
            $return['msg'] = '签到失败';
            extjson($return);
    }
}

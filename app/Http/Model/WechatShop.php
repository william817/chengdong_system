<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class WechatShop extends Model
{
    public $table = 'shop';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    public static function shopList()
    {
        $shopList = WechatShop::get();
        return $return = array(
            'status' => 200,
            'data' => $shopList,
        );
    }

    public static function shopExchange($input)
    {
        $id = $input['id'];
        $user_id = session()->get('userid');
//        $user_id = 76;
        $shopInfo = WechatShop::find($id);
        if ($shopInfo->num == 0) {
            return $return = array(
                'status'=>400,
                'msg' => '商品数量不足！',
            );
        }

        $score = WechatScore::where('user_id',$user_id)->value('score');

        if ($shopInfo->score > $score) {
            return $return = array(
                'status'=>400,
                'msg' => '您得学分不足！',
            );
        }
        DB::beginTransaction();
        WechatShop::where('id',$id)->decrement('num',1);
        WechatScore::where('user_id',$user_id)->decrement('score',$shopInfo->score);
        WechatScore::where('user_id',$user_id)->increment('shop',$shopInfo->score);
        $data['user_id'] = $user_id;
        $data['act_id'] = $id;
        $data['type'] = 6;
        $data['score'] = $shopInfo->score;
        $data['create_time'] = time();
        WechatScoreRecord::create($data);
        $shop_data['user_id'] = $user_id;
        $shop_data['shop_id'] = $id;
        $shop_data['score'] = $shopInfo->score;
        $shop_data['create_time'] = time();
        WechatShopRecord::create($shop_data);
        $push_data['type'] = 6;
        $push_data['user_id'] = $user_id;
        $push_data['title'] = $shopInfo->name;
        $push_data['score'] = $shopInfo->score;
        WechatFeedback::sendScoreRecord($push_data);
        DB::commit();
        return $return = array(
            'status'=>200,
            'msg' => '兑换成功！',
        );
        DB::rollback();
        return $return = array(
            'status'=>400,
            'msg' => '兑换失败！',
        );

    }
}

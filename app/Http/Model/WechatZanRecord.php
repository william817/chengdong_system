<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class WechatZanRecord extends Model
{
    public $table = 'zan_record';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    /**
     * zanAdd
     * 点赞
     * @return array
     */
    public static function zanAdd($input)
    {
//        $user_id = USREID;
        $user_id = session()->get('userid');
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        $res = WechatZanRecord::where('user_id',$user_id)->where('feed_id',$id)->first();
        if (isset($res)){
            extjson($return = array(
                'status' => 400,
                'msg' => '您已经点过赞了'
            ));
        }
        WechatFeedback::where('id',$id)->increment('num',1);
        $data['user_id'] = $user_id;
        $data['feed_id'] = $id;
        $data['zan_time'] = time();
        $re = WechatZanRecord::create($data);
        $return['status'] = $re->id ? 200 : 400;
        $return['msg'] = $re->id ? '点赞成功' : '点赞失败';
        return $return;
    }
}

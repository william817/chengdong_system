<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class WechatOption extends Model
{
    public $table = 'option';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    public static function optionList()
    {
        $user_id  = session()->get('userid');
//        $user_id  = 76;
        $opinionList = WechatOption::where('user_id',$user_id)->orderBy('create_time','desc')->get();
        foreach ($opinionList as $k=>$v){
            $v->create_time = date('Y-m-d H:i:s',$v->create_time);
        }
        $return['status'] = 200;
        $return['data'] = $opinionList;
        return $return;
    }


    public static function optionInfo($input)
    {
        $optionInfo = WechatOption::find($input['id']);
        $return['status'] = 200;
        $return['data'] = $optionInfo;
        return $return;
    }

    public static function optionSave($input)
    {
        $user_id  = session()->get('userid');
        $data['user_id'] = $user_id;
        $data['content'] = $input['content'];
        $data['create_time'] = time();
        $optionInfo = WechatOption::create($data);
        $return['status'] = $optionInfo->id ? 200 :400;
        $return['msg'] = $optionInfo->id ? '提交成功' :'提交失败';
        return $return;
    }
}

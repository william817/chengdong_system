<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WechatSignUp extends Model
{
    public $table = 'sign_up';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    //报名记录
    public static function signUp($input)
    {
        $info = array();
        $user_id = session()->get('userid');
//        $user_id = 76;
        if ($input['type'] == 1) {
            $info = WechatPartyAct::where('id', $input['id'])->first();
        } elseif ($input['type'] == 2) {
            $info = WechatMultiAcr::where('id', $input['id'])->first();
            if ($info->apply_time < time()) {
                $return['status'] = 400;
                $return['msg'] = '报名时间已截止！';
                return $return;
            }
        }
        if ($info->status != 2) {
            $return['status'] = 400;
            $return['msg'] = '活动未发布或已结束，不能报名！';
            return $return;
        }
        $SignInfo = WechatSignUp::where('user_id',$user_id)->where('act_id',$input['id'])->where('type',$input['type'])->first();
        if ($SignInfo) {
            $return['status'] = 400;
            $return['msg'] = '您已经报过名了！';
            return $return;
        }
//        $data['user_id'] = USERID;
        $data['user_id'] = $user_id;
        $data['act_id'] = $input['id'];
        $data['type'] = $input['type'];
        $data['sign_time'] = time();
        $re = WechatSignUp::create($data);
        $return['status'] = $re? 200 :400;
        $return['msg'] = $re? "报名成功" : "报名失败";
        return $return;

    }

    public static function signUpList($input)
    {
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        $type = isset($input['type']) && !empty($input['type']) ? $input['type'] : 0;
        $user_id = session()->get('userid');
        $count = WechatSignUp::where('act_id',$id)->where('type',$type)->count();
        $setting_num = 0;
        if ($type == 1){
            $setting_num = WechatPartyAct::where('id',$id)->value('setting_num');
        } elseif ($type == 2){
            $setting_num = WechatMultiAcr::where('id',$id)->value('setting_num');
        }
        $return['status'] =200;
        $return['count'] = $count;
        $return['setting_num'] = $setting_num;
        return $return;
    }

}

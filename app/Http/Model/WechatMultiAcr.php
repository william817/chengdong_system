<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class WechatMultiAcr extends Model
{
    public $table = 'multi_act';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    //综合必修列表
    public static function multiChoList($input)
    {
        $user_id = session()->get('userid');
        $MultiModel = WechatMultiAcr::where('type',2)->where('status','!=',1);
        $multiList = $MultiModel->orderBy('create_time','desc')->select('id','title','start_time')->get();
        if ($multiList) {
            $multiList = $multiList->toArray();
        }
        foreach ($multiList as $k => &$v) {
            $sign = WechatFeedback::where('user_id',$user_id)->where('act_id',$v['id'])->where('type',2)->first();
            $v['is_join'] = $sign ? 1 : 0;
        }
        $multiList = array_values($multiList);
        $return['status'] = 200;
        $return['data'] = $multiList;
        return $return;
    }

    //综合选修列表
    public static function multiReqList($input)
    {
        $user_id = session()->get('userid');
        $MultiModel = WechatMultiAcr::where('type',1)->where('status','!=',1);
        $multiList = $MultiModel->orderBy('create_time','desc')->select('id','title','start_time')->get();
        if ($multiList) {
            $multiList = $multiList->toArray();
        }
        foreach ($multiList as $k => &$v) {
            $sign = WechatFeedback::where('user_id',$user_id)->where('act_id',$v['id'])->where('type',2)->first();
            $v['is_join'] = $sign ? 1 : 0;
        }
        $multiList = array_values($multiList);
        $return['status'] = 200;
        $return['data'] = $multiList;
        return $return;
    }

    /**
     * multiInfo
     * 综合培训详情
     * @param $input
     * @return array
     */
    public static function multiInfo($input)
    {
        $user_id = session()->get('userid');
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        $info = WechatMultiAcr::find($id);
        $info->material = empty($info->material) ? '' : array_values(json_decode($info->material));
        $info->start_time = date('Y-m-d H:i',$info->start_time);
        $info->current_time = date('Y-m-d H:i:s',time());
        $info->over_time = date('Y-m-d H:i:s',$info->over_time);
        $info->apply_time = date('Y-m-d H:i',$info->apply_time);
        $info->count = WechatSignUp::where('act_id',$id)->where('type',2)->count();
        $sign_info = WechatSignIn::where('act_id',$info->id)->where('user_id',$user_id)->where('type',2)->where('style',$info->type)->first();
        if ($info->type == 2) {
            $signUp_info = WechatSignUp::where('act_id',$info->id)->where('user_id',$user_id)->where('type',2)->first();
            $info->is_signUp = $signUp_info ? 1 : 0;
        }
        $info->is_signIn = $sign_info ? 1 : 0;
        return $return = array(
            'status' => 200,
            'data' => $info
         );
    }
}

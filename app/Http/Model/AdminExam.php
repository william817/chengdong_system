<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminExam extends Model
{
    public $table = 'exam';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];


    public static function examAdd($input)
    {

        $exam = json_decode(($input['exam']), true);

        foreach ($exam as $key => $val) {
            $info['question'] = $val['question'];
            $info['answer'] = json_encode($val['answer'], JSON_UNESCAPED_UNICODE);
            $info['result'] = $val['result'];
            $info['grade'] = $val['grade'];
            $info['times_id'] = $val['id'];
            AdminExam::create($info);

        }

    }

    public static function examList($input)
    {

        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        $data = AdminExam::where('times_id', $id)->get()->toArray();
        foreach ($data['data'] as $k => &$v) {
            $v['answer'] = explode('###', $v['answer']);
//            $v['answer'] = json_encode($v['answer'],JSON_UNESCAPED_UNICODE);
        }
        $return['status'] = 200;
        $return['msg'] = 'success';
        $return['data'] = $data;
        extjson($return);
    }

    public static function examUpdate($input)
    {
        DB::beginTransaction();
        $exam = json_decode(($input['exam']),true);
        foreach($exam as $key=>$val) {
            $status = AdminExamList::where('id',$val['times_id'])->value('status');
            if ($status == 2 || $status == 3) {
                $return['status'] = 400;
                $return['msg'] = '已发布或已经结束，不可修改';
                $return['data'] = array();
                extjson($return);
            }
            $info['question'] = $val['question'];
            $info['answer'] = json_encode($val['answer'], JSON_UNESCAPED_UNICODE);
            $info['result'] = $val['result'];
            $info['grade'] = $val['grade'];
            $info['times_id'] = $val['times_id'];
            $info['update_time'] = time();
            if (isset($val['id'])) {
                AdminExam::where('id', $val['id'])->update($info);
            } else {
                AdminExam::create($info);
            }
        }
        DB::commit();
        $return['status'] = 200;
        $return['msg'] = '编辑成功';
        $return['data'] = array();
        extjson($return);
        DB::rollback();
        $return['status'] = 400;
        $return['msg'] = '编辑失败';
        $return['data'] = array();
        extjson($return);
    }

    public static function examInfo($input)
    {
        $id = isset($input['times_id']) && !empty($input['times_id']) ? $input['times_id'] : 0;
        if ($id == 0) {
            $return['status'] = 600;
            $return['msg'] = 'id无效';
            $return['data'] = array();
            extjson($return);
        }
        $info = AdminExam::where('times_id', $id)->get();
        foreach ($info as $k => $v) {
            $v->answer = json_decode($v->answer);
        }
        $return['status'] = 200;
        $return['msg'] = 'success';
        $return['data'] = $info;
        extjson($return);
    }

    public static function examDelete($input)
    {
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return['status'] = 600;
            $return['msg'] = 'id无效';
            $return['data'] = array();
            extjson($return);
        }
        $times_id = AdminExam::where('id', $id)->value('times_id');
        $status = AdminExamList::where('id', $times_id)->value('status');
      //  if ($status == 2) {
			//$return['status'] = 400;
            //$return['msg'] = '已发布不可删除';
            //$return['data'] = array();
            //extjson($return);
       // }
        $res = AdminExam::where('id', $id)->delete();
        if (!$res) {
            $return['status'] = 400;
            $return['msg'] = '删除失败';
            $return['data'] = array();
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = '删除成功';
        $return['data'] = array();
        extjson($return);
    }

    public static function examScoreAdd($input)
    {
        DB::beginTransaction();
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        $info = AdminExamList::find($id);
        $score = $info->score;
        AdminRecord::recordAdd($score, $id, 5);
        AdminScore::where('user_id', USERID)
            ->update([
                'score' => DB::raw('score +' . $score),
                'study' => DB::raw('study +' . $score),
            ]);
        AdminUser::where('id', USERID)->increment('score', $score);
        DB::commit();
        $return['status'] = 200;
        $return['msg'] = '增加积分!' . $score;
        $return['data'] = array();
        extjson($return);
        DB::rollback();
        $return['status'] = 400;
        $return['msg'] = '出错了';
        $return['data'] = array();
        extjson($return);
    }
}

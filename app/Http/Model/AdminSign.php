<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class AdminSign extends Model
{
    public $table = 'sign';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];
}

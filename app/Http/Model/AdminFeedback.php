<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class AdminFeedback extends Model
{
    public $table = 'feedback';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    //党建必修课反馈通过列表
    public static function partyReqFeedList($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $FeedbackList = DB::table('feedback')->join('party_act','party_act.id','=','feedback.act_id')->
            where('feedback.type',1)->where('feedback.status',2)->where('party_act.type',1)->where('feedback.act_id',$id)
            ->select('party_act.id','party_act.title','feedback.id','feedback.act_id','feedback.username','feedback.content','feedback.feed_time')
            ->orderBy('feed_time','desc')
            ->get();
        $return['status'] = 200;
        $return['data'] = $FeedbackList;
        return $return;
    }

    //党建必修课反馈不通过列表
    public static function partyReqFeedNoList($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $FeedbackList = DB::table('feedback')->join('party_act','party_act.id','=','feedback.act_id')->
        where('feedback.type',1)->where('feedback.status',1)->where('party_act.type',1)->where('feedback.act_id',$id)
            ->select('party_act.id','party_act.title','feedback.id','feedback.act_id','feedback.username','feedback.content','feedback.feed_time')
            ->orderBy('feed_time','desc')
            ->get();
        $return['status'] = 200;
        $return['data'] = $FeedbackList;
        return $return;
    }

    //党建必修课反馈通过/不通过接口
    public static function partyReqFeed($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $status = isset($input['status']) ? $input['status'] : 0;
        if ($status == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }

        $list = AdminFeedback::find($id);
        $party_list = AdminPartyAct::find($list->act_id);
        $re = AdminFeedback::where('id',$id)->update(['status'=>$status]);
        if ($status == 2){

            $score_data['user_id'] = $list->user_id;
            $score_data['act_id'] = $list->act_id;
            $score_data['score'] = $party_list->score;
            $score_data['create_time'] = time();
            $score_data['type'] = 1;
            WechatScore::where('user_id',$list->user_id)->increment('party_must',$party_list->score);
            $push_data['type'] = 1;

            WechatScore::where('user_id',$list->user_id)->increment('score',$party_list->score);
            WechatScoreRecord::create($score_data);
            $push_data['user_id'] = $list->user_id;
            $push_data['title'] = $party_list->title;
            $push_data['score'] = $party_list->score;
            WechatFeedback::sendScoreRecord($push_data);
        }

        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '操作成功' : '操作失败';
        return $return;
    }

    //党建必修课反馈删除接口
    public static function partyReqFeedDelete($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $re = AdminFeedback::where('id',$id)->delete();
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '删除成功' : '删除失败';
        return $return;
    }

    //党建选修课反馈通过列表
    public static function partyChoFeedList($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $FeedbackList = DB::table('feedback')->join('party_act','party_act.id','=','feedback.act_id')->
        where('feedback.type',1)->where('feedback.status',2)->where('party_act.type',2)->where('feedback.act_id',$id)
            ->select('party_act.title','feedback.id','feedback.act_id','feedback.username','feedback.content','feedback.feed_time')
            ->orderBy('feed_time','desc')
            ->get();
        $return['status'] = 200;
        $return['data'] = $FeedbackList;
        return $return;
    }

    //党建选修课反馈不通过列表
    public static function partyChoFeedNoList($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $FeedbackList = DB::table('feedback')->join('party_act','party_act.id','=','feedback.act_id')->
        where('feedback.type',1)->where('feedback.status',1)->where('party_act.type',2)->where('feedback.act_id',$id)
            ->select('party_act.title','feedback.id','feedback.act_id','feedback.username','feedback.content','feedback.feed_time')
            ->orderBy('feed_time','desc')
            ->get();
        $return['status'] = 200;
        $return['data'] = $FeedbackList;
        return $return;
    }

    //党建选修课反馈通过/不通过接口
    public static function partyChoFeed($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $status = isset($input['status']) ? $input['status'] : 0;
        if ($status == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $list = AdminFeedback::find($id);
        $party_list = AdminPartyAct::find($list->act_id);
        $re = AdminFeedback::where('id',$id)->update(['status'=>$status]);
        if ($status == 2){
            $score_data['user_id'] = $list->user_id;
            $score_data['act_id'] = $list->act_id;
            $score_data['score'] = $party_list->score;
            $score_data['create_time'] = time();
            $score_data['type'] = 2;
            WechatScore::where('user_id',$list->user_id)->increment('party_choice',$party_list->score);
            $push_data['type'] = 2;
            WechatScore::where('user_id',$list->user_id)->increment('score',$party_list->score);
            WechatScoreRecord::create($score_data);
            $push_data['user_id'] = $list->user_id;
            $push_data['title'] = $party_list->title;
            $push_data['score'] = $party_list->score;
            WechatFeedback::sendScoreRecord($push_data);
        }
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '操作成功' : '操作失败';
        return $return;
    }

    //党建选修课反馈删除接口
    public static function partyChoFeedDelete($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $re = AdminFeedback::where('id',$id)->delete();
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '删除成功' : '删除失败';
        return $return;
    }

    //综合必修课反馈通过列表
    public static function multiReqFeedList($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $FeedbackList = DB::table('feedback')->join('multi_act','multi_act.id','=','feedback.act_id')->where('feedback.act_id',$id)
            ->where('feedback.type',2)->where('feedback.status',2)->where('multi_act.type',1)->where('feedback.act_id',$id)
            ->select('multi_act.title','feedback.id','feedback.act_id','feedback.username','feedback.content','feedback.feed_time')
            ->orderBy('feed_time','desc')
            ->get();
        $return['status'] = 200;
        $return['data'] = $FeedbackList;
        return $return;
    }

    //综合必修课反馈不通过列表
    public static function multiReqFeedNoList($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $FeedbackList = DB::table('feedback')->join('multi_act','multi_act.id','=','feedback.act_id')->where('feedback.act_id',$id)
            ->where('feedback.type',2)->where('feedback.status',1)->where('multi_act.type',1)->where('feedback.act_id',$id)
            ->select('multi_act.title','feedback.id','feedback.act_id','feedback.username','feedback.content','feedback.feed_time')
            ->orderBy('feed_time','desc')
            ->get();
        $return['status'] = 200;
        $return['data'] = $FeedbackList;
        return $return;
    }

    //综合必修课反馈通过/不通过接口
    public static function multiReqFeed($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $status = isset($input['status']) ? $input['status'] : 0;
        if ($status == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $list = AdminFeedback::find($id);
        $party_list = AdminMultiAcr::find($list->act_id);
        $re = AdminFeedback::where('id',$id)->update(['status'=>$status]);

        if ($status == 2){

            $score_data['user_id'] = $list->user_id;
            $score_data['act_id'] = $list->act_id;
            $score_data['score'] = $party_list->score;
            $score_data['create_time'] = time();
            $score_data['type'] = 3;
            WechatScore::where('user_id',$list->user_id)->increment('multi_party',$party_list->score);
            $push_data['type'] = 3;

            WechatScore::where('user_id',$list->user_id)->increment('score',$party_list->score);
            WechatScoreRecord::create($score_data);
            $push_data['user_id'] = $list->user_id;
            $push_data['title'] = $party_list->title;
            $push_data['score'] = $party_list->score;
            WechatFeedback::sendScoreRecord($push_data);
        }

        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '操作成功' : '操作失败';
        return $return;
    }

    //综合必修课反馈删除接口
    public static function multiReqFeedDelete($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $re = AdminFeedback::where('id',$id)->delete();
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '删除成功' : '删除失败';
        return $return;
    }

    //综合选修课反馈通过列表
    public static function multiChoFeedList($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $FeedbackList = DB::table('feedback')->join('multi_act','multi_act.id','=','feedback.act_id')->where('feedback.act_id',$id)
            ->where('feedback.type',2)->where('feedback.status',2)->where('multi_act.type',2)->where('feedback.act_id',$id)
            ->select('multi_act.title','feedback.id','feedback.act_id','feedback.username','feedback.content','feedback.feed_time')
            ->orderBy('feed_time','desc')
            ->get();
        $return['status'] = 200;
        $return['data'] = $FeedbackList;
        return $return;
    }

    //综合选修课反馈不通过列表
    public static function multiChoFeedNoList($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $FeedbackList = DB::table('feedback')->join('multi_act','multi_act.id','=','feedback.act_id')->where('feedback.act_id',$id)
            ->where('feedback.type',2)->where('feedback.status',1)->where('multi_act.type',2)->where('feedback.act_id',$id)
            ->select('multi_act.title','feedback.id','feedback.act_id','feedback.username','feedback.content','feedback.feed_time')
            ->orderBy('feed_time','desc')
            ->get();
        $return['status'] = 200;
        $return['data'] = $FeedbackList;
        return $return;
    }

    //综合选修课反馈通过/不通过接口
    public static function multiChoFeed($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $status = isset($input['status']) ? $input['status'] : 0;
        if ($status == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $list = AdminFeedback::find($id);
        $party_list = AdminMultiAcr::find($list->act_id);
        $re = AdminFeedback::where('id',$id)->update(['status'=>$status]);

        if ($status == 2){

            $score_data['user_id'] = $list->user_id;
            $score_data['act_id'] = $list->act_id;
            $score_data['score'] = $party_list->score;
            $score_data['create_time'] = time();
            $score_data['type'] = 4;
            WechatScore::where('user_id',$list->user_id)->increment('multi_choice',$party_list->score);
            $push_data['type'] = 4;

            WechatScore::where('user_id',$list->user_id)->increment('score',$party_list->score);
            WechatScoreRecord::create($score_data);
            $push_data['user_id'] = $list->user_id;
            $push_data['title'] = $party_list->title;
            $push_data['score'] = $party_list->score;
            WechatFeedback::sendScoreRecord($push_data);
        }

        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '操作成功' : '操作失败';
        return $return;
    }

    //综合选修课反馈删除接口
    public static function multiChoFeedDelete($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $re = AdminFeedback::where('id',$id)->delete();
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '删除成功' : '删除失败';
        return $return;
    }
}

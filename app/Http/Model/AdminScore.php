<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class AdminScore extends Model
{
    public $table = 'score';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];


    public static function scoreList()
    {
//        $scoreList = AdminScore::get();
//        foreach ($scoreList as $k=>$v){
//            $v->username = AdminUser::where('id',$v->user_id)->value('username');
//        }
        $scoreList = AdminScore::join('user','user.id','=',
            'score.user_id')->select('score.*','user.username')->orderBy('user.create_time','desc')->get();
        return $return = array(
            'status' => 200,
            'data' => $scoreList
        );
    }

    public static function scoreInfo($input){
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return['status'] = 400;
            $return['msg'] = '请选择要操作的数据';
            $return['data'] = array();
            extjson($return);
        }
        $score_info = AdminScore::find($id);
        $score_info->username = AdminUser::where('id',$score_info->user_id)->value('username');
        $return['status'] = $score_info ? 200 : 400;
        $return['msg'] = $score_info ? '' : '该用户积分详情不存在';
        $return['data'] = $score_info ? $score_info : array();
        return $return;
    }

    public static function scoreEdit($input){
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return['status'] = 400;
            $return['msg'] = '请选择要操作的数据';
            $return['data'] = array();
            extjson($return);
        }
        $data['multi_choice'] = $input['multi_choice'];
        $data['multi_party'] = $input['multi_party'];
        $data['party_choice'] = $input['party_choice'];
        $data['party_must'] = $input['party_must'];
        $data['exam'] = $input['exam'];
        $data['shop'] = $input['shop'];
        $data['score'] = $input['score'];
        $re = AdminScore::where('id',$input['id'])->update($data);
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '修改成功' : '修改失败';
        return $return;
    }
}

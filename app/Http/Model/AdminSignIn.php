<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class AdminSignIn extends Model
{
    public $table = 'sign_in';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    //党建必修课签到详情接口
    public static function partyReqSignIn($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $SignInList = AdminSignIn::where('style',1)->where('act_id',$id)->where('type',1)->get();
        foreach ($SignInList as $k=>$v)
        {
            $v->username = AdminUser::where('id',$v->user_id)->value('username');
            $v->team = AdminUser::where('id',$v->user_id)->value('team');
            $v->title = AdminPartyAct::where('id',$v->act_id)->value('title');
        }

        $users = AdminUser::where('status',1)->where('member',1)->get();
		
        foreach ($users as $key => $val) {
            $sign_info = AdminSignIn::where('user_id',$val->id)->where('style',1)->where('act_id',$id)->where('type',1)->first();
			$val ->title = AdminPartyAct::where('id',$id)->value('title');
            if (!empty($sign_info)) {
                unset($users[$key]);
            } else {
                continue;
            }
        }
		foreach($users as $object){
			$NoUsers[] = $object->toArray();
		}
        return $return = array(
            'status' => 200,
            'data' => $SignInList,
            'no_sign' => $NoUsers
        );
    }

    //党建选修课签到详情接口
    public static function partyChoSignIn($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $SignInList = AdminSignIn::where('style',2)->where('act_id',$id)->where('type',1)->get();
        foreach ($SignInList as $k=>$v)
        {
            $v->username = AdminUser::where('id',$v->user_id)->value('username');
            $v->team = AdminUser::where('id',$v->user_id)->value('team');
            $v->title = AdminPartyAct::where('id',$v->act_id)->value('title');
        }
        return $return = array(
            'status' => 200,
            'data' => $SignInList
        );
    }

    //综合必修课签到详情接口
    public static function multiReqSignIn($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $SignInList = AdminSignIn::where('style',1)->where('act_id',$id)->where('type',2)->get();
        foreach ($SignInList as $k=>$v)
        {
            $v->username = AdminUser::where('id',$v->user_id)->value('username');
            $v->team = AdminUser::where('id',$v->user_id)->value('team');
            $v->title = AdminMultiAcr::where('id',$v->act_id)->value('title');
        }

        $users = AdminUser::where('status',1)->get();
        foreach ($users as $key => $val) {
            $sign_info = AdminSignIn::where('user_id',$val->id)->where('style',1)->where('act_id',$id)->where('type',2)->first();
            if (!empty($sign_info)) {
                unset($users[$key]);
            } else {
                continue;
            }
        }
        return $return = array(
            'status' => 200,
            'data' => $SignInList,
            'no_sign' => $users
        );
    }

    //综合选修课签到详情接口
    public static function multiChoSignIn($input)
    {
        $id = isset($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' => 400 ,
                'msg' => '数据无效'
            );
        }
        $SignInList = AdminSignIn::where('style',2)->where('act_id',$id)->where('type',2)->get();
        foreach ($SignInList as $k=>$v)
        {
            $v->username = AdminUser::where('id',$v->user_id)->value('username');
            $v->team = AdminUser::where('id',$v->user_id)->value('team');
            $v->title = AdminMultiAcr::where('id',$v->act_id)->value('title');
        }
        return $return = array(
            'status' => 200,
            'data' => $SignInList
        );
    }
}

<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class WechatExamRecord extends Model
{
    public $table = 'exam_record';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];
}

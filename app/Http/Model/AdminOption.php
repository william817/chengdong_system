<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class AdminOption extends Model
{
    public $table = 'option';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    public static function optionList()
    {
        $opinionList = AdminOption::orderBy('create_time','desc')->get();
        foreach ($opinionList as $k=>$v){
            $v->create_time = date('Y-m-d H:i:s',$v->create_time);
            $v->username = AdminUser::where('id',$v->user_id)->value('username');
        }
        $return['status'] = 200;
        $return['data'] = $opinionList;
        return $return;
    }

    public static function optionInfo($input)
    {
        $optionInfo = AdminOption::find($input['id']);
        $optionInfo->create_time = date('Y-m-d H:i:s',$optionInfo->create_time);
        $optionInfo->username = AdminUser::where('id',$optionInfo->user_id)->value('username');
        $return['status'] = 200;
        $return['data'] = $optionInfo;
        return $return;
    }

    public static function optionDelete($input)
    {
        $id = $input['id'];
        $optionInfo = AdminOption::find($id);
        if (!$optionInfo) {
            return $return = array(
                'status' => 400,
                'msg' => '此数据不存在或已被删除'
            );
        }
        $re = AdminOption::where('id',$id)->delete();
        $return['status'] = $re ? 200 :400;
        $return['data'] = $re ? '删除成功' :'删除失败';
        return $return;

    }
}

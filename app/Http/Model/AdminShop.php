<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class AdminShop extends Model
{
    public $table = 'shop';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    public static function shopAdd($input)
    {
        $data['name'] = $input['name'];
        $data['score'] = $input['score'];
        $data['num'] = $input['num'];
        $res = AdminShop::create($data);
        if (!$res) {
            $return['status'] = 400;
            $return['msg'] = '添加失败';
            $return['data'] = array();
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = '添加成功';
        $return['data'] = array();
        extjson($return);
    }

    public static function shopList($input)
    {
        $data = AdminShop::get();
        if (!$data) {
            $return['status'] = 400;
            $return['msg'] = '暂无数据';
            $return['data'] = array();
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = 'success';
        $return['data'] = $data;
        extjson($return);
    }

    public static function shopUpdate($input)
    {
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return['status'] = 400;
            $return['msg'] = '请选择要操作的数据';
            $return['data'] = array();
            extjson($return);
        }
        $data['name'] = $input['name'];
        $data['score'] = $input['score'];
        $data['num'] = $input['num'];
        $res = AdminShop::where('id',$id)->update($data);
        if (!$res) {
            $return['status'] = 400;
            $return['msg'] = '修改失败';
            $return['data'] = array();
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = '修改成功';
        $return['data'] = array();
        extjson($return);
    }

    public static function shopDelete($input)
    {
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return['status'] = 400;
            $return['msg'] = '请选择要操作的数据';
            $return['data'] = array();
            extjson($return);
        }
        $res = AdminShop::where('id',$id)->delete();
        if (!$res) {
            $return['status'] = 400;
            $return['msg'] = '删除失败';
            $return['data'] = array();
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = '删除成功';
        $return['data'] = array();
        extjson($return);
    }

    public static function shopInfo($input)
    {
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        $data = AdminShop::find($id);
        if (!$data) {
            $return['status'] = 400;
            $return['msg'] = '无此数据';
            $return['data'] = array();
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = 'success';
        $return['data'] = $data;
        extjson($return);
    }

    public static function shopGrounding($input)
    {
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return['status'] = 400;
            $return['msg'] = '请选择要操作的数据';
            $return['data'] = array();
            extjson($return);
        }
        $data['state'] = 2;
        $res = AdminShop::where('id',$id)->update($data);
        if (!$res) {
            $return['status'] = 400;
            $return['msg'] = '网络请求失败或该商品已下架';
            $return['data'] = array();
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = '下架成功';
        $return['data'] = array();
        extjson($return);
    }

    public static function shopExchange($input)
    {
        DB::beginTransaction();
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0 ;
        $info = AdminShop::find($id);
        if ($info->num <= 0) {
            $return['status'] = 400;
            $return['msg'] = '此商品已经没有了，下次快一些哟!';
            $return['data'] = array();
            extjson($return);
        }
        AdminRecord::recordAdd($info->score,6);
        AdminScore::where('user_id',USERID)
            ->update([
                'score'=>DB::raw('score -'.$info->score),
                'shop'=>DB::raw('shop +'.$info->score),
            ]);
        AdminShop::where('id',$id)->decrement('num',1);
        AdminUser::where('id',USERID)->decrement('score',$info->score);
        DB::commit();
        $return['status'] = 200;
        $return['msg'] = '商品兑换成功!';
        $return['data'] = array();
        extjson($return);
        DB::rollback();
        $return['status'] = 400;
        $return['msg'] = '商品兑换失败!';
        $return['data'] = array();
        extjson($return);
    }
}

<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class AdminLogin extends Model
{
    public $table = 'admin';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $guarded = [];

    //登陆
    public static function login($input)
    {
        isset($input['username']) && !empty($input['username']) ? $username = $input['username'] : extjson($return= array('status'=>400,'msg'=>'请输出用户名'));
        isset($input['password']) && !empty($input['password']) ? $password = $input['password'] : extjson($return = array('status'=>400,'msg'=>'请输出密码'));
        $password = md5($password);
        $UserInfo = AdminLogin::where('username',$username)->where('password',$password)->first();
        if (!$UserInfo) {
            return $return = array(
                'status' => 400,
                'msg' => '用户名或密码错误！'
            );
        }
        if (!session_id()) session_start();
        $_SESSION["user_id"]=$UserInfo->id;
        return $return = array(
            'status' => 200,
            'msg' => '登陆成功！'
        );
    }
}

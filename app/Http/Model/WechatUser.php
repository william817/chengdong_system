<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class WechatUser extends Model
{
    public $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    //首次提交个人信息保存
    public static function submitUserInfo($input)
    {
        $user_id = session()->get('userid');
        $data['username'] = $input['username'];
        $data['member'] = $input['member'];
        $data['status'] = 2;
        $re = WechatUser::where('id',$user_id)->update($data);
        $return['status'] = $re ? 200 : 100;
        $return['msg'] = $re ? '信息提交成功，待审核。' : '信息提交失败，请稍后重试！';
        return $return;
    }

    public static function UserEdit($input)
    {
        $user_id = session()->get('userid');
        $data['username'] = $input['username'];
        $data['member'] = $input['member'];
        $data['create_time'] = time();
        $re = WechatUser::where('id',$user_id)->update($data);
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '修改成功。' : '修改失败！';
        return $return;
    }

}

<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use App\Http\Model\AdminScore;
class AdminUser extends Model
{
    public $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];


    //后台审核通过
    public static function examineAndVerify($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择审核得id',
            );
            return $return;
        }
        $UserInfo = AdminUser::find($id);
        if (!$UserInfo){
            $return = array(
                'status' => 400,
                'msg' => '此用户不存在',
            );
            return $return;
        }

        $re = AdminUser::where('id',$id)->update(['status'=>1]);
        if ($re) {
            $result = AdminScore::where('user_id',$id)->first();
            if (!$result) {
                AdminScore::create(['user_id'=>$id]);
            }
        }
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '操作成功' : '操作失败，请稍后重试';
        return $return;
    }


    //后台用户删除接口
    public static function userDelete($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择删除得用户',
            );
            return $return;
        }
        $UserInfo = AdminUser::find($id);
        if (!$UserInfo){
            $return = array(
                'status' => 400,
                'msg' => '此用户不存在',
            );
            return $return;
        }
        $re = AdminUser::where('id',$id)->update(['status'=>4]);
        if ($re) {
            AdminScore::where('user_id',$id)->delete();
        }
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '删除成功' : '删除失败，请稍后重试';
        return $return;
    }

    //审核通过得员工列表
    public static function userPassList()
    {
//        $UserList = AdminUser::where('status',1)->select('id','username')->orderBy('create_time','desc')->get();
//        foreach ($UserList as $k => $v) {
//            $score = AdminScore::where('user_id',$v->id)->value('score');
//            $v->score = $score ? $score : 0;
//        }

        $UserList = AdminUser::join('score','score.user_id','=',
            'user.id')->where('user.status',1)->select('user.id','user.username','score.score','user.member')->orderBy('user.create_time','desc')->get();
        $return['status'] = 200;
        $return['data'] = $UserList;
        return $return;
    }

    //审核不通过得员工列表
    public static function userNoPassList()
    {
        $UserList = AdminUser::where('status',2)->select('id','username','member','team')->get();
        $return['status'] = 200;
        $return['data'] = $UserList;
        return $return;
    }

    //审核通过得员工编辑
    public static function userEdit($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择员工',
            );
            return $return;
        }
        $data['username'] = $input['username'];
        $data['member'] = $input['member'];
        $re = AdminUser::where('id',$id)->update($data);
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '修改成功' : '修改失败';
        return $return;
    }
    //发送模板消息
    public static function sendNotice($input)
    {
        $input['keyword4'] = $input['submiter'];
        $appid = "wxfeb3236ac589e519";
        $appsecret = "8b857b93de8ab85215266024168d94b0";
        $openids = array();
        switch ($input['member']){
            case 1:
                $openids = AdminUser::where('member',1)->where('status',1)->pluck('openid');//审核通过的党员
                break;
            case 2:
                $openids = AdminUser::where('status',1)->pluck('openid');//审核通过的所有成员
                break;
            default:
//                $openids = User::where('member',2)->where('status',1)->pluck('openid');//审核通过的非党员
        }
        $jump_url = '';
        switch ($input['type']) {
            case 1: //党建必修
                $jump_url = config('services.weixin.redirect').'Learning/activity?id='.$input['id'];
                break;
            case 2: //党建选修
                $jump_url = config('services.weixin.redirect').'Learning/room?id='.$input['id'];
                break;
            case 3:  //综合必修
                $jump_url = config('services.weixin.redirect').'synth/activity?id='.$input['id'];
                break;
            default: //综合选修
                $jump_url = config('services.weixin.redirect').'synth/take_activity?id='.$input['id'];
//                $jump_url = config('services.weixin.redirect').'synth/take_activity?id=1';
        }
        foreach ($openids as $openid) {
            $json_token=AdminUser::http_request("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appid."&secret=".$appsecret);
            $access_token=json_decode($json_token,true);
            $access_token=$access_token['access_token'];
            $template = array(
                'touser' => $openid,
                'template_id' => "CjYbNYy0MmoMrBgJZ5rSlm8vbkz8dCzPzoPIma8SZY0",
                'url' => $jump_url,
                'topcolor' => "#7B68EE",
                'data' => array(
                    'first' => array('value' => urlencode("您好,城东e学堂新的活动信息啦~"), 'color' => "#FF0000"),
                    'keyword1' => array('value' => urlencode($input['title']), 'color' => '#FF0000'),
                    'keyword2' => array('value' => urlencode(date('Y-m-d H:i:s',$input['start_time'])), 'color' => '#FF0000'),
                    'keyword3' => array('value' => urlencode($input['address']), 'color' => '#FF0000'),
                    'keyword4' => array('value' => urlencode($input['keyword4']), 'color' => '#FF0000'),
//                    'keyword5' => array('value' => urlencode($input['setting_num']), 'color' => '#FF0000'),
                    'remark'=>array('value'=>urlencode("请准时参加！"),'color'=>'#FF0000'),
                )
            );
            $json_template = json_encode($template);
            $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $access_token;
            $res = AdminUser::http_request($url, urldecode($json_template));
//            file_put_contents('test.txt',var_export($res,true));

        }
        $return['status'] = 200;
        $return['msg'] = '信息发送成功';
        extjson($return);
    }
    public static function http_request($url,$data=array()){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        // POST数据
        curl_setopt($ch, CURLOPT_POST, 1);
        // 把post的变量加上
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}

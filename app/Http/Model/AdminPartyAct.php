<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class AdminPartyAct extends Model
{
    public $table = 'party_act';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    //党建必修添加
    public static function partyReqSave($input)
    {
        $data['title'] = $input['title'];
        $data['abstract'] = $input['abstract'];
        $data['material'] = json_encode($input['material']);
        $data['address'] = $input['address'];
        $data['submiter'] = $input['submiter'];
        $data['start_time'] = $input['start_time'];
        $data['create_time'] = time();
        $data['over_time'] = $input['over_time'];
        $data['type'] = 1;
        $data['score'] = $input['score'];
        $data['setting_num'] = 0;
        $data['status'] = 1;
        $re = AdminPartyAct::create($data);
        $return['status'] = $re->id ? 200 :400;
        $return['msg'] = $re->id ? '添加成功' : '添加失败，请稍后重试！';
        return $return;
    }

    //党建必修列表
    public static function partyReqList($input)
    {
        $status = isset($input['status']) && !empty($input['status']) ? $input['status'] : 0;
        $PartyModel = AdminPartyAct::where('type',1);
        if ($status != 0){
            $PartyModel = $PartyModel->where('status',$status);
        }
        $partyList = $PartyModel->orderBy('create_time','desc')->select('id','title','start_time','over_time','status')->get();
        foreach ($partyList as $k => $v) {
            $v->really_num = AdminSignIn::where('act_id',$v->id)->where('type',1)->where('style',1)->count();
            $v->pass_num = AdminFeedback::where('act_id',$v->id)->where('type',1)->where('status',2)->count();
            $v->nopass_num = AdminFeedback::where('act_id',$v->id)->where('type',1)->where('status',1)->count();
//            $v->start_time = date('Y-m-d H:i:s',$v->start_time);
//            $v->over_time = date('Y-m-d H:i:s',$v->over_time);
        }
        $return['status'] = 200;
        $return['data'] = $partyList;
        return $return;
    }

    //党建必修修改
    public static function partyReqEdit($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $PartyInfo = AdminPartyAct::find($id);
        if (!$PartyInfo){
            $return = array(
                'status' => 400,
                'msg' => '该数据不存在！',
            );
            return $return;
        }
//        if ($PartyInfo->status != 1){
//            $return = array(
//                'status' => 400,
//                'msg' => '该活动增在开始或者已经结束，不能修改！',
//            );
//            return $return;
//        }
        $data['title'] = $input['title'];
        $data['abstract'] = $input['abstract'];
        $data['material'] = json_encode($input['material']);
        $data['address'] = $input['address'];
        $data['submiter'] = $input['submiter'];
        $data['score'] = $input['score'];
        $data['create_time'] = time();
        $data['start_time'] = $input['start_time'];
        $data['over_time'] = $input['over_time'];
        $re = AdminPartyAct::where('id',$id)->update($data);
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '修改成功' : '修改失败，请稍后重试！';
        return $return;
    }

    //党建必修详情
    public static function partyReqInfo($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $PartyInfo = AdminPartyAct::find($id);
        $PartyInfo->material = json_decode($PartyInfo->material);
//        $PartyInfo->start_time = date('Y-m-d H:i:s',$PartyInfo->start_time);
//        $PartyInfo->create_time = date('Y-m-d H:i:s',$PartyInfo->create_time);
//        $PartyInfo->over_time = date('Y-m-d H:i:s',$PartyInfo->over_time);
        $return['status'] = $PartyInfo->id ? 200 : 400;
        $return['msg'] = $PartyInfo->id ? 'success' : '该数据不存在！';
        $return['data'] = $PartyInfo->id ? $PartyInfo->toArray() : array();
        return $return;
    }

    //党建必修删除
    public static function partyReqDelete($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $PartyInfo = AdminPartyAct::find($id);
        if (!$PartyInfo){
            $return = array(
                'status' => 400,
                'msg' => '该数据不存在！',
            );
            return $return;
        }
        if ($PartyInfo->status == 2){
            $return = array(
                'status' => 400,
                'msg' => '该活动正在开始中，不能删除！',
            );
            return $return;
        }
        $re = AdminPartyAct::where('id',$id)->delete();
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '删除成功' : '删除失败，请刷新重试！';
        return $return;
    }

    //党建选修添加
    public static function partyChoSave($input)
    {
        $data['title'] = $input['title'];
        $data['abstract'] = $input['abstract'];
        $data['material'] = json_encode($input['material']);
        $data['address'] = $input['address'];
        $data['submiter'] = $input['submiter'];
        $data['start_time'] = $input['start_time'];
        $data['apply_time'] = $input['start_time'];
        $data['create_time'] = time();
        $data['over_time'] = $input['over_time'];
        $data['type'] = 2;
        $data['score'] = $input['score'];
        $data['setting_num'] = $input['setting_num'];
        $data['status'] = 1;
        $re = AdminPartyAct::create($data);
        $return['status'] = $re->id ? 200 :400;
        $return['msg'] = $re->id ? '添加成功' : '添加失败，请稍后重试！';
        return $return;
    }

    //党建选修列表
    public static function partyChoList($input)
    {
        $status = isset($input['status']) && !empty($input['status']) ? $input['status'] : 0;
        $PartyModel = AdminPartyAct::where('type',2);
        if ($status != 0){
            $PartyModel = $PartyModel->where('status',$status);
        }
        $partyList = $PartyModel->orderBy('create_time','desc')->select('id','title','setting_num','start_time','over_time','status')->get(); //缺少查询发布时间
        foreach ($partyList as $k => $v) {
            $v->really_num = AdminSignIn::where('act_id',$v->id)->where('type',1)->where('style',2)->count();
            $v->pass_num = AdminFeedback::where('act_id',$v->id)->where('type',1)->where('status',2)->count();
            $v->nopass_num = AdminFeedback::where('act_id',$v->id)->where('type',1)->where('status',1)->count();
//            $v->start_time = date('Y-m-d H:i:s',$v->start_time);
//            $v->over_time = date('Y-m-d H:i:s',$v->over_time);
        }
        $return['status'] = 200;
        $return['data'] = $partyList;
        return $return;
    }

    //党建选修修改
    public static function partyChoEdit($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $PartyInfo = AdminPartyAct::find($id);
        if (!$PartyInfo){
            $return = array(
                'status' => 400,
                'msg' => '该数据不存在！',
            );
            return $return;
        }
//        if ($PartyInfo->status != 1){
//            $return = array(
//                'status' => 400,
//                'msg' => '该活动增在开始或者已经结束，不能修改！',
//            );
//            return $return;
//        }
        $data['title'] = $input['title'];
        $data['abstract'] = $input['abstract'];
        $data['material'] = json_encode($input['material']);
        $data['address'] = $input['address'];
        $data['submiter'] = $input['submiter'];
        $data['score'] = $input['score'];
        $data['setting_num'] = $input['setting_num'];
        $data['start_time'] = $input['start_time'];
        $data['apply_time'] = $input['start_time'];
        $data['create_time'] = time();
        $data['over_time'] = $input['over_time'];
        $re = AdminPartyAct::where('id',$id)->update($data);
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '修改成功' : '修改失败，请稍后重试！';
        return $return;
    }

    //党建选修详情
    public static function partyChoInfo($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $PartyInfo = AdminPartyAct::find($id);
        $PartyInfo->material = json_decode($PartyInfo->material);
//        $PartyInfo->start_time = date('Y-m-d H:i:s',$PartyInfo->start_time);
//        $PartyInfo->create_time = date('Y-m-d H:i:s',$PartyInfo->create_time);
//        $PartyInfo->over_time = date('Y-m-d H:i:s',$PartyInfo->over_time);
        $return['status'] = $PartyInfo->id ? 200 : 400;
        $return['msg'] = $PartyInfo->id ? 'success' : '该数据不存在！';
        $return['data'] = $PartyInfo->id ? $PartyInfo->toArray() : array();
        return $return;
    }

    //党建选修删除
    public static function partyChoDelete($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $PartyInfo = AdminPartyAct::find($id);
        if (!$PartyInfo){
            $return = array(
                'status' => 400,
                'msg' => '该数据不存在！',
            );
            return $return;
        }
        if ($PartyInfo->status == 2){
            $return = array(
                'status' => 400,
                'msg' => '该活动正在开始中，不能删除！',
            );
            return $return;
        }
        $re = AdminPartyAct::where('id',$id)->delete();
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '删除成功' : '删除失败，请刷新重试！';
        return $return;
    }

    //发布/撤销详情接口
    public static function partyRelease($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $PartyInfo = AdminPartyAct::find($id);
        if (!$PartyInfo){
            $return = array(
                'status' => 400,
                'msg' => '该数据不存在！',
            );
            return $return;
        }
        $status = isset($input['status']) ? $input['status'] : 0;
        if ($status == 2) {
            if ($PartyInfo->start_time < time()) {
                $return = array(
                    'status' => 400,
                    'msg' => '开始时间已过！',
                );
                return $return;
            }
        }
        $re = AdminPartyAct::where('id',$id)->update(['status'=>$status]);
        if ($status == 2) {
            $data = AdminPartyAct::where('id',$id)->select('id','title','address','start_time','abstract','setting_num','submiter')->first();
            $data->member = 2;
            $data->type = $input['type'];
            AdminUser::sendNotice($data);
        }
        $return['status'] = $re ? 200 : 400 ;
        $return['msg'] = $re ? '操作成功' : '操作失败！';
        return $return;
    }

    public static function ChoStart($input){
        $id = $input['id'];
        $type = $input['type'];
        $appid = "wxfeb3236ac589e519";
        $appsecret = "8b857b93de8ab85215266024168d94b0";
        $openids = array();
        $openids = DB::table('sign_up')->join('user','user.id','=','sign_up.user_id')
            ->where('sign_up.type',$type)
            ->where('sign_up.act_id',$id)
            ->where('user.status',1)
            ->pluck('openid');
//        dd($openids);
        $info = array();
        if ($type == 1) {
            $info = AdminPartyAct::find($id);
        } elseif ($type == 2) {
            $info = AdminMultiAcr::find($id);
        }
        $input['keyword4'] = $info->submiter;
        foreach ($openids as $openid) {
            $json_token=AdminPartyAct::http_request("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appid."&secret=".$appsecret);
//            dd($json_token);
            $access_token=json_decode($json_token,true);
            $access_token=$access_token['access_token'];
            $template = array(
                'touser' => $openid,
                'template_id' => "2OCpfJasI-mcT9WFBSsOL0SdZUDALEm-zMuCH4Xj1wY",
//                'url' => $jump_url,
                'topcolor' => "#7B68EE",
                'data' => array(
                    'first' => array('value' => urlencode("您好,您参与得活动已成功开启~"), 'color' => "#FF0000"),
                    'keyword1' => array('value' => urlencode($info['title']), 'color' => '#FF0000'),
                    'keyword2' => array('value' => urlencode($info['address']), 'color' => '#FF0000'),
                    'keyword3' => array('value' => urlencode(date('Y-m-d H:i:s',$info['start_time'])), 'color' => '#FF0000'),
                    'keyword4' => array('value' => urlencode($input['keyword4']), 'color' => '#FF0000'),
//                    'keyword5' => array('value' => urlencode($input['setting_num']), 'color' => '#FF0000'),
                    'remark'=>array('value'=>urlencode("请准时参加！"),'color'=>'#FF0000'),
                )
            );
            $json_template = json_encode($template);
            $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $access_token;
            $res = AdminPartyAct::http_request($url, urldecode($json_template));
//            file_put_contents('test.txt',var_export($res,true));

        }
        $return['status'] = 200;
        $return['msg'] = '信息发送成功';
        extjson($return);
    }

    public static function ChoEnd($input){
        $id = $input['id'];
        $type = $input['type'];
        $appid = "wxfeb3236ac589e519";
        $appsecret = "8b857b93de8ab85215266024168d94b0";
        $openids = array();
        $openids = DB::table('sign_up')->join('user','user.id','=','sign_up.user_id')
            ->where('sign_up.type',$type)
            ->where('sign_up.act_id',$id)
            ->where('user.status',1)
            ->pluck('openid');
//        dd($openids);
        $info = array();
        if ($type == 1) {
            $info = AdminPartyAct::find($id);
            AdminPartyAct::where('id',$id)->update(['status'=>1]);
        } elseif ($type == 2) {
            $info = AdminMultiAcr::find($id);
            AdminMultiAcr::where('id',$id)->update(['status'=>1]);
        }
        $input['keyword4'] = $info->submiter;
        foreach ($openids as $openid) {
            $json_token=AdminPartyAct::http_request("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appid."&secret=".$appsecret);
//            dd($json_token);
            $access_token=json_decode($json_token,true);
            $access_token=$access_token['access_token'];
            $template = array(
                'touser' => $openid,
                'template_id' => "2OCpfJasI-mcT9WFBSsOL0SdZUDALEm-zMuCH4Xj1wY",
//                'url' => $jump_url,
                'topcolor' => "#7B68EE",
                'data' => array(
                    'first' => array('value' => urlencode("您好,您参与得活动已经取消~"), 'color' => "#FF0000"),
                    'keyword1' => array('value' => urlencode($info['title']), 'color' => '#FF0000'),
                    'keyword2' => array('value' => urlencode($info['address']), 'color' => '#FF0000'),
                    'keyword3' => array('value' => urlencode(date('Y-m-d H:i:s',$info['start_time'])), 'color' => '#FF0000'),
                    'keyword4' => array('value' => urlencode($input['keyword4']), 'color' => '#FF0000'),
//                    'keyword5' => array('value' => urlencode($input['setting_num']), 'color' => '#FF0000'),
                    'remark'=>array('value'=>urlencode("请相互通知！"),'color'=>'#FF0000'),
                )
            );
            $json_template = json_encode($template);
            $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $access_token;
            $res = AdminPartyAct::http_request($url, urldecode($json_template));
//            file_put_contents('test.txt',var_export($res,true));

        }
//        die(1);
        $return['status'] = 200;
        $return['msg'] = '信息发送成功';
        extjson($return);
    }
	public static function http_request($url,$data=array()){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        // POST数据
        curl_setopt($ch, CURLOPT_POST, 1);
        // 把post的变量加上
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

}


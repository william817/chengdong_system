<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class WechatShopRecord extends Model
{
    public $table = 'shop_record';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];
}

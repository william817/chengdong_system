<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class WechatScoreRecord extends Model
{
    public $table = 'score_record';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    public static function scoreRecord()
    {
        $user_id = session()->get('userid');
        $data = WechatScoreRecord::where('user_id',$user_id)->orderBy('create_time','desc')->get();
        foreach ($data as $k=>$v){
            $v->create_time = date('Y-m-d',$v->create_time);
            $v->content = '';
            switch ($v->type) {
                case 1:
                    $v->content = '党建必修活动获得'.$v->score.'积分';
                    break;
                case 2:
                    $v->content = '党建选修活动获得'.$v->score.'积分';
                    break;
                case 3:
                    $v->content = '综合必修活动获得'.$v->score.'积分';
                    break;
                case 4:
                    $v->content = '综合选修活动获得'.$v->score.'积分';
                    break;
                case 5:
                    $v->content = '考试获得'.$v->score.'积分';
                    break;
                case 6:
                    $v->content = '兑换商品减去'.$v->score.'积分';
                    break;
                default:

            }
        }
        return $return = array(
            'status' => 200,
            'data' => $data
        );
    }
}

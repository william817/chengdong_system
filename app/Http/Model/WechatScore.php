<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class WechatScore extends Model
{
    public $table = 'score';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    public static function scoreTotal()
    {
        $user_id = session()->get('userid');
        $data['score'] = WechatScore::where('user_id',$user_id)->value('score');
        $count = WechatScore::where('score','>',$data['score'])->count();
        $data['ranking'] = $count + 1;
        $data['count'] = WechatScore::count();
        return $return = array(
            'status' => 200,
            'data' => $data
        );
    }

    public static function scoreRank()
    {
        $data = WechatScore::join('user','user.id','=','score.user_id')
            ->where('user.status',1)->orderBy('score.score','desc')->orderBy('user.id','desc')->
            select('user.username','score.score')->get();
        return $return = array(
            'status' => 200,
            'data' => $data
        );
    }


}

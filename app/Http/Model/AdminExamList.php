<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class AdminExamList extends Model
{
    public $table = 'exam_list';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    public static function examListAdd($input)
    {
        $data['title'] = $input['title'];
        $data['score'] = $input['score'];
        $data['time'] = $input['time'];
        $data['pass_grade'] = $input['pass_grade'];
        $data['pass_times'] = $input['pass_times'];
        $data['status'] = 1;
        $res = AdminExamList::create($data);
        if (!$res) {
            $return['status'] = 400;
            $return['msg'] = '添加失败';
            $return['data'] = array();
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = '添加成功';
        $return['data'] = array();
        extjson($return);
    }

    public static function examListIndex()
    {
        $data = AdminExamList::orderBy('id','desc')->get();
        foreach ($data as $k => $v) {
            $exam_record = AdminExamRecord::where('list_id',$v->id)->get();
            foreach ($exam_record as $key=>$val) {
                if (isset($exam_record[$key+1])){
                    if ($exam_record[$key]['user_id'] == $exam_record[$key+1]['user_id']) {
                        unset($exam_record[$key]);
                    }
                }
            }
            $v->join_nums = count($exam_record);
        }
        if (!$data) {
            $return['status'] = 400;
            $return['msg'] = '暂无数据';
            $return['data'] = array();
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = 'success';
        $return['data'] = $data;
        extjson($return);
    }

    public static function examListUpdate($input)
    {
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        $status = AdminExamList::where('id',$id)->value('status');
//        if ($status == 2 || $status == 3) {
//            $return['status'] = 400;
//            $return['msg'] = '已发布或已经结束，不可修改';
//            $return['data'] = array();
//            extjson($return);
//        }
        $data['title'] = $input['title'];
        $data['score'] = $input['score'];
        $data['time'] = $input['time'];
        $data['pass_grade'] = $input['pass_grade'];
        $data['pass_times'] = $input['pass_times'];
        $res = AdminExamList::where('id',$id)->update($data);
        if (!$res) {
            $return['status'] = 400;
            $return['msg'] = '修改失败';
            $return['data'] = array();
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = '修改成功';
        $return['data'] = array();
        extjson($return);
    }

    public static function examListInfo($input)
    {
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        $res = AdminExamList::find($id);
        if (!$res) {
            $return['status'] = 400;
            $return['msg'] = '无此数据';
            $return['data'] = array();
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = 'success';
        $return['data'] = $res;
        extjson($return);
    }

    public static function examListDelete($input)
    {
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        $status = AdminExamList::where('id',$id)->value('status');
//        if ($status == 2) {
//            $return['status'] = 400;
//            $return['msg'] = '已发布不可删除';
//            $return['data'] = array();
//            extjson($return);
//        }
        DB::beginTransaction();
        AdminExamList::where('id',$id)->delete();
        AdminExam::where('times_id',$id)->delete();
        DB::commit();
        $return['status'] = 400;
        $return['msg'] = '删除失败';
        $return['data'] = array();
        extjson($return);
        DB::rollback();
        $return['status'] = 200;
        $return['msg'] = '删除成功';
        $return['data'] = array();
        extjson($return);
    }

    public static function examListHistory()
    {
        $data = AdminExamList::get();
        $return['status'] = 200;
        $return['msg'] = 'success';
        $return['data'] = $data;
        extjson($return);
    }

    public static function startExam($input)
    {
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        $status = isset($input['status']) && !empty($input['status']) ? $input['status'] : 0;
        if ($id == 0) {
            $return['status'] = 400;
            $return['msg'] = '请选择开启的题目！';
            $return['data'] = array();
            extjson($return);
        }
        if (!AdminExamList::find($id)) {
            $return['status'] = 400;
            $return['msg'] = '改题目不存在！';
            $return['data'] = array();
            extjson($return);
        }
        $res = AdminExamList::where('id',$id)->update(['status'=>$status]);
        if (!$res) {
            $return['status'] = 400;
            $return['msg'] = '开启失败！';
            $return['data'] = array();
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = '开启成功！';
        $return['data'] = array();
        extjson($return);
    }

}

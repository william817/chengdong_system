<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class WechatExamList extends Model
{
    public $table = 'exam_list';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    public static function examList()
    {
        $user_id = session()->get('userid');
//        $user_id = 76;
        $examList = WechatExamList::where('status','!=',1)->orderBy('id','desc')->get();
        foreach ($examList as $k=>$v){
            $exam_pass = WechatExamRecord::where('list_id',$v->id)->where('user_id',$user_id)->where('status',1)->first();
            $v->is_pass = $exam_pass ? 1 : 0;
        }
        return $return = array(
            'status' => 200,
            'data' => $examList
        );
    }

    public static function examListInfo($input)
    {
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            return $return = array(
                'status' =>400,
                'msg' => '请选择需要查看得试题'
            );
        }
        $examList = WechatExamList::find($id);
        $totalGrade = WechatExam::where('times_id',$examList->id)->sum('grade');
        $examList->content = '本次测试总分为'.$totalGrade.'分,通过分数为'.$examList->pass_grade.'分,通过可加学分'.$examList->score.'分';
        return $return = array(
            'status' => 200,
            'data' => $examList
        );
    }

    public static function examListSub($input)
    {
        $user_id = session()->get('userid');
        $id = isset($input['id']) && !empty($input['id']) ? $input['id'] : 0;
        $score = isset($input['score']) && !empty($input['score']) ? $input['score'] : 0;
        $info = WechatExamList::find($id);
        $count = WechatExamRecord::where('list_id',$id)->where('user_id',$user_id)->count();
        $content = '';

        //若通过，增加积分
        //不通过，不加分，不通过次数+1
        if ($score>=$info->pass_grade) {
            DB::beginTransaction();
            WechatScore::where('user_id',$user_id)->increment('exam',$info->score);
            WechatScore::where('user_id',$user_id)->increment('score',$info->score);
            $score_data['user_id'] = $user_id;
            $score_data['act_id'] = $id;
            $score_data['type'] = 5;
            $score_data['score'] = $info->score;
            $score_data['create_time'] = time();
            WechatScoreRecord::create($score_data);
            $exam_data['list_id'] = $id;
            $exam_data['user_id'] = $user_id;
            $exam_data['grade'] = $score;
            $exam_data['status'] = 1;
            $exam_data['create_time'] = $score_data['create_time'];
            WechatExamRecord::create($exam_data);
            $push_data['type'] = 5;
            $push_data['user_id'] = $user_id;
            $push_data['title'] = $info->title;
            $push_data['score'] = $info->score;
            WechatFeedback::sendScoreRecord($push_data);
            DB::commit();
            return $return = array(
                'status' => 200,
                'content' => '考试通过，增加'.$info->score.'学分'
            );
            DB::rollback();
        } else {
            DB::beginTransaction();
            $exam_data['list_id'] = $id;
            $exam_data['user_id'] = $user_id;
            $exam_data['grade'] = $score;
            $exam_data['status'] = 2;
            $exam_data['create_time'] = time();
            WechatExamRecord::create($exam_data);
            DB::commit();
            return $return = array(
                'status' => 200,
                'content' => '考试不通过，您还有'.($info->pass_times - $count - 1).'次答题机会'
            );
            DB::rollback();
        }
    }

    public static function examListCheck($input)
    {
        //判断是否答过题
        $id = $input['id'];
        $user_id = session()->get('userid');
        $exam_pass = WechatExamRecord::where('list_id',$id)->where('user_id',$user_id)->where('status',1)->first();
        if ($exam_pass) {
            $return['status'] = 400;
            $return['msg'] = '您已经参加过该考试了';
            extjson($return);
        }
        $info = WechatExamList::find($id);
        $count = WechatExamRecord::where('list_id',$id)->where('user_id',$user_id)->count();
        if ($info->pass_times <= $count) {
            $return['status'] = 400;
            $return['msg'] = '考试次数已大于'.$info->pass_times.'，不能参加考试';
            extjson($return);
        }
        $return['status'] = 200;
        $return['msg'] = '';
        extjson($return);
    }
}

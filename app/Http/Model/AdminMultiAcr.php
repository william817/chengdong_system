<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use App\Http\Model\AdminUser;
class AdminMultiAcr extends Model
{
    public $table = 'multi_act';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    //综合必修添加
    public static function multiReqSave($input)
    {
        $data['title'] = $input['title'];
        $data['abstract'] = $input['abstract'];
        $data['material'] = isset($input['material']) && !empty($input['material']) ? json_encode($input['material']): '';
        $data['address'] = $input['address'];
        $data['submiter'] = $input['submiter'];
        $data['start_time'] = $input['start_time'];
        $data['create_time'] = time();
        $data['over_time'] = $input['over_time'];
        $data['type'] = 1;
        $data['score'] = $input['score'];
        $data['setting_num'] = 0;
        $data['status'] = 1;
        $re = AdminMultiAcr::create($data);
        $return['status'] = $re->id ? 200 :400;
        $return['msg'] = $re->id ? '添加成功' : '添加失败，请稍后重试！';
        return $return;
    }

    //综合必修列表
    public static function multiReqList($input)
    {
        $status = isset($input['status']) && !empty($input['status']) ? $input['status'] : 0;
        $MultiModel = AdminMultiAcr::where('type',1);
        if ($status != 0){
            $MultiModel = $MultiModel->where('status',$status);
        }
        $multiList = $MultiModel->orderBy('create_time','desc')->select('id','title','start_time','over_time','status')->get();
        foreach ($multiList as $k => $v) {
            $v->really_num = AdminSignIn::where('act_id',$v->id)->where('type',2)->where('style',1)->count();
            $v->pass_num = AdminFeedback::where('act_id',$v->id)->where('type',2)->where('status',2)->count();
            $v->nopass_num = AdminFeedback::where('act_id',$v->id)->where('type',2)->where('status',1)->count();
//            $v->over_time = date('Y-m-d H:i',$v->over_time);
//            $v->start_time = date('Y-m-d H:i',$v->start_time);
        }
        $return['status'] = 200;
        $return['data'] = $multiList;
        return $return;
    }

    //综合必修修改
    public static function multiReqEdit($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $MultiInfo = AdminMultiAcr::find($id);
        if (!$MultiInfo){
            $return = array(
                'status' => 400,
                'msg' => '该数据不存在！',
            );
            return $return;
        }
//        if ($MultiInfo->status != 1){
//            $return = array(
//                'status' => 400,
//                'msg' => '该活动增在开始或者已经结束，不能修改！',
//            );
//            return $return;
//        }
        $data['title'] = $input['title'];
        $data['abstract'] = $input['abstract'];
        $data['material'] = json_encode($input['material']);
        $data['address'] = $input['address'];
        $data['submiter'] = $input['submiter'];
        $data['score'] = $input['score'];
        $data['create_time'] = time();
        $data['start_time'] = $input['start_time'];
        $data['over_time'] = $input['over_time'];
        $re = AdminMultiAcr::where('id',$id)->update($data);
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '修改成功' : '修改失败，请稍后重试！';
        return $return;
    }

    //综合必修详情
    public static function multiReqInfo($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $MultiInfo = AdminMultiAcr::find($id);
        $MultiInfo->material = json_decode($MultiInfo->material);
//        $MultiInfo->start_time = date('Y-m-d H:i:s',$MultiInfo->start_time);
//        $MultiInfo->create_time = date('Y-m-d H:i:s',$MultiInfo->create_time);
//        $MultiInfo->over_time = date('Y-m-d H:i:s',$MultiInfo->over_time);
        $return['status'] = $MultiInfo->id ? 200 : 400;
        $return['msg'] = $MultiInfo->id ? 'success' : '该数据不存在！';
        $return['data'] = $MultiInfo->id ? $MultiInfo->toArray() : array();
        return $return;
    }

    //综合必修删除
    public static function multiReqDelete($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $MultiInfo = AdminMultiAcr::find($id);
        if (!$MultiInfo){
            $return = array(
                'status' => 400,
                'msg' => '该数据不存在！',
            );
            return $return;
        }
        if ($MultiInfo->status == 2){
            $return = array(
                'status' => 400,
                'msg' => '该活动正在开始中，不能删除！',
            );
            return $return;
        }
        $re = AdminMultiAcr::where('id',$id)->delete();
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '删除成功' : '删除失败，请刷新重试！';
        return $return;
    }

    //综合选修添加
    public static function multiChoSave($input)
    {
        $data['title'] = $input['title'];
        $data['abstract'] = $input['abstract'];
        $data['material'] = json_encode($input['material']);
        $data['address'] = $input['address'];
        $data['submiter'] = $input['submiter'];
        $data['start_time'] = $input['start_time'];
        $data['apply_time'] = $input['apply_time'];
        $data['create_time'] = time();
        $data['over_time'] = $input['over_time'];
        $data['type'] = 2;
        $data['score'] = $input['score'];
        $data['setting_num'] = $input['setting_num'];
        $data['status'] = 1;
        $re = AdminMultiAcr::create($data);
        $return['status'] = $re->id ? 200 :400;
        $return['msg'] = $re->id ? '添加成功' : '添加失败，请稍后重试！';
        return $return;
    }

    //综合选修列表
    public static function multiChoList($input)
    {
        $status = isset($input['status']) && !empty($input['status']) ? $input['status'] : 0;
        $MultiModel = AdminMultiAcr::where('type',2);
        if ($status != 0){
            $MultiModel = $MultiModel->where('status',$status);
        }
        $multiList = $MultiModel->orderBy('create_time','desc')->select('id','title','setting_num','start_time','over_time','status')->get(); //缺少查询发布时间
        foreach ($multiList as $k => $v) {
            $v->really_num = AdminSignIn::where('act_id',$v->id)->where('type',2)->where('style',2)->count();
            $v->pass_num = AdminFeedback::where('act_id',$v->id)->where('type',2)->where('status',2)->count();
            $v->nopass_num = AdminFeedback::where('act_id',$v->id)->where('type',2)->where('status',1)->count();
//            $v->over_time = date('Y-m-d H:i',$v->over_time);
//            $v->start_time = date('Y-m-d H:i',$v->start_time);
        }
        $return['status'] = 200;
        $return['data'] = $multiList;
        return $return;
    }

    //综合选修修改
    public static function multiChoEdit($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $MultiInfo = AdminMultiAcr::find($id);
        if (!$MultiInfo){
            $return = array(
                'status' => 400,
                'msg' => '该数据不存在！',
            );
            return $return;
        }
//        if ($MultiInfo->status != 1){
//            $return = array(
//                'status' => 400,
//                'msg' => '该活动增在开始或者已经结束，不能修改！',
//            );
//            return $return;
//        }
        $data['title'] = $input['title'];
        $data['abstract'] = $input['abstract'];
        $data['material'] = json_encode($input['material']);
        $data['address'] = $input['address'];
        $data['submiter'] = $input['submiter'];
        $data['score'] = $input['score'];
        $data['setting_num'] = $input['setting_num'];
        $data['start_time'] = $input['start_time'];
        $data['apply_time'] = $input['apply_time'];
        $data['create_time'] = time();
        $data['over_time'] = $input['over_time'];
        $re = AdminMultiAcr::where('id',$id)->update($data);
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '修改成功' : '修改失败，请稍后重试！';
        extjson($return);
    }

    //综合选修详情
    public static function multiChoInfo($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $MultiInfo = AdminMultiAcr::find($id);
        $MultiInfo->material = json_decode($MultiInfo->material);
//        $MultiInfo->start_time = date('Y-m-d H:i:s',$MultiInfo->start_time);
//        $MultiInfo->create_time = date('Y-m-d H:i:s',$MultiInfo->create_time);
//        $MultiInfo->over_time = date('Y-m-d H:i:s',$MultiInfo->over_time);
        $return['status'] = $MultiInfo->id ? 200 : 400;
        $return['msg'] = $MultiInfo->id ? 'success' : '该数据不存在！';
        $return['data'] = $MultiInfo->id ? $MultiInfo->toArray() : array();
        extjson($return);
    }

    //综合选修删除
    public static function multiChoDelete($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $MultiInfo = AdminMultiAcr::find($id);
        if (!$MultiInfo){
            $return = array(
                'status' => 400,
                'msg' => '该数据不存在！',
            );
            return $return;
        }
        if ($MultiInfo->status == 2){
            $return = array(
                'status' => 400,
                'msg' => '该活动正在开始中，不能删除！',
            );
            return $return;
        }
        $re = AdminMultiAcr::where('id',$id)->delete();
        $return['status'] = $re ? 200 : 400;
        $return['msg'] = $re ? '删除成功' : '删除失败，请刷新重试！';
        return $return;
    }

    //发布/撤销详情接口
    public static function multiChoRelease($input)
    {
        $id = isset($input['id']) &&!empty($input['id']) ? $input['id'] : 0;
        if ($id == 0) {
            $return = array(
                'status' => 400,
                'msg' => '请选择活动',
            );
            return $return;
        }
        $MultiInfo = AdminMultiAcr::find($id);
        if (!$MultiInfo){
            $return = array(
                'status' => 400,
                'msg' => '该数据不存在！',
            );
            return $return;
        }
        $status = isset($input['status']) ? $input['status'] : 0;
        if ($status == 2) {
            if ($MultiInfo->start_time < time()) {
                $return = array(
                    'status' => 400,
                    'msg' => '开始时间已过！',
                );
                return $return;
            }
        }
        $re = AdminMultiAcr::where('id',$id)->update(['status'=>$status]);
        if ($status == 2) {
            $data = AdminMultiAcr::where('id',$id)->select('id','title','address','start_time','abstract','setting_num','submiter')->first();
            $data->member = 2;
            $data->type = $input['type'];
            AdminUser::sendNotice($data);
        }
        $return['status'] = $re ? 200 : 400 ;
        $return['msg'] = $re ? '操作成功' : '操作失败！';
        return $return;
    }

}


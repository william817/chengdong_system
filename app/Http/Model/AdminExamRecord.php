<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class AdminExamRecord extends Model
{
    public $table = 'exam_record';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];
}

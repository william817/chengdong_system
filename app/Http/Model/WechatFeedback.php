<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class WechatFeedback extends Model
{
    public $table = 'feedback';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $guarded = [];

    /**
     * feedbackSave
     * 反馈增加
     * @param $input
     * @return array
     */
    public static function feedbackSave($input)
    {
        $user_id = session()->get('userid');
        $feed_info = WechatFeedback::where('user_id',$user_id)->where('act_id',$input['act_id'])->where('type',$input['type'])->first();
        if ($feed_info) {
            return $return = array(
                'status' => 400,
                'msg' => '您已经提交过反馈了'
            );
        }
        $List = array();
        if ($input['type'] == 1) {
            $List = WechatPartyAct::find($input['act_id']);
            $member = WechatUser::where('id',$user_id)->where('status',1)->value('member');
//            if (empty($member) || $member != 1) {
//                return $return = array(
//                    'status' => 400,
//                    'msg' => '只有党员可以参加'
//                );
//            }
        } elseif ($input['type'] == 2) {
            $List = WechatMultiAcr::find($input['act_id']);
        }
        $sign_info = WechatSignIn::where('user_id',$user_id)->where('act_id',$input['act_id'])->where('type',$input['type'])->where('style',$List->type)->first();
        if (empty($sign_info)){
            return $return = array(
                'status' => 400,
                'msg' => '您还未签到'
            );
        }
        $data['act_id'] = $input['act_id'];
        $data['type'] = $input['type'];
        $data['user_id'] = $user_id;
        $data['username'] = WechatUser::where('id',$user_id)->value('username');
        $data['content'] = $input['content'];
        $data['feed_time'] = time();
        $data['num'] = 0;
        $data['status'] = 1;
        $re = WechatFeedback::create($data);
//        $score_data['user_id'] = $user_id;
//        $score_data['act_id'] = $input['act_id'];
//        $score_data['score'] = $List->score;
//        $score_data['create_time'] = time();
//        $score_data['type'] = '';
//        $push_data = array();
//        if ($input['type'] == 1) {
//            if ($List->type == 1) {
//                $score_data['type'] = 1;
//                WechatScore::where('user_id',$user_id)->increment('party_must',$List->score);
//                $push_data['type'] = 1;
//            } elseif ($List->type == 2) {
//                $score_data['type'] = 2;
//                WechatScore::where('user_id',$user_id)->increment('party_choice',$List->score);
//                $push_data['type'] = 2;
//            }
//        } elseif ($input['type'] == 2) {
//            if ($List->type == 1) {
//                $score_data['type'] = 3;
//                WechatScore::where('user_id',$user_id)->increment('multi_party',$List->score);
//                $push_data['type'] = 3;
//            } elseif ($List->type == 2) {
//                $score_data['type'] = 4;
//                WechatScore::where('user_id',$user_id)->increment('multi_choice',$List->score);
//                $push_data['type'] = 4;
//            }
//        }
//        WechatScore::where('user_id',$user_id)->increment('score',$List->score);
//        WechatScoreRecord::create($score_data);
//        $push_data['user_id'] = $user_id;
//        $push_data['title'] = $List->title;
//        $push_data['score'] = $List->score;
//        self::sendScoreRecord($push_data);
////            file_put_contents('test.txt',var_export($res,true));

        $return['status'] = $re->id ? 200 : 400;
        $return['msg'] = $re->id ? '反馈成功，待审核' : '反馈失败';
        return $return;
    }

    /**
     * @param $input
     * @return array
     */
    public static function feedbackList($input)
    {
        $user_id = session()->get('userid');
        $id = isset($input['id']) ? $input['id'] : 0 ;
        $type = isset($input['type']) ? $input['type'] : 0 ;
        $style = isset($input['style']) ? $input['style'] : 0 ;
        $feedbackList = array();
        if ($type == 1){
            $feedbackList = WechatFeedback::join('party_act','party_act.id','=','feedback.act_id')
                ->where('feedback.act_id',$id)
                ->where('party_act.type',$style)
                ->where('feedback.type',$type)
                ->where('feedback.status',2)
                ->orderBy('id','desc')
                ->select('feedback.*')
                ->get();
        } elseif ($type == 2) {
            $feedbackList = WechatFeedback::join('multi_act','multi_act.id','=','feedback.act_id')
                ->where('feedback.act_id',$id)
                ->where('multi_act.type',$style)
                ->where('feedback.type',$type)
                ->where('feedback.status',2)
                ->orderBy('id','desc')
                ->select('feedback.*')
                ->get();
        }
        if ($feedbackList){
            $feedbackList = $feedbackList->toArray();
        }
        foreach ($feedbackList as $k=>&$v){
            $zanInfo = WechatZanRecord::where('user_id',$user_id)->where('feed_id',$v['id'])->first();
            if ($zanInfo){
                $v['is_zan'] = 1;
            } else {
                $v['is_zan'] = 0;
            }
            $v['contentY'] = $v['username'].':'.'  '.$v['content'];
        }
        $feedbackList = array_values($feedbackList);
        return $return = array(
            'status' => 200,
            'data' => $feedbackList
        );
    }


    public static function sendScoreRecord($data)
    {
        $score_type = '';
        $score_reason = '';
        switch ($data['type']) {
            case 1:
                $score_type = '学分增加';
                $score_reason = '参加党建必修'. $data['title'] .'活动';
                break;
            case 2:
                $score_type = '学分增加';
                $score_reason = '参加党建选修'. $data['title'] .'活动';
                break;
            case 3:
                $score_type = '学分增加';
                $score_reason = '参加综合必修'. $data['title'] .'活动';
                break;
            case 4:
                $score_type = '学分增加';
                $score_reason = '参加综合选修'. $data['title'] .'活动';
                break;
            case 5:
                $score_type = '学分增加';
                $score_reason = '参加'. $data['title'] .'考试';
                break;
            case 6:
                $score_type = '学分减少';
                $score_reason = '兑换'. $data['title'] .'商品';
                break;
        }
        $appid = "wxfeb3236ac589e519";
        $appsecret = "8b857b93de8ab85215266024168d94b0";
        $openids = WechatUser::where('id',$data['user_id'])->value('openid');
        $json_token=AdminUser::http_request("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appid."&secret=".$appsecret);
//            dd($json_token);
        $access_token=json_decode($json_token,true);
        $access_token=$access_token['access_token'];
        $template = array(
            'touser' => $openids,
            'template_id' => "KfPSmDfYIErNoYSSUH3iUGlTb2H27xpUDMAku9lGxuI",
//                'url' => $jump_url,
            'topcolor' => "#7B68EE",
            'data' => array(
                'first' => array('value' => urlencode("您好,您得学分有了新变动~"), 'color' => "#FF0000"),
                'keyword1' => array('value' => urlencode($score_type), 'color' => '#FF0000'),
                'keyword2' => array('value' => urlencode($score_reason), 'color' => '#FF0000'),
                'keyword3' => array('value' => urlencode($data['score']), 'color' => '#FF0000'),
//                    'keyword5' => array('value' => urlencode($input['setting_num']), 'color' => '#FF0000'),
                'remark'=>array('value'=>urlencode("请到个人中心中查看详情信息！"),'color'=>'#FF0000'),
            )
        );
        $json_template = json_encode($template);
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $access_token;
        $res = AdminUser::http_request($url, urldecode($json_template));
//        $return['status'] = 200;
//        $return['msg'] = '审核通过成功';
//        extjson($return);
        return $res;
    }

}

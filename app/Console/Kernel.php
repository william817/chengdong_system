<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     * 定义Artisan命令
     * @var array
     */
    protected $commands = [
                \App\Console\Commands\Timing::class,
    ];

    /**
     * Define the application's command schedule.
     * 定义调度任务
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('test:ok')
             ->timezone('Asia/ShangHai')
//                  ->everyMinute();
                  ->monthly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Model\Room;
class Timing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:ok';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $test = 1;
//        file_put_contents('test.txt', var_export($test,true), FILE_APPEND);
        Room::createCostInformation();
    }
}

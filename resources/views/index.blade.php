<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
		<title>印象城东</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/index.css')}}" />
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
		<script src="{{asset('js/hotcss.js')}}"></script>

	</head>

	<body>
		<div class="list_con">
			<div class="box">
				<img src="{{asset('img/bg1.jpg')}}" />
			</div>
			<img class="up_w" src="{{asset('image1/up_w.png')}}" />
			<div class="navigation flex-between">
				<a href="{{url('Learning/index')}}">
				{{--<a href="{{url('holdOn')}}">--}}
					<div class="left flex-center">
						<img src="{{asset('image1/index2.png')}}" /> 党建学习
					</div>
				</a>
				<a href="{{url('synth/index')}}">
					<div class="right flex-center">
						<img src="{{asset('image1/index3.png')}}" /> 综合培训
					</div>
				</a>
			</div>
			<div class="navigation flex-between">
				<a href="{{url('text/index')}}">
				{{--<a href="{{url('holdOn')}}">--}}
					<div class="right flex-center">
						<img src="{{asset('image1/index1.png')}}" /> 在线测试
					</div>
				</a>
				<a href="{{url('my/index')}}">
					<div class="left flex-center">
						<img src="{{asset('image1/people_new.png')}}" /> 个人中心
					</div>
				</a>
			</div>
			<div class="navigation flex-between">
				<div class="new_left flex-center">
					<img src="{{asset('image1/chengdong_e.png')}}" />
				</div>
				<a href="{{url('opinion_list')}}">
				{{--<a href="{{url('holdOn')}}">--}}
					<div class="new_right flex-center">
						<img src="{{asset('image1/feed_back.png')}}" />意见征集
					</div>
				</a>
			</div>

		</div>
		<script type="text/javascript">
			$(function() {

			})
		</script>
	</body>

</html>
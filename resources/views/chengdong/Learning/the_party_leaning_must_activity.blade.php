<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>党建必修</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/the_party_leaning_must_activity.css?v=2')}}" />
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/public.js')}}"></script>
		<script src="{{asset('js/leftTime.min.js')}}"></script>
		<script src="{{asset('layer/layer.js')}}"></script>


	</head>
	<body>
		<div class="list_con">
			<div class="title">党的十九大报告</div>
			<div class="list_pos">
				<div class="pos flex-center">
					<img src="{{asset('image1/danjiant.png')}}" />
				</div>
				<div class="time1">
					<div class="left">时间</div>
					<div class="right" id="time"></div>
				</div>
				<div class="time">
					<div class="left">地点</div>
					<div class="right" id="address"></div>
				</div>
				<div class="time">
					<div class="left">简介</div>
					<div class="right" id="abstract"></div>
				</div>
				<div class="time">
					<div class="left">学习资料:</div>
				</div>
				<div class="time">
					<div class="left zhiliao" id="zhiliao"></div>
				</div>
			</div>

			<div class="button_con flex-around">
				<button id="apply" class="click">
					<span id="to_apply">
						我要签到
					</span>
					<span id="success">
						签到成功
					</span>
			    </button>
				<button id="feedback">
					<span id="to_feed">
						学习反馈
					</span>
					<span id="feed_success">
						反馈成功
					</span>
			    </button>
			</div>
			<div class="count_down" id="dataInfoShow">
				<div class="center" style="background: #ff0000">
				</div>
				<div class="data-show-box" id="dateShow" style="position: absolute;left:0.5rem;top:0.25rem">
					<span class="date-tiem-span d">00</span>天
					<span class="date-tiem-span h">00</span>时
					<span class="date-tiem-span m">00</span>分
					<span class="date-s-span s">00</span>秒
				</div>


			</div>
			<div id="feed">
				<div class="feed_title flex-around" id="feed_title">
					
				</div>
				<div id="list_con">

				</div>
			</div>
			<!--底部的固定内容-->
			<img class="img_pos" src="{{asset('image1/d_bg_pos.png')}}" />
			<img class="bottom" src="{{asset('image1/bottom.png')}}" />
		</div>
	</body>

</html>

<script>
	//获取参数的方法
	var start_time,current_time,over_time;
	var getParam = function (name) {
		var search = document.location.search;
		var pattern = new RegExp("[?&]" + name + "\=([^&]+)", "g");
		var matcher = pattern.exec(search);
		var items = null;
		if (null != matcher) {
			try {
				items = decodeURIComponent(decodeURIComponent(matcher[1]));
			} catch (e) {
				try {
					items = decodeURIComponent(matcher[1]);
				} catch (e) {
					items = matcher[1];
				}
			}
		}
		return items;
	};
	$(document).ready(function () {
		var id = getParam('id');
		$.post("{{url('wechat/party/info')}}", {
			id: id
		}, function (data) {
//			console.log(data);
			if (data.status == 200) {
				$('.title').html(data.data.title);
				$('#time').html(data.data.start_time);
				$('#address').html(data.data.address);
				$('#abstract').html(data.data.abstract);
//                $('.count_down1').html(data.data.over_time);
				start_time = data.data.start_time;
				over_time = data.data.over_time;
				current_time = data.data.current_time;
				var str = '';
				if (data.data.material != null) {
					for (var i = 0; i < data.data.material.length; i++) {
						str += '<div class="list flex-center">' +
//								'<div class="left">' + '学习资料:' + '</div>' +
								'<div class="list_a">' + '<a href="' + data.data.material[i].mate_url + '">' + data.data.material[i].mate_time + '</a>' +
								'</div>' + '</div>';
					}
					$('.zhiliao').append(str);
				} else {
					str += '暂无学习资料...';
					$('.zhiliao').append(str);
				}
				if (data.data.is_signIn == 1) {
					$("#to_apply").hide();
					$("#success,.count_down,#feed").show();
				}
			}
			setDateImportFn(start_time, over_time, current_time);
		});
	});

	function setDateImportFn(start_time, over_time, current_time) {
		// var over_time=$(".over_time").text(),
		//     current_time=$(".current_time").text();
		var data1 = {};
		//当前时间
		data1.startdate = current_time;
		//反馈结束时间CTB
		data1.enddate = over_time;
		//console.log(current_time);
		//console.log(over_time);
		clearTime2 = $.leftTime(data1, function (d) {
			if (d.status) {
				var $dateShow1 = $("#dateShow");
				$dateShow1.find(".d").html(d.d);
				$dateShow1.find(".h").html(d.h);
				$dateShow1.find(".m").html(d.m);
				$dateShow1.find(".s").html(d.s);
				$("#dataInfoShow").html();

			}
		});
	}


	$(document).ready(function () {
		var id = getParam('id');
		var type = 1;
		var style = 1;
		$.post("{{url('wechat/feedback/list')}}", {
			id: id, type: type, style: style
		}, function (data) {
			if (data.status == 200) {
				if (data.data.length > 0) {
					var feed_title = '<span>反馈区</span><a href="{{url("feed/list")}}?id='+id+'&type='+type+'&style='+style +'">显示更多</a>';
					$("#feed_title").append(feed_title);
				}
				var str = '';
				for (var i = 0; i < data.data.length; i++) {
					var str1 = '';
					if (data.data[i].is_zan == 1) {
						str1 += '<img src="{{asset('img/zan.png')}}" />';
					} else {
						str1 += '<img id="img' + i + '" src="{{asset('img/nozan.png')}}" onclick="zan(' + data.data[i].id + ',' + i + ')" />';
					}
					str += '<div class="list flex-center">' +
							'<div class="left data-content " >' + data.data[i].username + ':' + data.data[i].content + '</div>' +
							'<div class="right flex-center ">' + str1 + '&nbsp' + '<span id = "num' + i + '">' + data.data[i].num + '</span>' +
							'</div>' +
							'</div>';
				}
				$('#list_con').append(str);
			}
		});
	});

	$("#to_apply").click(function () {
		var id = getParam('id');
		var type = 1;
		$.post("{{url('wechat/sign/in')}}", {
			id: id, type: type
		}, function (data) {
			console.log(data);
			if (data.status == 200) {
				layer.open({
					content: '签到成功'
					,btn: '我知道了'
				});
				$("#to_apply").hide();
				$("#success,.count_down,#feed").show();
			} else {
				layer.open({
					content: data.msg
					,btn: '我知道了'
				});
			}
		});

	});
	$("#success").click(function(){
		layer.open({
			content: '您已经签到过了！'
			,btn: '我知道了'
		});
//		alert("您已经签到过了！");
	});
	$("#to_feed").click(function () {
		var id = getParam('id');
		window.location.href = "{{url('Learning/feed')}}?id=" + id;
	});

	function zan(id, i) {
		$.post("{{url('wechat/zan/add')}}", {
			id: id
		}, function (data) {
			if (data.status === 200) {
				var src = "{{asset('img/zan.png')}}";
				$("#img" + i).prop("src", src);
				var num = $("#num" + i).html();
				num = $("#num" + i).html(++num);
			} else {
				layer.open({
					content: data.msg
					,btn: '我知道了'
				});
			}
		});
	}

</script>
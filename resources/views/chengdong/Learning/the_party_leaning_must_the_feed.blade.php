<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>党建必修</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/the_party_leaning_must_the_feed.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/public.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
		<script src="{{asset('layer/layer.js')}}"></script>

		<style>
			.content1{
				font-size: 40px;
				text-align: center;
				margin: 1.2rem 1rem;
			}
		</style>
	</head>

	<body>
		<div class="list_con">
			<div class="title">学习反馈</div>
			<div class="list_pos">
				<img class="pos" src="{{asset('img/fangkui.png')}}"/>
				<textarea placeholder="请输入...(反馈内容不得小于50字，不得大于300字)" name="feedback" rows="2" cols="" id="feedback"></textarea>
			</div>
			<button id="apply" class="click">
				<span class="apply">
					提交
				</span>
			</button>
			<div class="seccess">
				<div class="applysccsess flex-center">
					提交成功
					<img src="{{asset('image1/ok.png')}}" />
				</div>
			</div>
			<!--底部的固定内容-->
			<img class="img_pos" src="{{asset('image1/d_bg_pos.png')}}" />
			<img class="bottom" src="{{asset('image1/bottom.png')}}" />
		</div>
		<div class="model">
			<div class="content">
				<div class="content1"></div>
			</div>
			<button id="go_back" class="click">
				我知道了
			</button>
		</div>
	</body>

</html>

<script>
	var getParam = function(name) {
		var search = document.location.search;
		var pattern = new RegExp("[?&]" + name + "\=([^&]+)", "g");
		var matcher = pattern.exec(search);
		var items = null;
		if(null != matcher) {
			try {
				items = decodeURIComponent(decodeURIComponent(matcher[1]));
			} catch(e) {
				try {
					items = decodeURIComponent(matcher[1]);
				} catch(e) {
					items = matcher[1];
				}
			}
		}
		return items;
	};
	$('#apply').click(function () {
		var id = getParam('id');
		var type = 1;
		var content = $('#feedback').val();
		if (content.length > 300 || content.length < 50) {
			$('#feedback').html(content);
			$('#apply').css('display', 'none');
			$('.model').show();
			$('.content1').html('反馈内容不得小于50字，不得大于300字');
		} else {
			$.post("{{url('wechat/feedback/add')}}", {
				act_id: id, type: type, content: content
			}, function (data) {
				console.log(data);
				if (data.status == 200) {
					//layer.open({
					//	content: '提交成功，请等待审核！'
					//	,btn: '我知道了'
					//});
					alert('提交成功，请等待审核！');
					window.history.back(-1);
				} else {
					$('#feedback').html(content);
					$('#apply').css('display', 'none');
					$('.model').show();
					$('.content1').html(data.msg);
				}
			});
		}

//
	});
	$("#go_back").click(function () {
		$('.model').hide();
		$('#apply').show();
		$('.apply').html('重新提交');
	});
	
	function GetLength(str) {
		return str.replace(/[\u0391-\uFFE5]/g,"aa").length;  //先把中文替换成两个字节的英文，在计算长度
	}
</script>
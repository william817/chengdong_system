<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>党建学习</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/the_party_leaning.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/public.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	</head>
	<body>
		<div class="list_con">
			<div class="content">
			<img class="img_pos" src="{{asset('image1/d_bg_pos.png')}}"/>
			<div class="list_pos">
				<div class="title flex-center">
						<img class="img1" src="{{asset('image1/dang_2.png')}}"/>
						<img class="img2" src="{{asset('image1/line_2.png')}}"/>
						<div class="">
							<p class="big">党建学习</p>
						</div>
				</div>
				<div id="list_con">
					<a  href="{{url('Learning/must')}}">
						<div class="list flex-center">
	                         <div class="right">
	                         	必修课
	                         </div>
						</div>
					</a>
					<a  href="{{url('Learning/take')}}">
						<div class="list flex-around">
							<div class="right">
								选修课
							</div>
						</div>
					</a>
				</div>
				<img src="{{asset('image1/up_r.png')}}" class="showmore">
				</img>
			</div>
			</div>
		</div>

	</body>

</html>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>
    <title>意见征集</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/opinion_list.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}"/>
    <script src="{{asset('js/hotcss.js')}}"></script>
    <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
</head>
<body>
<div class="list_con">
    <div class="header">
        <img class="logo" src="{{asset('img/option_bg.png')}}"/>
        <div class="header_botttom flex-center">
            <img class="line" src="{{asset('image1/line_1.png')}}"/>
        </div>
    </div>
    <div class="title flex-center">
        我的意见
    </div>
    <div class="list_pos">
        <img class="pos" src="{{asset('img/option.png')}}"/>

        {{--<textarea placeholder="请输入" name="feedback" rows="2" cols=""></textarea>--}}
    </div>
    <button id="apply" class="click">
				<span class="apply">
					我要提意见
				</span>
    </button>
    <div class="model">
        <div class="content">

        </div>
        <button id="go_back" class="click">
            返回
        </button>
    </div>
</div>
</body>

</html>
<script>
    $(document).ready(function () {
        $.post("{{url('wechat/option/list')}}", {}, function (data) {
            if (data.data.length == 0) {
                var str1 = '<div class="option">您暂未提交任何意见!</div>';
                $('.list_pos').append(str1);
            }
            if (data.status == 200) {
                var str = '';
                for (var i = 0; i < data.data.length; i++) {
                    str += '<div onclick="info(' + data.data[i].id + ')">' +
                            '<span>' + data.data[i].create_time + '&nbsp;提交</span>' +
                            '</div>';
                }
                $('.list_pos').append(str);
            }
        });

        $('#apply').click(function () {
            window.location.href = "{{url('opinion')}}";
        });
    });

    function info(id) {
        window.location.href = "{{url('opinion_info')}}?id=" + id;
    }

</script>
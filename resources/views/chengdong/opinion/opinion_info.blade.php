<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>意见征集</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/the_synth_must_activity_feed.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	</head>
	<body>
		<div class="list_con">
			<div class="header">
				<img class="logo" src="{{asset('img/option_bg.png')}}" />
				<div class="header_botttom flex-center">
					<img class="line" src="{{asset('image1/line_1.png')}}" />
				</div>
			</div>
			<div class="title flex-center">
				意见征集
			</div>
			<div class="list_pos">
				<img class="pos" src="{{asset('img/option.png')}}" />
				<textarea placeholder="请输入" name="feedback" rows="2" cols="" class="content"></textarea>
			</div>
			{{--<button id="apply" class="click">--}}
				{{--<span class="apply">--}}
					{{--提交--}}
				{{--</span>--}}
			{{--</button>--}}
			<div class="model">
				<div class="content">
					
				</div>
				<button id="go_back" class="click">
				 返回
			</button>
			</div>
		</div>
	</body>

</html>
<script>
	var getParam = function (name) {
		var search = document.location.search;
		var pattern = new RegExp("[?&]" + name + "\=([^&]+)", "g");
		var matcher = pattern.exec(search);
		var items = null;
		if (null != matcher) {
			try {
				items = decodeURIComponent(decodeURIComponent(matcher[1]));
			} catch (e) {
				try {
					items = decodeURIComponent(matcher[1]);
				} catch (e) {
					items = matcher[1];
				}
			}
		}
		return items;
	};
	$(document).ready(function() {
		var id = getParam('id');
		$.post("{{url('wechat/option/info')}}", {
			id:id
		}, function (data) {
			if (data.status == 200) {
				$('.content').val(data.data.content);
			}
		});
	});
</script>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{asset('css/feed_list.css')}}"/>
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
		<script src="{{asset('layer/layer.js')}}"></script>
	</head>
	<body>
		<div id="content">
		</div>
		<script type="text/javascript">
			$("#content").on('click','.list .like .nozan',function(){ 
				$(this).hide();
				$(this).next().show();
			})
		</script>
	</body>
</html>
<script>
	var getParam = function(name) {
		var search = document.location.search;
		var pattern = new RegExp("[?&]" + name + "\=([^&]+)", "g");
		var matcher = pattern.exec(search);
		var items = null;
		if(null != matcher) {
			try {
				items = decodeURIComponent(decodeURIComponent(matcher[1]));
			} catch(e) {
				try {
					items = decodeURIComponent(matcher[1]);
				} catch(e) {
					items = matcher[1];
				}
			}
		}
		return items;
	};

	$(document).ready(function () {
		var id = getParam('id');
		var type = getParam('type');
		var style = getParam('style');
		$.post("{{url('wechat/feedback/list')}}", {
			id: id, type: type, style:style
		}, function (data) {
			if (data.status == 200) {

				var str = '';
				for (var i = 0; i < data.data.length; i++) {
					var str1 = '';
					if (data.data[i].is_zan == 1) {
						str1 += '<img src="{{asset('img/zan.png')}}" />';
					} else {
						str1 += '<img id="img' + i + '" src="{{asset('img/nozan.png')}}" onclick="zan(' + data.data[i].id + ',' + i + ')" />';
					}
					str += '<div class="list">' + '<div class="name">' + data.data[i].username + '</div>' +
							'<div class="text">' + data.data[i].content + '</div>' +
							'<div class="like flex-end">' + str1 + '<span id = "num' + i + '">' + data.data[i].num + '</span>' + '</div>' + '</div>';
				}
				$('#content').append(str);
			}
		});
	});
	function zan(id, i) {
		$.post("{{url('wechat/zan/add')}}", {
			id: id
		}, function (data) {
			if (data.status === 200) {
				var src = "{{asset('img/zan.png')}}";
				$("#img" + i).prop("src", src);
				var num = $("#num" + i).html();
				num = $("#num" + i).html(++num);
			} else {
				layer.open({
					content: data.msg
					,btn: '我知道了'
				});
			}
		});
	}
</script>

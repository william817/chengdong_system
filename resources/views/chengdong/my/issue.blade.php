<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>我要培训</title>
		<link rel="stylesheet" type="text/css" href="../../css/wanttrain.css" />
		<link rel="stylesheet" type="text/css" href="../../css/public.css" />
		<script src="../../js/hotcss.js"></script>
		<script src="../../js/public.js"></script>
		<script src="../../js/jquery-3.1.1.min.js"></script>
	</head>
	<body>
		<div class="list_con">
			<div class="title">
				<input type="" name="" id="" value="" placeholder="培训名称" />
			</div>
			<form action="" method="post">
				<div class="list_pos">
					<div class="pos flex-center">
						<img src="../../image1/peixun.png"/>
					</div>
					<div class="list list1 flex-between">
						<div class="left">培训名称</div><input type="" name="name" id="" placeholder="培训名称" value="" />
					</div>
					<div class="list list2 flex-between">
						<div class="left">培训内容</div><input type="" name="content" id="" placeholder="培训内容" value="" />
					</div>
					<div class="list list1 flex-between">
						<div class="left">培训地点</div><input type="" name="address" id="" placeholder="地点" value="" />
					</div>
					<div class="list flex-between">
						<div class="left">培训时间</div><input type="" name="time" id="" placeholder="时间" value="" />
					</div>
				</div>
			</form>
			<button id="apply">
				发起培训
			</button>
			<div class="seccess">
				<div class="applysccsess flex-center">
					发起成功
					<img src="../../image1/ok.png"/>
				</div>
			</div>
			<!--底部的固定内容-->
			<img class="img_pos" src="../../image1/d_bg_pos.png"/>
            <img class="bottom" src="../../image1/bottom.png"/>
		</div>
		<script type="text/javascript">
				$('#apply').click(function() {
					$(this).css('display','none');
					$('.seccess').show();
				});
		</script>
	</body>

</html>
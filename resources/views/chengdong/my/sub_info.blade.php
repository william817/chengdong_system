<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>信息审核</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/info.css?v=2')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
		<script src="{{asset('layer/layer.js')}}"></script>
		<style type="text/css">
			.demo1:after {
				content: "";
				width: 20px;
				height: 20px;
			}
			 .content1{
				 font-size: 35px;
				 text-align: center;
				 margin: 1.2rem 1rem;
			 }
		</style>
	</head>

	<body>
		<div class="logo">
			<img src="{{asset('image1/info.jpg')}}" />
			<div class="info_logo flex-center">
				<img src="{{asset('image1/demo_logo.png')}}" />
			</div>
		</div>
		<div class="title">
			个人信息
		</div>

		<div class="content">
			<div class="list">
				姓名
			</div>
			<div class="input input1 flex-center">
				<input type="text" name="username" id="username" value=""/>
			</div>
			<div class="checked flex-center">
				<div class="lists flex-center">
					<div id="check1" class="check"></div>
					中共党员
				</div>
				<div class="lists flex-center">
					<div id="check2" class="check "></div>
					非中共党员
				</div>
			</div>

		</div>
			<div class="submit" type="submit" id="submit" >提交 </div>

		<div class="model">
			<div class="content" id="prompt">
				<div class="content1"></div>
			</div>
			<button id="go_back" class="click">
				我知道了
			</button>
		</div>
		<script type="text/javascript">
		</script>
	</body>

</html>
<script>
	var member  = 0,
			data = {};
	$("#check1").click(function() {
		$(this).addClass('active');
		$("#check2").removeClass('active');
		member = 1;
	});
	$("#check2").click(function() {
		$(this).addClass('active');
		$("#check1").removeClass('active');
		member = 2;
	});

	$(document).ready(function() {
		$(".model").hide();
		var str1 = '';
		$('#prompt').append(str1);
		$.post("{{url('wechat/user/pass')}}",  function(data) {
			var str = '';
			if (data.data.status == 3){
				$(".model").show();
				str = '请完善您的个人信息！';
				$('.content1').append(str);
			} else if (data.data.status == 2) {
				$('.submit').text("待审核");
				$('.submit').css('pointer-events','none');
				$('#username').val(data.data.username);
				if (data.data.member == 1) {
					$('#check1').addClass('active');
					$("#check2").removeClass('active');
					member = 1;
				}else if (data.data.member == 2) {
					$("#check2").addClass('active');
					$("#check1").removeClass('active');
					member = 2;
				}
				$(".model").show();
				str = '您已提交个人信息，请耐心等候后台审核！';
				$('.content1').append(str);
			}else if (data.data.status == 1){
				$('.submit').text("已审核");
				$('.submit').css('pointer-events','none');
			}
		})
	});

	$('#submit').click(function() {
		var str1 = '';
		$('.content1').html(str1);
		$(".model").hide();
		var username = $('#username').val();
            $.post("{{url('wechat/submit/info')}}", {
                username: username,
                member:member

            }, function(data) {
                if(data.status == 200) {
                    $('#username').html(username);
                    if (member == 1) {
                        $('#check1').addClass('active');
                        $("#check2").removeClass('active');
                    }else if (member == 2) {
                        $("#check2").addClass('active');
                        $("#check1").removeClass('active');
                    }
                    var str = '提交成功，请等待身份审核！';
                    console.log(str);
                    $('.content1').append(str);
                    $(".model").show();
                } else {
					layer.open({
						content: '提交成功，等待身份审核'
						,btn: '我知道了'
					});
                }
            });
	});
	$("#go_back").click(function() {
			$(".model").hide()
		})
</script>
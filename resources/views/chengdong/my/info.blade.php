<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>个人信息</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/info.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
		<style type="text/css">
			.demo1:after {
				content: "";
				width: 20px;
				height: 20px;
			}
		</style>
	</head>

	<body>
		<div class="logo">
			<img src="{{asset('image1/info.jpg')}}" />
			<div class="info_logo flex-center">
				<img src="{{asset('image1/demo_logo.png')}}" />
			</div>
		</div>
		<div class="title">
			个人信息
		</div>
			<div class="content">
				<div class="list">
					姓名
				</div>
				<div class="input input1 flex-center">
					<input type="text" name="username" id="username" value="" />
				</div>
				<div class="checked flex-center">
					<div class="lists flex-center">
						<div id="check1" class="check"></div>中共党员
					</div>
					<div class="lists flex-center">
						<div id="check2" class="check "></div>非中共党员
					</div>
				</div>

			</div>
			<div class="submit"/>
				提交
			</div>
		<div class="model model_1">
			<div class="content">
				请提交完整信息！
			</div>
			<div id="go_back" class="click">
				我知道了
			</div>
		</div>
		<div class="model model_2">
			<div class="content">
				提交成功！
			</div>
			<div id="go_back_2" class="click">
				我知道了
			</div>
		</div>
		<script type="text/javascript">
		</script>
	</body>

</html>
<script>
	var member  = 0,
		data = {};
//	$("#check1").click(function() {
//		$(this).addClass('active');
//		$("#check2").removeClass('active');
//		 member = 1;
//	});
//	$("#check2").click(function() {
//		$(this).addClass('active');
//		$("#check1").removeClass('active');
//		member = 2;
//	});
	$(document).ready(function() {
//		$(".model").hide();
		$.post("{{url('wechat/user/pass')}}",  function(data) {
//			console.log(data);
			var str = '';
			if (data.status == 200){
				if(data.data.status == 1){
					$("#username").attr("disabled","disabled");
					$(".check").attr("disabled",true);
					$('.submit').text("已审核");
					$('.submit').css('pointer-events','none');
				}else if(data.data.status == 2){
					$("#username").attr("disabled","disabled");
					$(".check").attr("disabled",true);
					$('.submit').text("待审核");
					$('.submit').css('pointer-events','none');
				}
				$('#username').val(data.data.username);
				if (data.data.member == 1) {
					$('#check1').addClass('active');
					$("#check2").removeClass('active');
				}else if (data.data.member == 2) {
					$("#check2").addClass('active');
					$("#check1").removeClass('active');
				}
			}
			member = data.data.member;
		})
	});
	$('.submit').click(function() {
		if($('#username').val() == ''){

			$('.model_1').show();
			return;
		}else{
			//这里提交身份
			var type = member;
			var username = $('#username').val();
			console.log("提交",data);
			$.post("{{url('wechat/user/edit')}}", {
				member:type , username:username
			} , function(data) {
				if (data.status == 200) {
					$('.model_2').show();
				}
			})
		}
	});
	$("#go_back").click(function() {
		$(".model_1").hide()
	})
	$("#go_back_2").click(function() {
		$(".model_2").hide()
	})
</script>
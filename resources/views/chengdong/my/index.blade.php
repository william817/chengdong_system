<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>个人中心</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/people_center.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	</head>
	<body>
		<div class="list_con">
		    <div class="header">
		    	<img class="logo" src="{{asset('image1/center.jpg')}}"/>
		    	<div class="header_botttom flex-center">
		    		<img class="line" src="{{asset('image1/line_1.png')}}"/>
		    	</div>
		    </div>
		    <!--修改-->
			<div class="list_pos">
				<div class="title flex-center">
					<img src="{{asset('img/people.png')}}"/>
					个人中心
				</div>
				<a href="{{url('my/info')}}">
					<div class="list flex-around">
						<div class="left">
							个人信息
						</div>
						<img class="right" src="{{asset('image1/jiantou.png')}}"/>
					</div>
				</a>
				{{--<a href="{{url('holdOn')}}">--}}
				{{--<a href="{{url('my/issue')}}">--}}
					{{--<div class="list flex-around">--}}
						{{--<div class="left">--}}
							{{--我要培训--}}
						{{--</div>--}}
						{{--<img class="right" src="{{asset('image1/jiantou.png')}}"/>--}}
					{{--</div>--}}
				{{--</a>--}}
				{{--<a href="{{url('holdOn')}}">--}}
					<a href="{{url('integral/integral')}}">
					<div class="list flex-around">
						<div class="left flex-center">
							 积分系统
						 </div>
						<img class="right" src="{{asset('image1/jiantou.png')}}"/>
					</div>
				</a>
			</div>
		</div>
	</body>

</html>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>专业技能</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/skills.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	</head>
	<body>
		<div class="list_con">
			 <div class="header">
		    	<img class="logo" src="{{asset('image1/skills.jpg')}}"/>
		   </div>
			<div class="list_pos">
				<div class="title flex-center">
					<img class="img1" src="{{asset('image1/light.png')}}"/>
					<img class="img2" src="{{asset('image1/line_3.png')}}"/>
					<div class="">
						<p class="big">阅读分享</p>
						<p class="little">Professional Skills</p>
					</div>
				</div>
			    <div id="list_con">
				{{--<a href="{{url('school/text')}}">--}}
					{{--<div class="list list3 flex-around">--}}
						{{--<div class="left">--}}
							{{--1--}}
						{{--</div>--}}
                         {{--<div class="right">--}}
							 {{--习近平六下解放...--}}
                         {{--</div>--}}
					{{--</div>--}}
				{{--</a>--}}
				{{--<a href="{{url('school/text')}}">--}}
					{{--<div class="list list3 flex-around">--}}
						{{--<div class="left">--}}
							{{--2--}}
						{{--</div>--}}
                         {{--<div class="right">--}}
							 {{--国家税务总局局...国家税务总局局...--}}
                         {{--</div>--}}
					{{--</div>--}}
				{{--</a>--}}
				{{--<a href="{{url('school/text')}}">--}}
					{{--<div class="list list3 flex-around">--}}
						{{--<div class="left">--}}
							{{--3--}}
						{{--</div>--}}
                         {{--<div class="right">--}}
							 {{--科技部部长指出...--}}
                         {{--</div>--}}
					{{--</div>--}}
				{{--</a>--}}
				</div>
				<img src="{{asset('image1/up_w.png')}}" class="showmore">
				</img>
			</div>
		</div>
	</body>

</html>
<script>
	$(document).ready(function(){
		var type = 0;
		$.post("{{url('wechat/share/list')}}",{type:type},function(data){
//			console.log(data);
			if (data.status == 200) {
				console.log(data);
				var str = '';
				var id = '';
				for (var i=0 ; i<data.data.length ; i++) {
					id = data.data[i].id;
					var url = "{{url('school/text?id=')}}"+id +"?type=2";
					console.log(url);
					str += '<a href='+url+'>' +
							'<div class="list list3 flex-around">' +
							'<div class="left">'+(i+1)+'</div>' +
							'<div class="right">' +
							data.data[i].title +
							'</div>' +
							'</div>' +
							'</a>';
//					console.log(str);
				}
				$('#list_con').append(str);
			}
		});
	});
</script>
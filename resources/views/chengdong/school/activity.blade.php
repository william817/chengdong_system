<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>党建活动</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/activity.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/public.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	</head>
	<body>
		<div class="list_con">
			<div class="title"></div>
			<div class="list_pos">
				<div class="pos flex-center">
					<img src="{{asset('image1/danjiant.png')}}"/>
				</div></div>
			<div class="time flex-center"><div class="left">活动时间</div><div class="right" id="time"></div></div>
			<div class="time flex-center"><div class="left">活动地点</div><div class="right" id="address"></div></div>


			<button id="apply" class="click">
				<span class="apply">
					我要签到
				</span>
			</button>
			<div class="seccess">
				<div class="applysccsess flex-center" >
					签到成功
					<img src="{{asset('image1/ok.png')}}"/>
				</div>
			</div>
			<!--底部的固定内容-->
			<img class="img_pos" src="{{asset('image1/d_bg_pos.png')}}"/>
            <img class="bottom" src="{{asset('image1/bottom.png')}}"/>
		</div>
	</body>

</html>

<script>
	var getParam = function(name){
		var search = document.location.search;
		var pattern = new RegExp("[?&]"+name+"\=([^&]+)", "g");
		var matcher = pattern.exec(search);
		var items = null;
		if(null != matcher){
			try{
				items = decodeURIComponent(decodeURIComponent(matcher[1]));
			}catch(e){
				try{
					items = decodeURIComponent(matcher[1]);
				}catch(e){
					items = matcher[1];
				}
			}
		}
		return items;
	};
	$(document).ready(function(){
		var id = getParam('id');
		$.post("{{url('member/info')}}",{id:id},function(data){
			if (data.status == 200) {
				$('.title').text(data.data.title);
				$('.list_pos').text(data.data.comment);
				var date = new Date(data.data.start_time*1000);
				Y = date.getFullYear() + '-'; // 获取完整的年份(4位,1970)
				m = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-'; // 获取月份(0-11,0代表1月,用的时候记得加上1)
				d = date.getDate() + ' '; // 获取日(1-31)
				h = date.getHours() + ':'; // 获取小时数(0-23)
				i = date.getMinutes(); // 获取分钟数(0-59)
				$('#time').text(Y+m+d+h+i);
				$('#address').text(data.data.address);
				var str = '<input type = "hidden" value = "'+data.data.id+'" class = "hidden">';
				$('.list_con').append(str);
			}
		});
	});
	$('#apply').click(function(){
		var id = $('.hidden').val();
		$.post("{{url('sign/in')}}",{id:id},function(data){
			if (data.status == 200) {
				$('#apply').css('display','none');
				$('.seccess').show();
			} else if (data.status == 400) {
				alert(data.msg);
			} else {
				alert('抱歉，服务器开小差了！');
			}
		});
	});
</script>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>培训任务</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/list.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/public.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	</head>
	<body>
		<div class="list_con">
			<div class="content">
			<img class="img_pos" src="{{asset('image1/d_bg_pos.png')}}"/>
			<div class="list_pos">
				<div class="title flex-center">
						<img class="img1" src="{{asset('image1/dang_2.png')}}"/>
						<img class="img2" src="{{asset('image1/line_2.png')}}"/>
						<div class="">
							<p class="big">培训任务</p>
							<p class="little">Party building Activities</p>
						</div>
				</div>

				<div id="list_con">
					{{--<a  href="{{url('base/activity')}}">--}}
						{{--<div class="list flex-around">--}}
								{{--<div class="left">--}}
									{{--1--}}
								{{--</div>--}}
		                         {{--<div class="right">--}}
		                         	{{--全国十三大人民...--}}
		                         {{--</div>--}}
							{{--</div>--}}
					{{--</a>--}}
					{{--<a  href="{{url('base/activity')}}">--}}
						{{--<div class="list flex-around">--}}
							{{--<div class="left">--}}
								{{--2--}}
							{{--</div>--}}
							{{--<div class="right">--}}
								{{--毛主席思想--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</a>--}}
					{{--<a  href="{{url('base/activity')}}">--}}
						{{--<div class="list flex-around">--}}
							{{--<div class="left">--}}
								{{--3--}}
							{{--</div>--}}
							{{--<div class="right">--}}
								{{--邓小平理论--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</a>--}}
				</div>
				<img src="{{asset('image1/up_red.png')}}" class="showmore">
				</img>
			</div>
			</div>
			<img class="bottom" src="{{asset('image1/bottom.png')}}"/>
		</div>
		
		<script type="text/javascript">
			$(function() {
				$.post("{{url('member/list')}}",{type:0,kind:1},function(data){
					console.log(data);
					var str = "";
					for(var i=0; i<data.data.length; i++) {
						id = data.data[i].id;
						var url = "{{url('school/activity?id=')}}"+id;
						console.log(url);
						str += '<a href='+url+'>' +
								'<div class="list flex-around">' +
								'<div class="left">'+(i+1)+'</div>' +
								'<div class="right">' +
								data.data[i].title +
								'</div>' +
								'</div>' +
								'</a>';
					}
					$('#list_con').append(str);
				});
			});
		</script>
	</body>

</html>
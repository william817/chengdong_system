<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>美文分享</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/share.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	</head>
	<body>
		<div class="list_con">
			<div class="header">
		    	<img class="logo" src="{{asset('image1/share.jpg')}}"/>
		   </div>
			<div class="list_pos">
				<div class="title flex-center">
					<img class="img1" src="{{asset('image1/light.png')}}"/>
					<img class="img2" src="{{asset('image1/line_3.png')}}"/>
					<div class="">
						<p class="big">学习阅读</p>
						<p class="little">Good Of Share</p>
					</div>
				</div>
				<div id="list_con">
				</div>
				<img src="{{asset('image1/up_w.png')}}" class="showmore">
				</img>
			</div>
		</div>
	</body>
</html>
<script>
	$(document).ready(function(){
		var type = 0;
		$.post("{{url('wechat/study/list')}}",{type:type},function(data){
//			console.log(data);
			if (data.status == 200) {
				var str = '';
				var id = '';
				for (var i=0 ; i<data.data.length ; i++) {
					id = data.data[i].id;
					var url = "{{url('school/text?id=')}}"+id +"?type=1";
					console.log(url);
					str += '<a href='+url+'>' +
							'<div class="list list3 flex-around">' +
							'<div class="left">'+(i+1)+'</div>' +
							'<div class="right">' +
							data.data[i].title +
							'</div>' +
							'</div>' +
							'</a>';
//					console.log(str);
				}
				$('#list_con').append(str);
			}
		});
	});
</script>
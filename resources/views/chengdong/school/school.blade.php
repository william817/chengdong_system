<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>党建</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/school.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	</head>
	<body>
		<div class="list_con">
		    <div class="header">
		    	<img class="logo" src="{{asset('image1/chengdong.jpg')}}"/>
		    	<div class="header_botttom flex-center">
		    		<img class="line" src="{{asset('image1/line_1.png')}}"/>
		    	</div>
		    </div>
		    <!--修改-->
			<div class="list_pos">
				<div class="title">
					<p>党建</p>
					<p>Metro-East School</p>
				</div>
				<a href="{{url('school/share')}}">
					<div class="list flex-around">
						<div class="left">
							阅读学习
						</div>
						<img class="right" src="{{asset('image1/jiantou.png')}}"/>
					</div>
				</a>
				<a href="{{url('school/skill')}}">
					<div class="list flex-around">
						<div class="left">
							学习分享
						</div>
						<img class="right" src="{{asset('image1/jiantou.png')}}"/>
					</div>
				</a>
				<a href="{{url('school/activity_list')}}">
					<div class="list flex-around">
						<div class="left flex-center">
							 {{--<img src="{{asset('image1/dang_1.png')}}"/>--}}
							 培训任务
						 </div>
						<img class="right" src="{{asset('image1/jiantou.png')}}"/>
					</div>
				</a>
			</div>
		</div>
	</body>

</html>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>综合培训</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/the_synth.css?v=1')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	</head>
	<body>
		<div class="list_con">
			 <div class="header">
		    	<img class="logo" src="{{asset('image1/chengdong.jpg')}}"/>
		    	<div class="header_botttom flex-center">
		    		<img class="line" src="{{asset('image1/line_1.png')}}"/>
		    	</div>
		    </div>
			<div class="list_pos">
				<div class="title flex-center">
					<img src="{{asset('img/zhong_he.png')}}"/>
					综合培训
				</div>
				<a href="{{url('synth/must')}}">
					<div class="list flex-around">
						<div class="left">
							青年骨干培训班
						</div>
						<img class="right" src="{{asset('image1/jiantou.png')}}"/>
					</div>
				</a>
				<a href="{{url('synth/take')}}">
				{{--<a href="{{url('holdOn')}}">--}}
					<div class="list flex-around">
						<div class="left">
							其他培训
						</div>
						<img class="right" src="{{asset('image1/jiantou.png')}}"/>
					</div>
				</a>
			</div>
		</div>
	</body>

</html>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>其他培训</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/the_synth_take_activity.css?v=1')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
		<script src="{{asset('js/leftTime.min.js')}}"></script>
		<script src="{{asset('layer/layer.js')}}"></script>
		<style>
			.content1{
				font-size: 40px;
				text-align: center;
				margin: 1.2rem 1rem;
			}
		</style>
	</head>

	<body>
		<div class="list_con">
			<div class="header">
				<img class="logo" src="{{asset('image1/share.jpg')}}" />
				<div class="header_botttom flex-center">
					<img class="line" src="{{asset('image1/line_1.png')}}" />
				</div>
			</div>
			<div class="list_pos">
				<div class="title flex-center">

				</div>
				<div class="list_cons">
					<div class="list clearfix">
						<div class="text">时间:</div>
						<div class="text_r text_01"></div>
					</div>
					<div class="list clearfix">
						<div class="text">报名截止:</div>
						<div class="text_r text_04"></div>
					</div>
					<div class="list clearfix">
						<div class="text">地点:</div>
						<div class="text_r text_02"></div>
					</div>
					<div class="list clearfix">
						<div class="text">简介:</div>
						<div class="text_r text_03"></div>
					</div>
					<div class="list clearfix">
					<div class="text08">学习资料：</div>
					</div>
					<div class="list zhiliao">

						{{--<div class="list" style="width:11rem;">--}}
						{{--<a href="">资料资料资料资料资料</a>--}}
						{{--</div>--}}
						{{--<div class="list" style="width:11rem;">--}}
							{{--<a href="">资料资料资料资料资料</a>--}}
						{{--</div>--}}
					</div>
				</div>
				<div class="condition">
					<img class="img_l" src="{{asset('image1/img_l.png')}}" />
					<img class="img_r" src="{{asset('image1/img_r.png')}}" />
				</div>
				<div class="button_con clearfix">
					<button id="check_in" class="check_in">
					<span id="to_apply">
						我要签到
					</span>
					<span id="success">
						签到成功
					</span>
			    </button>
					<button id="feedback" class="feedback">
					<span id="to_feed">
						学习反馈
					</span>
					<span id="feed_success">
						反馈成功
					</span>
			    </button>
				</div>
				<div class="count_down" id="dataInfoShow">
					<div class="center" style="background: #ff0000">
					</div>
					<div class="data-show-box" id="dateShow">
						<span class="date-tiem-span d">00</span>天
						<span class="date-tiem-span h">00</span>时
						<span class="date-tiem-span m">00</span>分
						<span class="date-s-span s">00</span>秒
					</div>


				</div>
				<div id="feed">
					<div class="feed_title flex-around" id="feed_title">
				</div>
					<div id="list_con">

					</div>
				</div>
				<button id="apply" class="click">
					<span class="apply">
						我要报名
					</span>
				</button>
				<div class="model">
					<div class="content">
						<div class="content1"></div>
					</div>
					<button id="go_back" class="click">
				报名
			</button>
				</div>
			</div>
		</div>
	</body>

</html>
<script>
	var start_time,current_time,over_time;
	//获取参数的方法
	var getParam = function (name) {
		var search = document.location.search;
		var pattern = new RegExp("[?&]" + name + "\=([^&]+)", "g");
		var matcher = pattern.exec(search);
		var items = null;
		if (null != matcher) {
			try {
				items = decodeURIComponent(decodeURIComponent(matcher[1]));
			} catch (e) {
				try {
					items = decodeURIComponent(matcher[1]);
				} catch (e) {
					items = matcher[1];
				}
			}
		}
		return items;
	};
	$(document).ready(function () {
		var id = getParam('id');
		$.post("{{url('wechat/multi/info')}}", {
			id: id
		}, function (data) {
//			`
			if (data.status == 200) {
				$('.title').html(data.data.title);
				$('.text_01').html(data.data.start_time);
				$('.text_02').html(data.data.address);
				$('.text_03').html(data.data.abstract);
				$('.text_04').html(data.data.apply_time);
				$('.condition').html('当前报名人数为'+ data.data.count + '人,报满'+ data.data.setting_num +'人开课。');
				//$('.count_down').html(data.data.over_time);
                start_time = data.data.start_time;
				over_time = data.data.over_time;
				current_time = data.data.current_time;
				var str = '';
				if (data.data.material != null) {
					for (var i = 0; i < data.data.material.length; i++) {
						str += '<div class="list flex-center">' +
//								'<div class="left" style="text-align: left">' + '学习资料:' + '</div>' +
								'<div class="list_a">' + '<a href="'+data.data.material[i].mate_url+'">'+data.data.material[i].mate_time +'</a>' +
								'</div>' + '</div>';
					}
					$('.zhiliao').append(str);
				} else {
					str += '暂无学习资料...';
					$('.zhiliao').append(str);
				}
//				alert(data.data.is_signUp);
//				alert(data.data.is_signIn);
				if (data.data.is_signUp == 1 && data.data.is_signIn == 0 ) {
					$('#apply').css('display', 'none');
					$('.button_con').show();
				} else if (data.data.is_signUp == 1 && data.data.is_signIn == 1 ) {
					$('#apply').css('display', 'none');
					$('#to_apply').hide();
					$('.button_con,.count_down,#feed,#success').show();
				}
			}
            setDateImportFn(start_time,over_time,current_time);
		});

	});
	function setDateImportFn(start_time,over_time,current_time) {
        // var over_time=$(".over_time").text(),
        //     current_time=$(".current_time").text();
        var data1={};
		//当前时间
        data1.startdate=current_time;
		//反馈结束时间
        data1.enddate=over_time;

        clearTime2=$.leftTime(data1,function(d){
			if(d.status){
				var $dateShow1=$("#dateShow");
				$dateShow1.find(".d").html(d.d);
				$dateShow1.find(".h").html(d.h);
				$dateShow1.find(".m").html(d.m);
				$dateShow1.find(".s").html(d.s);
				$("#dataInfoShow").html();

			}
		});
	}


	$(document).ready(function () {
		var id = getParam('id');
		var type = 2;
		var style = 2;

		$.post("{{url('wechat/feedback/list')}}", {
			id: id, type: type, style:style
		}, function (data) {
			if (data.status == 200) {
				if (data.data.length > 0) {
					var feed_title = '<span>反馈区</span><a href="{{url("feed/list")}}?id='+id+'&type='+type+'&style='+style +'">显示更多</a>';
					$("#feed_title").append(feed_title);
				}
				var str = '';
				for (var i = 0; i < data.data.length; i++) {
					var str1 = '';
					if (data.data[i].is_zan == 1) {
						str1 += '<img src="{{asset('img/zan.png')}}" />';
					} else {
						str1 += '<img id="img' + i + '" src="{{asset('img/nozan.png')}}" onclick="zan(' + data.data[i].id + ',' + i + ')" />';
					}
					str += '<div class="list flex-center" style="justify-content: left;">' +
							'<div class="left data-content " >' + data.data[i].contentY + '</div>' +
							'<div class="right flex-center " style="justify-content: left;">' + str1 + '&nbsp;' + '<span id = "num' + i + '">' + data.data[i].num + '</span>' +
							'</div>' +
							'</div>';
				}
				$('#list_con').append(str);
			}
		});
	});
	$('#apply').click(function() {
		var id = getParam('id');
		var type = 2;
		$.post("{{url('wechat/signUp/list')}}", {
			id: id,type:type
		}, function (data) {
			if (data.status == 200) {
				$('#apply').css('display', 'none');
//				$('.model').show();
				//var str = '';
				//str += '目前报名人数为'+ data.count + '人,报满'+ data.setting_num +'人开课，是否继续报名？';
				layer.open({
					content: '是否确定报名!'
					,btn: ['是', '否']
					,yes: function(index){
						$.post("{{url('wechat/sign/up')}}", {
							id: id,type:type
						}, function (data) {
							if (data.status == 200) {
								layer.open({
									content: '报名成功'
									,btn: '我知道了'
								});
								$('.model').hide();
								$('.button_con').show();
							} else {
								layer.open({
									content: data.msg
									,btn: '我知道了'
								});
								$('#apply').show();
							}
						});
						layer.close(index);
					}
					,no: function(index){
						$('#apply').show();
					}
				});
//				console.log(str);
//				$('.content1').append(str);
			}
		});

	});

	//报名成功
		{{--$("#go_back").click(function() {--}}
			{{--var id = getParam('id');--}}
			{{--var type = 2;--}}
			{{--$.post("{{url('wechat/sign/up')}}", {--}}
				{{--id: id,type:type--}}
			{{--}, function (data) {--}}
				{{--if (data.status == 200) {--}}
					{{--layer.open({--}}
						{{--content: '报名成功'--}}
						{{--,btn: '我知道了'--}}
					{{--});--}}
					{{--$('.model').hide();--}}
					{{--$('.button_con').show();--}}
				{{--} else {--}}
					{{--layer.open({--}}
						{{--content: data.msg--}}
						{{--,btn: '我知道了'--}}
					{{--});--}}
				{{--}--}}
			{{--});--}}

		{{--});--}}
	
	$("#to_apply").click(function () {
		var id = getParam('id');
		var type = 2;
		$.post("{{url('wechat/sign/in')}}", {
			id: id, type: type
		}, function (data) {
			if (data.status == 200) {
				layer.open({
					content: '签到成功'
					,btn: '我知道了'
				});
				$("#to_apply").hide();
				$("#success,.count_down,#feed").show();
			} else {
				layer.open({
					content: data.msg
					,btn: '我知道了'
				});
			}
		});

	});
	$("#success").click(function(){
		layer.open({
			content: '您已经签到过了！'
			,btn: '我知道了'
		});
//		alert("您已经签到过了！");
	});
	$("#to_feed").click(function () {
		var id = getParam('id');
		window.location.href = "{{url('synth/feed')}}?id=" + id;
	});

	function zan(id, i) {
		$.post("{{url('wechat/zan/add')}}", {
			id: id
		}, function (data) {
			if (data.status === 200) {
				var src = "{{asset('img/zan.png')}}";
				$("#img" + i).prop("src", src);
				var num = $("#num" + i).html();
				num = $("#num" + i).html(++num);
			} else {
				layer.open({
					content: data.msg
					,btn: '我知道了'
				});
			}
		});
	}


</script>

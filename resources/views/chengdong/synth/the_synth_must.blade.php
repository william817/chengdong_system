<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>青年骨干培训班</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/the_synth_must.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	</head>

	<body>
		<div class="list_con">
			<div class="header">
				<img class="logo" src="{{asset('image1/chengdong.jpg')}}" />
				<div class="header_botttom flex-center">
					<img class="line" src="{{asset('image1/line_1.png')}}" />
				</div>
			</div>
			<div class="list_pos">
				<div class="title flex-center">
					<img src="{{asset('img/zhong_he.png')}}" /> 青年骨干培训班
				</div>
				<div id="list_con">

				</div>
			</div>
		</div>
	</body>

</html>
<script>
	$(document).ready(function() {
		$.post("{{url('wechat/multiReq/list')}}", function(data) {
			if (data.data.length == 0) {
				var str2 = '<span style="font-size: 25px">暂无活动发布!</span>';
				$('#list_con').append(str2).css({"text-align":"center"});
			}
			if(data.status == 200) {
				console.log(data);
				var str = '';
				var id = data.data.id;
				for(var i = 0; i < data.data.length; i++) {
					id = data.data[i].id;
					var str1 = '';
					if (data.data[i].is_join == 0) {
						str1 += '<div class="rights active">' + '未修' + '</div>';
					} else {
						str1 += '<div class="rights">' + '已修' + '</div>';
					}
					var url = "{{url('synth/activity')}}?id="+id;
					str += '<div class="list_content flex-between">' +
							'<a href='+ url +'>' +
							'<div class="list">'+
							'<div class="left">'+ (i + 1) + '</div>' +
							'<div class="right">' + data.data[i].title + '</div>' +
							'</div>' +
							'</a>' + str1 +
							'</div>';

//					console.log(str);
				}
				$('#list_con').append(str);
			}
		});
	});
</script>
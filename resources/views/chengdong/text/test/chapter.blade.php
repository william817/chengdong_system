<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>在线测试</title>
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<meta name="author" content="" />
<meta name="renderer" content="webkit">
<meta content='target-densitydpi=device-dpi, width=640' name='viewport'>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="white">
<meta name="format-detection" content="telephone=no">
<link href="{{asset('style/css.css?v=1')}}" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
<style type="text/css">
	.ys {
		color: #d30101;
	}
</style>
<script>
	/*不同分辨率手机加载*/
	var userScalable = "no";
	var ins = "";
	(function () {
		if (window.devicePixelRatio === 1.5) {
			userScalable = "yes";
			ins = "initial-scale=0.5"
		}
		var text = "<meta content='" + ins + ", target-densitydpi=device-dpi, width=640,user-scalable=" + userScalable + "' name='viewport'>";
		document.write(text);
	})();
</script>
</head>
<body>

<div id="answer" class="w640">
    <img class="logo_img" src="{{asset('image1/test.jpg')}}"/>
    <div id="answer_con">
   
    </div>
</div>
<script type="text/javascript">
	//获取参数的方法
	length = 0;
    var id = 0;
	var getParam = function (name) {
		var search = document.location.search;
		var pattern = new RegExp("[?&]" + name + "\=([^&]+)", "g");
		var matcher = pattern.exec(search);
		var items = null;
		if (null != matcher) {
			try {
				items = decodeURIComponent(decodeURIComponent(matcher[1]));
			} catch (e) {
				try {
					items = decodeURIComponent(matcher[1]);
				} catch (e) {
					items = matcher[1];
				}
			}
		}
		return items;
	};
	$(document).ready(function () {
		id = getParam('id');
		$.post("{{url('wechat/exam/info')}}", {
			times_id: id
		}, function (data) {
			console.log(data.data);
		var str = '';
		datas = data.data;
		length = datas.length;
		for(var i = 0;i<datas.length;i++){
			if((i+1)==1){
				str+="<div id='q"+(i+1) +"'><div class='content'>"+
						"<p class='subject_t'>"+(i+1)+'.'+datas[i].question+"</p>"+
						"<p><input id='rd"+(i+1)+"_1' name='rd"+(i+1)+"' type='radio' value='true' sure='A'><label for='rd"+(i+1)+"_1'>"+datas[i].answer.A+"</label></p>"+
						"<p><input id='rd"+(i+1)+"_2' name='rd"+(i+1)+"' type='radio' value='false' sure='B'><label for='rd"+(i+1)+"_2'>"+datas[i].answer.B+"</label></p>"+
						"<p><input id='rd"+(i+1)+"_3' name='rd"+(i+1)+"' type='radio' value='false' sure='C'><label for='rd"+(i+1)+"_3'>"+datas[i].answer.C+"</label></p>"+
						"<p><input id='rd"+(i+1)+"_4' name='rd"+(i+1)+"' type='radio' value='false' sure='D'><label for='rd"+(i+1)+"_4'>"+datas[i].answer.D+"</label></p>"+
					"</div>"+
					"<div id='msg"+(i+1)+"' class='no_choice' style='display:none;'>您还没选择！</div>"+
					"<div class='enter_btn'><a id='"+(i+1)+"' href='javascript:void(0);'>确定</a></div>"+
				"</div>"+
				 "<div id='a"+(i+1)+"' style='display:none;'>"+
		            "<div class='content_jd'>"+
						"<p></p>"+
							"<span id='r"+(i+1)+"' class='right' style='display:none;'></span>"+
							"<span id='w"+(i+1)+"' class='wrong' style='display:none;'></span>"+
					"</div>"+
					"<div class='enter_btn_2'><a id="+(i+1)+ " href='javascript:void(0);'>下一题</a></div>"+
				"</div>"
			}else{
					str += "<div id='q"+(i+1) +"' style='display:none;'><div class='content'>"+
						"<p class='subject_t'>"+(i+1)+'.'+datas[i].question+"</p>"+
						"<p><input id='rd"+(i+1)+"_1' name='rd"+(i+1)+"' type='radio' value='true' sure='A'><label for='rd"+(i+1)+"_1'>"+datas[i].answer.A+"</label></p>"+
						"<p><input id='rd"+(i+1)+"_2' name='rd"+(i+1)+"' type='radio' value='false' sure='B'><label for='rd"+(i+1)+"_2'>"+datas[i].answer.B+"</label></p>"+
						"<p><input id='rd"+(i+1)+"_3' name='rd"+(i+1)+"' type='radio' value='false' sure='C'><label for='rd"+(i+1)+"_3'>"+datas[i].answer.C+"</label></p>"+
						"<p><input id='rd"+(i+1)+"_4' name='rd"+(i+1)+"' type='radio' value='false' sure='D'><label for='rd"+(i+1)+"_4'>"+datas[i].answer.D+"</label></p>"+
					"</div>"+
					"<div id='msg"+(i+1)+"' class='no_choice' style='display:none;'>您还没选择！</div>"+
					"<div class='enter_btn'><a id='"+(i+1)+"' href='javascript:void(0);'>确定</a></div>"+
					"</div>"+
				 "<div id='a"+(i+1)+"' style='display:none;'>"+
		            "<div class='content_jd'>"+
						"<p></p>"+
							"<span id='r"+(i+1)+"' class='right' style='display:none;'></span>"+
							"<span id='w"+(i+1)+"' class='wrong' style='display:none;'></span>"+
					"</div>"+
					"<div class='enter_btn_2'><a id="+(i+1)+" href='javascript:void(0);'>下一题</a></div>"+
				"</div>"
			}
		}
		$("#answer_con").html(str);
		});
	});
	allscode = 0;
	$(function () {
		a1 = a2 = a3 = a4 = a5 = a6 = a7 = a8 = a9 = a10 = 0;
		$("#answer_con").on('click','.enter_btn a',function(e){  
			var new_id = e.target.id;
		   	var old_id = Number(e.target.id)+1;
            var newstr = '#q'+new_id+' '+"input:radio[name='rd"+new_id+"']:checked";
            var msg = "#msg"+new_id;
           	var rl1 = $(newstr).val();
           	var checked = $(newstr).attr('sure');
           	var qustion1 = "#q"+new_id;
           	var qustion2 = "#q"+old_id;
           	var answer = "#a"+new_id;
           	var right = "#r"+new_id;
           	var wrong = "#w"+new_id;
			if (rl1 == null) {
				$(msg).fadeIn(500);
			}
			else {
				setTimeout($(qustion1).fadeOut(100), 1);
				setTimeout($(answer).fadeIn(100), 100);
				if(datas[new_id-1].result == checked){
					allscode += datas[new_id-1].grade;
					console.log(allscode);
					$(right).fadeIn();
				}else {
					$(wrong).fadeIn();
				}
			}
        });
        //点击下一题
        $("#answer_con").on('click','.enter_btn_2 a',function(e){
        	var new_id = Number(e.target.id);
        	console.log(new_id);
        	var a = "#a"+(new_id);
        	var q = "#q"+(new_id+1);
        	if(new_id<datas.length){
        		$(a).hide();
        		$(q).show();
        	}else{
				window.location.href = "{{url('text/finish')}}?sc="+allscode+"&id="+id;
			}
        }); 
        
        
		{{--$("#b10").click(function () {--}}
			{{--var cbl10 = $('#q10 input:checkbox[name="cb10"]:checked');--}}
			{{--if (cbl10 == null) {--}}
				{{--$("#msg10").fadeIn(500);--}}
			{{--}--}}
			{{--else {--}}
				{{--sc = a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9 + a10;//分数--}}
				{{--window.location.href = "{{url('text/finish')}}?sc="+allscode;--}}
			{{--}--}}
		{{--});--}}
	});
</script>
</body>
</html>
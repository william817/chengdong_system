<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>测试说明</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/test_wraning.css?v=1')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/public.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
		<script src="{{asset('layer/layer.js')}}"></script>
	</head>
	<body>
		
		<div class="list_con">
			<div class="header">
		    	<img class="logo" src="{{asset('image1/test.jpg')}}"/>
		   </div>
		   
		   <div class="headertitle flex-center">
		   	  <img src="{{asset('image1/book.png')}}"/><span class="title"></span>
		   </div>
		   <div  class="list_pos">
				<div class="pos flex-center">
					<img src="{{asset('image1/test.png')}}"/>
				</div>
				<span class="content"></span>
			</div>
			
			<div class="condition">
				<img class="img_l" src="{{asset('image1/img_gl.png')}}"/>
				<img class="img_r" src="{{asset('image1/img_gr.png')}}"/>

			</div>
			<div id="jump">

			</div>
		</div>

	</body>

</html>
<script>
	//获取参数的方法
	var getParam = function (name) {
		var search = document.location.search;
		var pattern = new RegExp("[?&]" + name + "\=([^&]+)", "g");
		var matcher = pattern.exec(search);
		var items = null;
		if (null != matcher) {
			try {
				items = decodeURIComponent(decodeURIComponent(matcher[1]));
			} catch (e) {
				try {
					items = decodeURIComponent(matcher[1]);
				} catch (e) {
					items = matcher[1];
				}
			}
		}
		return items;
	};
	$(document).ready(function () {
		var id = getParam('id');
		$.post("{{url('wechat/examList/info')}}", {
			id:id
		}, function (data) {
			if (data.status == 200) {
				$('.title').html(data.data.title);
				$('.content').html(data.data.content);
				$('.condition').html('答题时间' + data.data.time + '分钟');
				var url = '<div id="apply" class="click" onclick="textAdmin('+ data.data.id +')"> <span class="apply">我要测试 </span> </div>';
				$('#jump').html(url);
			}
		});
	});
	function textAdmin(id) {
		$.post("{{url('wechat/examList/check')}}", {
			id:id
		}, function (data) {
			if (data.status == 200) {
				window.location.href = "{{url('text/charter')}}?id=" + id;
			} else {
				layer.open({
					content: data.msg
					,btn: '我知道了'
				});
			}
		});
	}
</script>
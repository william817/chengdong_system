<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>在线测试</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/test_wraning.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	</head>
	<body>
		<div class="list_con">
			<div class="header">
		    	<img class="logo" src="{{asset('image1/test.jpg')}}"/>
		   </div>
		   <div class="headertitle flex-center">
		   	  测试成绩
		   </div>
		   <div class="intergral">
		   	  <img src="{{asset('image1/book.png')}}"/>
		   	  <p class="sc"></p>
		   </div>
		   <div class="condition">
				<img class="img_l" src="{{asset('image1/img_gl.png')}}"/>
				<img class="img_r" src="{{asset('image1/img_gr.png')}}"/>

			</div>
			<div class="seccess">
				<a href="{{url('text/index')}}">
				<div class="applysccsess flex-center">
					返回首页
				</div>
				</a>
			</div>	
		</div>
	</body>

</html>
<script>
	var getParam = function (name) {
		var search = document.location.search;
		var pattern = new RegExp("[?&]" + name + "\=([^&]+)", "g");
		var matcher = pattern.exec(search);
		var items = null;
		if (null != matcher) {
			try {
				items = decodeURIComponent(decodeURIComponent(matcher[1]));
			} catch (e) {
				try {
					items = decodeURIComponent(matcher[1]);
				} catch (e) {
					items = matcher[1];
				}
			}
		}
		return items;
	};
	$(document).ready(function () {
		var id = getParam('id');
		var sc = getParam('sc');
		$.post("{{url('wechat/exam/sub')}}", {
			id: id, score: sc
		},function (data) {
			if (data.status==200){
				$('.sc').html(sc);
				$('.condition').append(data.content);
			}
		});
	});

</script>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>积分系统</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/integral.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
		<script src="{{asset('js/public.js')}}"></script>
	</head>

	<body>
		<div class="list_con">
			<div class="header">
		    	<img class="logo" src="{{asset('image1/intergral2.jpg')}}"/>
		   </div>
		    <div class="headertitle flex-center">
		   	   我的学分
		   </div>
		    <div class="intergral">
		   	  <p class = "score"></p>
		   </div>
		   <a href="{{url('integral/rank')}}">
			   <div class="number">
			   	  您目前的排名是
			   </div>
		   </a>
			<div class="list_pos flex-between">
			    <a href="{{url('integral/record')}}">
				<div class="list">
					学分<br/>记录
				</div>
				</a>
				<a href="{{url('integral/conversion')}}">
				{{--<a href="{{url('holdOn')}}">--}}
				<div class="list">
					礼品<br/>兑换
				</div>
				</a>
			</div>
		</div>
	</body>

</html>
<script>
	$(document).ready(function() {
		$.post("{{url('wechat/score/total')}}", function(data) {
			$('.score').html(data.data.score);
			var str = data.data.ranking + '/' + data.data.count;
			$('.number').append(str);
		});
	});
</script>
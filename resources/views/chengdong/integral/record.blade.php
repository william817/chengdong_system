<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>积分记录</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/record.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	</head>
	<body>
		<div class="list_con">
			<div class="header">
		    	<img class="logo" src="{{asset('image1/intergral2.jpg')}}"/>
		   </div>
		    <div class="headertitle flex-center">
		   	  <img src="{{asset('image1/recode.png')}}"/>积分记录
		   </div>
			<div class="list_pos">
				<div class="html">

				</div>

				<div class="seccess">
					<div class="applysccsess flex-center">
						返回
					</div>
				</div>	
			</div>
		</div>
		<script type="text/javascript">
			$(function(){
				$(".applysccsess").click(function(){
					window.history.go(-1)
				})
			})
		</script>
	</body>

</html>
<script>
	$(document).ready(function() {
		$.post("{{url('wechat/score/record')}}", function(data) {
			console.log(data);
			if (data.status == 200 ) {
				if (data.data.length != 0) {
					var str = '';
					for (var i = 0; i < data.data.length; i++) {
						str += '<div class="list flex-center">' +
								'<div class="left">' + data.data[i].create_time +
								'</div>' + '<div class="right">' + data.data[i].content +
								'</div>' + '</div>';
					}
					$('.html').html(str);
				} else {
					var str1 = '';
					str1 += '<div class="list flex-center">暂无积分记录</div>';
					$('.html').html(str1);
				}
			} else {
				var str2 = '';
				str2 += '<div class="list flex-center">服务器异常</div>';
				$('.html').html(str2);
			}
		});
	});
</script>
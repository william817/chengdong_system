<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>积分排名</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/integral.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
		<script src="{{asset('js/public.js')}}"></script>
	</head>

	<body>
		<div class="list_con">
			<div class="header">
		    	<img class="logo" src="{{asset('image1/intergral2.jpg')}}"/>
		   </div>
		    <div class="headertitle flex-center">
		   	   积分排行
		   </div>
		  <div class="numbers_con">
		  	
		
		   {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		   {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		   {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		   {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		   {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		   {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		   {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		   {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		   {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		    {{--<div class="numbers">--}}
		   	  {{--当前的排名--}}
		   {{--</div>--}}
		     </div>
		</div>
	</body>

</html>
<script>
	$(document).ready(function() {
		$.post("{{url('wechat/score/rank')}}", function(data) {
			if (data.status == 200 ) {

				var str = '';
				for (var i = 0; i < data.data.length; i++) {
					str += '<div class="numbers">' +
							data.data[i].username + '&nbsp;'+'&nbsp;'+ ':' + '&nbsp;'+'&nbsp;'+ data.data[i].score + '学分' +
							'</div>';
				}
				$('.numbers_con').html(str);

			} else {
				var str2 = '';
				str2 += '<div class="list flex-center">服务器异常</div>';
				$('.html').html(str2);
			}
		});
	});
</script>
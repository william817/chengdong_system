<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>积分兑换</title>
		<link rel="stylesheet" type="text/css" href="{{asset('css/conversion.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/public.css')}}" />
		<script src="{{asset('js/hotcss.js')}}"></script>
		<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
		<script src="{{asset('layer/layer.js')}}"></script>
	</head>

	<body>
		<div class="list_con">
			<div class="header">
		    	<img class="logo" src="{{asset('image1/intergral2.jpg')}}"/>
		   </div>
		    <div class="headertitle flex-center">
		   	  <img src="{{asset('image1/zhi.png')}}"/>礼品兑换
		   </div>
			<div class="list_pos">


			</div>

				<div class="wraning">
					兑换商品后请到前台处领取
				</div>
				<div class="seccess">
					<div class="applysccsess flex-center">
						返回
					</div>
				</div>
			</div>

		<script type="text/javascript">
			$(function(){
				$(".applysccsess").click(function(){
					window.history.go(-1)
				})
			})
		</script>
	</body>

</html>
<script>
	$(document).ready(function () {
		$.post("{{url('wechat/shop/list')}}", {
		}, function (data) {
			console.log(data);
			if (data.status == 200) {
				var str = '';
				for (var i=0; i<data.data.length; i++) {
				str += '<div class="list">' +
						'<div class="left">' +
						'<span>' + data.data[i].name + '</span>' +
						'<span>&nbsp;' + data.data[i].score + '积分</span>' +
						'<span>&nbsp;数量:'+data.data[i].num + '</span>' +
						'</div>' + '<div class="right">' +
						'<img src="{{asset('image1/duihuan.png')}}" onclick="exchange('+ data.data[i].id + ')" />' +
						'</div>' + '</div>';
				}
				$('.list_pos').append(str);
			}
		});
	});

	function exchange(id) {
		$.post("{{url('wechat/shop/exchange')}}", {
			id:id
		}, function (data) {
			if (data.status == 200) {
				alert('兑换成功！')
				location.reload();
			}else {
				layer.open({
					content: data.msg
					,btn: '我知道了'
				});
			}
		})
	}
</script>